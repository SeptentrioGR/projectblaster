# Current Version 1.4 #

# What this game about? #
Play a wave based Survival game and collect coins to upgrade your ship. 
Defeat your enemies and reach the highest score!
Get Reward by defeating the waves and use the coins you collected to upgrade you ship.

# FEATURES #
Endless Wave survival based Gameplay
Enemies are randomize in every session.
5 Different Upgrades to get with the Coins 
Different Weapons to get thought the waves
Each Wave get different rewards

# To Do List #
There are few things that i am planning to add in the future that i just havent found the time.
Few things are:
1)Story Mode with missions and a story about the alien invasion 
2)Buy different ships
3)Boss Battles
4)Better upgrade system
5)better reward system

Latest Update 5 July 2016
Update History

# Update 1.4.2 #

-bug fixes

# Update 1.4 #

*New* Able to buy upgrade at wave 2 / 4 / 8 / 16 / 24 / 32 / 40 / 48 and beyond
*New* Change how the Magnet Upgrade works. 
Old Implementation: All items move towards ship anywhere in the level
New Implementation: When you go close by the item they start move towards you
The higher the magnet upgrade is the higher the distance the item will start to move towards the player
UI Changes
bugfixes
some new music
some new effects


#Update 1.3 #


Various Balance Changes
Added Floating Text when getting Score or Coins
Sounds should blend better
Added option to delete save data in settings

# Update 1.2 #
New Feature

Added New Introduction Screen for a small background story of the game skipable
Revamb Upgrade System.
Upgrades are now Available.
Shield : Protect you from on enemy attack
Missiles: Extra fire power against Ships
Fire Rate: How fast you can fire 
Speed: How fast your ship can move around 
General Changes
After some feedback i balanced out the difficuilty of the game. 
Various Bug fixes


# Update 1.1 #

Fixed Project Blaster title barely fits
made the menu scrolling faster in the main menu
change the text size in about Screen
Fixed Buttons that stayed highlighted after tapping them 

changed some in game text to make more sense
change the health bar from right to left down corner of the screen
fixed that Paused Menu overlap by Texts
Changed the size of the background planets to differentiate from foreground obstacles.
Autofire occurs during initial fly-in on a restart fixed

# Summary #

A game where you are flying in space and shooting bad guys and avoiding asteroids while collecting coins. The are two modes in the game Survival Mode where you have to survive as much as you can and collect coins and upgrade your ship. There is another one which is the story mode of the game. The game will have 12 Missions and the story. Each mission will have to go through various enemies and fight the bosses.

# Story #

It the near future humanity has advanced quite a lot and they build a aircraft prototype for space flight. The foundation of Earth Defense noticed some strange signals coming out of space. They could not figure out what it was. They decided to not wait any longer and send someone with the prototype aircraft to check it out. Jack one of the finest pilots they had was chosen to be the main pilot of the Space Hyperspace Intuitive Prototype or S.H.I.P for short. He trained and learn everything it needs to know about flying this complicated ship. Unfortunately his train was fall short when he had to try it immediately and if everything goes as plan to go investigate the signal near orbit of earth. Things turn upside down when the signal was a trap to lure him out and to destroy the ship because the ALien race that are called Maos did not want humans to be able to fly and explore the space they wanted to keep them occupied in their own planet and world as much as possible. WIthout Humans know Maos imprisoned humans long time ago in their little planet and they sabotage any space flight without they know it. To them they think that they are free to do whatever they want but in reality Maos rules them and Jack will be the only one who can defend the Earth Against and free  them. He soon will realise that failure is not an option. This is the chance for Humanity to free

# Gameplay #

The game uses an old school space shooting where you control the ship using the finger to move the ship around the Screen . As enemies come down and try to shoot you down you have to defeat them. While you have to avoid the asteroids which will damage your ship if they hit you. Each wave enemies will get spawn faster and they will be harder to kill. Survive as much as you can and beat your score.

You have the story Mode where you complete missions and go forward in the story. In the Mission UI

# Mindset #

The game will have score and the more you survive and higher the score will be. Stay alive as much as you can and compete with yourself or your friends.
 
# Technical #

Screens
1.	Title Screen
a.	Options
b.	Upgrades
c.	HighScore
d.	Play
2.	Game
a.	Score
b.	Missiles
c.	Coins
d.	PauseMenu
e.	GameOver
f.	Win
3.	GameOver
4.	HighScore
5.	Win
a.	HighScore

# Controls #

The game will mainly support touch for navigating though the main menu and controlling the ship. The ship will auto attack so you don't have to press any button to attack and everything is automatically.
Mechanics

The main game Mode of the game will br a survival mode. Each wave will include 16 enemies that you will have to defeat. When enemies are defeated a new and harder wave will appear. Each map will have 12 waves. 

The enemies will have variety of movement and AI which will use simple algorithm. 

# Level Design #

The Level will be a simple space scenery where the background will change randomly each match. there will be random planes that willl come by big or small randonly changing and also some nice star space effects. 

# Game Flow #

1.	Player Starts in the main menu
2.	Upgrades the ship if he has enough coins
3.	Starts the Game 
4.	If win goes back into the main menu and upgrades ship with Coins
5.	If looses goes back into the main menu and losses everything. 


# Development #

Abstract Classes / Components

1.	Ships
2.	Asteroids
3.	Game Manager
4.	Music Manager
5.	Level Manager
6.	Enemy Manager


# Derived Classes / Component Compositions #

1.	Ship
a.	Player
b.	Enemies
2.	Asteroids
 .	Rock
3.	LevelManager
 .	LevelBackground
4.	MusicManager
 .	PlayingMusic

# Style Attributes #

The game will have a Sprite style game with 3D and 2D graphics semi realistic. All ships are well draw and the background as well. Everything will blend together as i tryto make a material interface design. 

# Graphics Needed #

1.	Characters
a.	Ships
i.	PlayerShip
ii.	Enemy Ships
iii.	Boss
b.	Other
 .	Asteroids
2.	Ambient
 .	Space level