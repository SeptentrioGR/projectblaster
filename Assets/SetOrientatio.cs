using UnityEngine;

public class SetOrientatio : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        DeviceOrientation orientation = Input.deviceOrientation;
        if (orientation == DeviceOrientation.Portrait)
        {
            Screen.orientation = ScreenOrientation.Portrait;
        }

    }

}
