using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuPauseAndLoadLevel : MonoBehaviour
{

    public int levelToLoad;
    public float delay = 2f;

    // load the specified level
    void LoadLevel()
    {
        SceneManager.LoadScene(levelToLoad);
    }

    void loadLevelButton()
    {
        Invoke("LoadLevel", delay);
    }
}
