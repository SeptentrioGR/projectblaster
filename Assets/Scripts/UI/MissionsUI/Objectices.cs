public class Objectices
{
    public int rank;
    public string Name;
    public string desc;
    public int value;
    public bool completed;


    public Objectices(int rank, string name, string desc)
    {
        value = 0;
        setRank(rank);
        setName(name);
        setDesc(desc);
        Reset();

    }

    public void setName(string nName)
    {
        Name = nName;
    }

    public void setDesc(string ndesc)
    {
        desc = ndesc;
    }

    public void setRank(int nRank)
    {
        rank = nRank;
    }

    public void Completed()
    {
        completed = true;
    }

    public void Reset()
    {
        completed = false;
    }
}
