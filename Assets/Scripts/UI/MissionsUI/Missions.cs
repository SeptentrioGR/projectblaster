using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Missions : MonoBehaviour
{

    public List<GameObject> missions = new List<GameObject>();
    public List<Objectices> obj = new List<Objectices>();
    public GameObject ObjectivePrefab;
    public GameObject content;

    public GameObject ClaimRewardWindow;
    public bool claimingTheReward;
    public Animator[] textsAnimators;
    public Text[] textsOfReward;
    public GameObject button;

    public void initObjectives()
    {
        GameControler.control.LoadChallenges();
        obj.Add(new Objectices(GameControler.control.RankInKilling, "Killing", "Kill " + GameControler.control.enemykilled + " / " + GameControler.control.enemyToKill + " Enemies"));
        obj.Add(new Objectices(GameControler.control.RankInSurvival, "Survive", "Survive Wave " + GameControler.control.waveToSurvive));
        obj.Add(new Objectices(GameControler.control.RankInGreedy, "Greedy", "Collect " + GameControler.control.goldcoinscollected + " / " + GameControler.control.goldCointToUnlocked + " Gold Coins"));
        obj.Add(new Objectices(0, "Come over here!", "Upgrade Magnet "));
        obj.Add(new Objectices(0, "tatatatatatatatatata", "Upgrade Fire Rate "));
        obj.Add(new Objectices(0, "Need For Speed!", "Upgrade Speed"));
        obj.Add(new Objectices(0, "Big Weapon Upgrade", "Buy Weapons"));
        obj.Add(new Objectices(0, "Hit me! I am not afraid", "Upgrade The Shield "));
    }

    public Objectices getMission(int index)
    {
        return obj[index];
    }

    // Use this for initialization
    void Awake()
    {
        initObjectives();

        foreach (GameObject o in missions)
        {
            o.transform.GetChild(3).GetComponent<Button>().interactable = false;
        }


        for (int i = 0; i < missions.Count; i++)
        {
            missions[i].transform.GetChild(0).gameObject.GetComponent<Text>().text = "" + obj[i].rank;
            missions[i].transform.GetChild(1).gameObject.GetComponent<Text>().text = obj[i].Name;
            missions[i].transform.GetChild(2).gameObject.GetComponent<Text>().text = obj[i].desc;
            missions[i].transform.SetParent(content.transform);
        }
    }

    public void UpdateMissions()
    {
        for (int i = 0; i < missions.Count; i++)
        {
            missions[i].transform.GetChild(0).gameObject.GetComponent<Text>().text = "" + obj[i].rank;
            missions[i].transform.GetChild(1).gameObject.GetComponent<Text>().text = obj[i].Name;
            missions[i].transform.GetChild(2).gameObject.GetComponent<Text>().text = obj[i].desc;
            missions[i].transform.SetParent(content.transform);
        }


        obj[0].rank = GameControler.control.RankInKilling;
        obj[0].desc = "Kill " + GameControler.control.enemykilled + " / " + GameControler.control.enemyToKill + " Enemies";

        obj[1].rank = GameControler.control.RankInSurvival;
        obj[1].desc = "Survive Wave " + GameControler.control.waveToSurvive;

        obj[2].rank = GameControler.control.RankInGreedy;
        obj[2].desc = "Collect " + GameControler.control.goldcoinscollected + " / " + GameControler.control.goldCointToUnlocked + " Gold Coins";



    }

    void Update()
    {
        UpdateMissions();
        if (GameControler.control.RankInKilling < 24)
        {
            if (GameControler.control.enemykilled >= GameControler.control.enemyToKill)
            {
                missions[0].transform.GetChild(3).GetComponent<Button>().interactable = true;
                missions[0].transform.GetChild(3).GetComponent<Button>().transform.GetChild(0).GetComponent<Text>().text = "Claim";
            } else
            {
                missions[0].transform.GetChild(3).GetComponent<Button>().interactable = false;
                missions[0].transform.GetChild(3).GetComponent<Button>().transform.GetChild(0).GetComponent<Text>().text = "Not yet Unlocked";
            }
        } else
        {
            missions[0].transform.GetChild(3).GetComponent<Button>().interactable = false;
            missions[0].transform.GetChild(3).GetComponent<Button>().transform.GetChild(0).GetComponent<Text>().text = "Max Rank Reached";
        }

        if (GameControler.control.RankInSurvival < 24)
        {
            if (GameControler.control.waveSurvived >= GameControler.control.waveToSurvive)
            {
                missions[1].transform.GetChild(3).GetComponent<Button>().interactable = true;
                missions[1].transform.GetChild(3).GetComponent<Button>().transform.GetChild(0).GetComponent<Text>().text = "Claim";
            } else
            {
                missions[1].transform.GetChild(3).GetComponent<Button>().interactable = false;
                missions[1].transform.GetChild(3).GetComponent<Button>().transform.GetChild(0).GetComponent<Text>().text = "Not yet Unlocked";

            }
        } else
        {
            missions[1].transform.GetChild(3).GetComponent<Button>().interactable = false;
            missions[1].transform.GetChild(3).GetComponent<Button>().transform.GetChild(0).GetComponent<Text>().text = "Max Rank Reached";
        }

        if (GameControler.control.RankInGreedy < 24)
        {
            if (GameControler.control.goldcoinscollected >= GameControler.control.goldCointToUnlocked)
            {
                missions[2].transform.GetChild(3).GetComponent<Button>().interactable = true;
                missions[2].transform.GetChild(3).GetComponent<Button>().transform.GetChild(0).GetComponent<Text>().text = "Claim";
            } else
            {
                missions[2].transform.GetChild(3).GetComponent<Button>().interactable = false;
                missions[2].transform.GetChild(3).GetComponent<Button>().transform.GetChild(0).GetComponent<Text>().text = "Not yet Unlocked";
            }
        } else
        {
            missions[2].transform.GetChild(3).GetComponent<Button>().interactable = false;
            missions[2].transform.GetChild(3).GetComponent<Button>().transform.GetChild(0).GetComponent<Text>().text = "Max Rank Reached";
        }


        if (claimingTheReward)
        {
            ClaimRewardWindow.SetActive(true);

            if (textsAnimators[0].GetBool("Played") != true)
            {
                textsAnimators[0].SetBool("Played", true);
            } else if (textsAnimators[0].gameObject.transform.localScale.x >= 1)
            {
                if (textsAnimators[1].GetBool("Played") != true)
                {
                    textsAnimators[1].SetBool("Played", true);
                } else if (textsAnimators[1].gameObject.transform.localScale.x >= 1)
                {

                    if (textsAnimators[3].GetBool("Played") != true)
                    {
                        textsAnimators[3].SetBool("Played", true);
                    } else if (textsAnimators[3].gameObject.transform.localScale.x >= 1)
                    {

                        claimingTheReward = false;
                        button.SetActive(enabled);
                    }
                }

            }
        }
    }

    public void HideClaimWindow()
    {
        AudioManager.AudioAPI.PlaySound(1);
        ClaimRewardWindow.SetActive(false);
        for (int i = 0; i < textsAnimators.Length; i++)
        {
            textsAnimators[i].SetBool("Played", false);
            textsAnimators[i].gameObject.transform.localScale = new Vector3(0, 0, 0);
            button.SetActive(false);
        }
    }

    public void changeObjective(int m, int i)
    {
        missions[m].transform.GetChild(0).gameObject.GetComponent<Text>().text = "" + (obj[i].rank += 1);
        missions[m].transform.GetChild(2).gameObject.GetComponent<Text>().text = obj[i].desc;

    }

    public void IncreaseKillingRank(int i)
    {
        AudioManager.AudioAPI.PlaySound(1);
        if (!claimingTheReward)
        {
            GameControler.control.RankInKilling++;
            Claim("Killing", i);
            GameControler.control.SaveChallengesData();
            GameControler.control.SavePlayerUpgrades();
        }

    }

    public void IncreaseSurvivalRank(int i)
    {

        AudioManager.AudioAPI.PlaySound(1);
        if (!claimingTheReward)
        {
            GameControler.control.RankInSurvival++;
            Claim("Survive", i);
            GameControler.control.SaveChallengesData();
            GameControler.control.SavePlayerUpgrades();
        }
    }

    public void IncreaseGreedyRank(int i)
    {
        AudioManager.AudioAPI.PlaySound(1);
        if (!claimingTheReward)
        {
            GameControler.control.RankInGreedy++;
            Claim("Greedy", i);
            GameControler.control.SaveChallengesData();
        }
    }


    public void Claim(string value, int i)
    {
        switch (value)
        {
            case "Killing":
                claimingTheReward = true;
                changeObjective(i, 0);

                if (GameControler.control.RankInKilling == 8)
                {
                    ClaimRewardWindow.GetComponent<MissionRewardManager>().setMission(
                             "Congratulations", "" + obj[0].rank, " Unlocked new Ship!");
                    GameControler.control.shipUnlocked = 1;
                } else if (GameControler.control.RankInKilling % 3 == 0)
                {
                    ClaimRewardWindow.GetComponent<MissionRewardManager>().setMission(
                             "Dominant", "" + obj[0].rank, " Rewarded " + 250 + " Coins");
                    GameControler.control.coins += 250;
                } else
                {
                    ClaimRewardWindow.GetComponent<MissionRewardManager>().setMission(
                             "Good", "" + obj[0].rank, "No Reward");
                }
                GameControler.control.enemyToKill *= 2;
                break;
            case "Survive":
                claimingTheReward = true;
                changeObjective(i, 1);
                GameControler.control.waveToSurvive++;
                if (GameControler.control.RankInSurvival == 12)
                {
                    ClaimRewardWindow.GetComponent<MissionRewardManager>().setMission(
                                "Congratulations!", "" + obj[1].rank, "Unlocked new Ship!");
                    GameControler.control.shipUnlocked = 2;
                } else if (GameControler.control.RankInSurvival % 2 == 0)
                {
                    ClaimRewardWindow.GetComponent<MissionRewardManager>().setMission(
                                 "Nice!", "" + obj[1].rank, " Rewarded " + 100 + " Coins");
                    GameControler.control.coins += 100;
                } else if (GameControler.control.RankInSurvival % 4 == 0)
                {
                    ClaimRewardWindow.GetComponent<MissionRewardManager>().setMission(
                                   "Sweet!", "" + obj[1].rank, " Rewarded " + 10 + " Missiles !");
                    GameControler.control.missles += 10;
                } else
                {
                    ClaimRewardWindow.GetComponent<MissionRewardManager>().setMission(
                             "Sweet", "" + obj[1].rank, "No Reward");
                }
                break;
            case "Greedy":
                claimingTheReward = true;
                changeObjective(i, 2);
                GameControler.control.goldCointToUnlocked *= 2;
                if (GameControler.control.RankInGreedy % 2 == 0)
                {
                    ClaimRewardWindow.GetComponent<MissionRewardManager>().setMission(
                                   "Nice!", "" + obj[0].rank, " Rewarded " + 100 * GameControler.control.RankInGreedy + " Coins");
                    GameControler.control.coins += 100;
                } else if (GameControler.control.RankInGreedy % 4 == 0)
                {
                    ClaimRewardWindow.GetComponent<MissionRewardManager>().setMission(
                                       "Awesome", "" + obj[i].rank, " Rewarded " + 10 + " Missiles and " + "Shield Upgrade");
                    GameControler.control.shield = 5;
                    GameControler.control.missles = 10;
                } else
                {
                    ClaimRewardWindow.GetComponent<MissionRewardManager>().setMission(
                             "Awesome", "" + obj[2].rank, "No Reward");
                }
                break;
            case "Come over here!":
                changeObjective(i, 3);
                break;
            case "tatatatatatatatatata":
                changeObjective(i, 4);
                break;
            case "Need For Speed!":
                changeObjective(i, 5);
                break;
            case "Big Weapon Upgrade":
                changeObjective(i, 6);
                break;
            case "Hit me! I am not afraid":
                changeObjective(i, 7);
                break;
        }
    }


}
