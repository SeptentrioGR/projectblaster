﻿using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.IO;

[XmlRoot("MissionsCollection")]
public class MissionContainer {
    [XmlArray("Missions")]
    [XmlArrayItem("Mission")]
    public List<Mission> missions = new List<Mission>();

	public static MissionContainer Load(string path)
    {
        Debug.Log("load " + path);
        TextAsset _xml = Resources.Load<TextAsset>(path);
        XmlSerializer serializer = new XmlSerializer(typeof(MissionContainer));

        StringReader reader = new StringReader(_xml.text);

        MissionContainer items = serializer.Deserialize(reader) as MissionContainer;

        reader.Close();
        return items;
    }


    public static MissionContainer loadPathfile()
    {
        string filename = "/missions.xml";
        Debug.Log("Loading File from" + Application.persistentDataPath + filename);
        if (File.Exists(Application.persistentDataPath + filename))
        {
            BinaryFormatter bf = new BinaryFormatter();
            //Debug.Log("Loading file from " + Application.persistentDataPath + filename);
            FileStream file = File.Open(Application.persistentDataPath + filename, FileMode.Open);
            TextAsset _xml = (TextAsset)bf.Deserialize(file);
            XmlSerializer serializer = new XmlSerializer(typeof(MissionContainer));
            StringReader reader = new StringReader(_xml.text);


            MissionContainer items = serializer.Deserialize(reader) as MissionContainer;
            reader.Close();
            return items;
        }else
        {
            SaveMissionFile("mission");
            return null;
        }
        
    }
   
    public static void SaveMissionFile(string path)
    {
        TextAsset _xml = Resources.Load<TextAsset>(path);
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + path);
        bf.Serialize(file, _xml);
        file.Close();
    }
}
