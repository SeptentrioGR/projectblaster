﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

[Serializable]
public class Mission {
    [XmlAttribute("name")]
    public string name;
    [XmlElement("description")]
    public string desc;
    [XmlElement("objective")]
    public string objct;
}
