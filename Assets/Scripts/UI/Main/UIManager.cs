using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public Sprite pauseIcon;
    public Sprite playIcon;
    public Image imageholder;

    // Update is called once per frame
    void Update()
    {
        if (Game_Manager.gm.gameIsPaused)
        {
            imageholder.sprite = playIcon;
        } else
        {
            imageholder.sprite = pauseIcon;
        }


    }
}
