using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HighScoreScript : MonoBehaviour {

    public Text HScore;
    public Text Score;
    public Text TotalKills;
    public Text CoinsCollected;
    // Use this for initialization
    void Start () {
        HScore.text = "HighScore" + "\n"+ GameControler.control.highscore;
        Score.text = "Score" + "\n" + GameControler.control.score;

    }
}
