using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;// Required when using Event data.
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class MainMenuManager : MonoBehaviour, IPointerEnterHandler// required interface when using the OnPointerEnter method.
{
    //Diffrent Panels in our Main Menu
    [Header("Panels")]
    public GameObject Main;
    public GameObject levels;
    public GameObject shop;
    public GameObject Options;
    public GameObject About;
    public GameObject Score;
    public GameObject shipSelectPanel;
    public GameObject confirmationPane;
    public GameObject IsUnlockedPanel;
    [Header("Buttons")]
    //PlayButton
    public GameObject PlayButton;
    //Reference to our back and quit Buttons
    public GameObject QuitButton;
    public Button BackButton;
    private Button SB;//Survival Button
    public Button upgrades;
    public Toggle AttackModeToggle;

    [Header("Texts")]
    public Text HightScore;
    public Text PreviousMatchText;


    public Slider MusicSfx;
    public Slider SoundSfx;
    public Slider OffsetSlider;
    void Awake()
    {
        if (GameControler.control)
        {
            GameControler.control.LoadGame();
        }
        Time.timeScale = 1f;
        showTitle();

    }

    void Start()
    {
        SB = GameObject.FindGameObjectWithTag("Survival").GetComponent<Button>();
        SB.interactable = true;
        displayQuitWhenAppropiate();

        if (GameControler.GetAutoAttackMode())
        {
            AttackModeToggle.isOn = GameControler.GetAutoAttackMode();
        }
        MusicSfx.value = AudioManager.AudioAPI.GetMusicVolume();
        SoundSfx.value = AudioManager.AudioAPI.GetSoundVolume();
        OffsetSlider.value = GameControler.GetOffset();
    }


    void Update()
    {
        UpdateText();
        if (AudioManager.AudioAPI)
        {
            AudioManager.AudioAPI.setMusicVolume(MusicSfx.value);
            AudioManager.AudioAPI.setSoundVolue(SoundSfx.value);
        } else
        {
            Debug.LogWarning("AudioManager not Found");
        }
        GameControler.setOffset((int)OffsetSlider.value);
    }


    public void ValueChangePlaySound()
    {
        if (!AudioManager.AudioAPI.SoundSource.isPlaying)
        {
            AudioManager.AudioAPI.PlaySound(0);
        }
    }

    void displayQuitWhenAppropiate()
    {
        switch (Application.platform)
        {
            case RuntimePlatform.WindowsPlayer:
                QuitButton.SetActive(true);
                break;
            case RuntimePlatform.Android:
                QuitButton.SetActive(true);
                break;
            default:
                QuitButton.SetActive(true);
                break;
        }
    }

    public void showTitle()
    {
        Animation(Main, "Enter");
    }

    #region Buttons
    //Buttons 
    public void Play()
    {
        if (AudioManager.AudioAPI)
            AudioManager.AudioAPI.PlaySound(1);
        Animation(Main, "Exit");
        Animation(levels, "Enter");
        shipSelectPanel.SetActive(true);
    }
    //Buttons 
    public void Upgrades()
    {
        if (AudioManager.AudioAPI)
            AudioManager.AudioAPI.PlaySound(1);
        Animation(levels, "Exit");
        Animation(shop, "Enter");
        shipSelectPanel.SetActive(false);
    }


    public void Survival()
    {
        if (AudioManager.AudioAPI)
            AudioManager.AudioAPI.PlaySound(1);
        if (GameObject.FindObjectOfType<ShipSelectionManager>().IsUnlocked())
        {
            enableButtons(false);
            loadLevel("02 LoadingGame");
        } else
        {
            IsUnlockedPanel.SetActive(true);
        }

    }


    public void SettingsButton()
    {
        if (AudioManager.AudioAPI)
            AudioManager.AudioAPI.PlaySound(1);
        Animation(Main, "Exit");
        Animation(Options, "Enter");
    }

    public void HighScoreButton()
    {
        if (AudioManager.AudioAPI)
            AudioManager.AudioAPI.PlaySound(1);
        Animation(Main, "Exit");
        Animation(Score, "Enter");
    }

    public void AboutButton()
    {
        if (AudioManager.AudioAPI)
            AudioManager.AudioAPI.PlaySound(1);
        Animation(Main, "Exit");
        Animation(About, "Enter");
    }

    public void ExitButton()
    {
        if (AudioManager.AudioAPI)
            AudioManager.AudioAPI.PlaySound(1);

        GameControler.control.saveGame();
        Application.Quit();
    }

    public void ThisIsBackButton(string panel)
    {
        if (AudioManager.AudioAPI)
            AudioManager.AudioAPI.PlaySound(0);
        switch (panel)
        {
            case "Levels":
                Animation(Main, "Enter");
                Animation(levels, "Exit");
                shipSelectPanel.SetActive(false);
                break;
            case "Options":
                SaveAndExit();
                Animation(Main, "Enter");
                Animation(Options, "Exit");
                break;
            case "score":
                Animation(Main, "Enter");
                Animation(Score, "Exit");
                break;
            case "shop":
                GameControler.control.saveGame();
                GameObject.FindObjectOfType<Upgrade_Manager>().updateUpgrades();
                Animation(levels, "Enter");
                Animation(shop, "Exit");
                shipSelectPanel.SetActive(true);
                break;
            case "about":
                Animation(Main, "Enter");
                Animation(About, "Exit");
                break;
        }
    }

    public void enableButtons(bool value)
    {
        SB.interactable = value;
        BackButton.interactable = value;
        upgrades.interactable = value;
        //GameObject.FindGameObjectWithTag("Defense").GetComponent<Button>().interactable = value;
    }
    #endregion

    public void HideIsUnlockedPanel()
    {
        IsUnlockedPanel.SetActive(false);
    }



    public void Animation(GameObject panel, string value)
    {
        panel.GetComponent<Animator>().SetTrigger(value);
    }



    public void loadLevel(string level)
    {
        if (AudioManager.AudioAPI)
            AudioManager.AudioAPI.PlaySound(1);
        StartCoroutine(LevelLoad(level));
    }

    IEnumerator LevelLoad(string level)
    {

        AsyncOperation async = SceneManager.LoadSceneAsync(level);
        while (!async.isDone)
        {
            yield return null;
        }
        yield return async;

    }

    public void Confirmation(int value)
    {
        if (value == 0)
        {
            confirmationPane.SetActive(false);
        } else if (value == 1)
        {
            confirmationPane.SetActive(true);
        } else
        {
            Debug.LogWarning("Confirmation Button value out of bound");
        }

    }

    public void DeleteData()
    {
        GameControler.control.deleteFiles();
        GameControler.control.ResetToDefault();
        if (AudioManager.AudioAPI)
            AudioManager.AudioAPI.SetDefaults();
        if (AudioManager.AudioAPI)
            MusicSfx.value = AudioManager.AudioAPI.GetMusicVolume();
        if (AudioManager.AudioAPI)
            SoundSfx.value = AudioManager.AudioAPI.GetSoundVolume();
    }


    public void SaveData()
    {
        GameControler.control.SaveGameData();
        GameControler.control.SavePlayerData();
        GameControler.control.SavePlayerUpgrades();
    }

    public void LoadData()
    {
        GameControler.control.LoadPlayerUpgrades();
        GameControler.control.LoadGameData();
        GameControler.control.LoadPlayerData();
    }


    //Do this when the cursor enters the rect area of this selectable UI object.
    public void OnPointerEnter(PointerEventData eventData)
    {
        Debug.Log("The cursor entered the selectable UI element.");
    }


    public void SaveAndExit()
    {
        GameControler.SetMusicVolume(MusicSfx.value);
        GameControler.SetSoundVolume(SoundSfx.value);
        if (AttackModeToggle.isOn)
        {
            GameControler.setAutoAttackMode(0);
        } else if (!AttackModeToggle.isOn)
        {
            GameControler.setAutoAttackMode(1);
        }


    }


    public void UpdateText()
    {
        PreviousMatchText.text = GameControler.control.score.ToString("00000000");
        HightScore.text = GameControler.control.highscore.ToString("00000000");
    }


    public void AttackModeButton()
    {
        AttackModeToggle.isOn = !AttackModeToggle.isOn;
    }

}


