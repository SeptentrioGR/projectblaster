using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HowToPlayScript : MonoBehaviour
{

    public List<GameObject> pages = new List<GameObject>();//Pages of content
    public int curPage; //The current Page showned in Screen
    public Vector2 off_pos;//offScreenPosition
    public Vector2 onS_pos;//onScreenPosition
    public GameObject nextButton;
    public GameObject previousButton;
    public Text numbPages;//Number of Pages Text


    void Start()
    {
        curPage = 0;
        for (int i = 0; i < pages.Capacity; i++)
        {
            pages[i].gameObject.SetActive(false);
        }
        adjustPages();
    }

    public void Update()
    {
        numbPages.text = curPage + 1 + "/" + pages.Capacity;

        for (int i = 0; i < pages.Capacity; i++)
        {
            pages[i].gameObject.SetActive(false);
        }
        pages[curPage].gameObject.SetActive(true);
        if (curPage <= 0)
        {
            previousButton.SetActive(false);
        } else
        {
            previousButton.SetActive(true);
        }
        if (curPage >= pages.Capacity - 1)
        {
            nextButton.SetActive(false);
        } else
        {
            nextButton.SetActive(true);
        }
    }

    public void adjustPages()
    {





    }

    public void nextPage()
    {
        AudioManager.AudioAPI.PlaySound(1);
        adjustPages();
        curPage++;
    }

    public void prevPage()
    {
        AudioManager.AudioAPI.PlaySound(0);
        adjustPages();
        curPage--;
    }

}
