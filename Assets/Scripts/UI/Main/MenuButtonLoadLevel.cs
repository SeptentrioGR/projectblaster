using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuButtonLoadLevel : MonoBehaviour
{
    public string level;

    public AudioClip click;

    public void loadLevel()
    {

        StartCoroutine(LoadYourLevel(level));
    }

    public void loadLevel(string text)
    {
        Camera.main.GetComponent<AudioSource>().PlayOneShot(click, GameControler.GetSoundVolume());
        StartCoroutine(LoadYourLevel(text));

    }

    IEnumerator LoadYourLevel(string level)
    {
        AsyncOperation async = SceneManager.LoadSceneAsync(level);
        while (!async.isDone)
        {
            yield return null;
        }
        yield return async;
    }
}
