using UnityEngine;
using UnityEngine.UI;

public class OptionsScript : MonoBehaviour
{

    [Header("Audio Sliders")]
    public Slider Music;
    public Slider Sound;

    // Update is called once per frame
    void Update()
    {
        Camera.main.GetComponent<AudioSource>().volume = GameControler.GetMusicVolume();
        GameControler.SetMusicVolume(Music.value);
        GameControler.SetSoundVolume(Sound.value);
    }
}
