using UnityEngine;
using UnityEngine.UI;

public class Upgrade_Manager : MonoBehaviour
{
    [Header("Upgrade General Settings")]
    //Coin Text
    public Text Coins;

    #region Increase Value Each Upgrade
    //How much missiles do i get with each upgrade
    public int mMissilesIncriment;
    //How much Magent power do i get with each upgrade
    public float mMagnetIncriment;
    //How much Speed Power do i get with each upgrade
    public float mSpeedIncriment;
    //How fast do i shoot
    public float mFireRateIncriment;
    //How much shield do i get
    public int mShieldIncriment;
    #endregion

    [Header("Missles Upgrade Settings")]
    #region Variables for Missiles
    public Button MissleButton; // Buy Button
    public Text Missle; //Text for how many Missiles do i have 
    public Text MissleCostText;//Text that shows how expensive missles are
    public GameObject MissleUpgradePoints; //How many missiles do i have currently in UI
    #endregion

    [Header("Magnet Upgrade Settings")]
    #region For Magnet Upgrade
    public Button MagnetButton; //The buttin that increase the magnet count
    public Text MagnetCostText; //The Text of How much does the Magnet Power Cost
    public GameObject[] MagnetUpgradePoints;//Visua
    #endregion

    [Header("Speed Upgrade Settings")]
    #region WeaponSpeed Upgrades Settings
    public Button buySpeedButton;//The button to buy more speed
    public Text SpeedCostText;//The text that shows the cost of the speed upgrade
    public GameObject[] SpeedUpgradePoints;//Visual bar for how many upgrade i have updated and how much i havent
    #endregion

    [Header("Fire Rate")]
    #region For Weapon Upgrade Shield
    public Button rateOfFireButton;//The button to buy the upgrade to increase the fire rate of your weapons
    public Text rateOfFireCostText;//The text that shows how much fire rate cost 
    public GameObject[] rateOfFreupgradePoints;//Visual bar for how much upgrade I have and how much I havent.
    #endregion

    [Header("Shield Upgrade")]
    #region Shield Upgrade Settings
    public Button ShieldBuyButton;//The Buy Button for Shield
    public Text ShieldCountText;//How much shield I have today Text
    public Text ShieldCostText;//The text of how much does shield cost
    #endregion

    void Start()
    {
        updateUpgrades();
    }


    // Update is called once per frame
    //Update The Texts
    void Update()
    {
        if (Coins)
        {
            Coins.text = "" + GameControler.control.coins;//Update your coins Text to show the player how much money he has
        }
        #region Update the Shield Text

        ShieldCostText.text = "" + GameControler.control.upgradeCostShield;
        if (GameControler.control.ShieldCount <= 5)
        {
            ShieldCostText.fontSize = 33;
            ShieldCostText.text = " out of stock";
        }
        #endregion



        #region Update the Missiles Upgrade Text
        MissleCostText.text = "" + GameControler.control.upgradeCostMissle;
        if (GameControler.control.missles > 50)
        {
            MissleCostText.fontSize = 33;
            MissleCostText.text = " out of stock";
        }
        #endregion

        #region Update the Shield Upgrade Text
        ShieldCostText.text = "" + GameControler.control.upgradeCostShield;
        if (GameControler.control.shield >= 5)
        {
            ShieldCostText.fontSize = 33;
            ShieldCostText.text = "out of stock";
        }
        #endregion

        #region Update the Fire Rate Upgrade Text
        if (GameControler.control.FireRateCount <= 4)
        {
            rateOfFireCostText.text = "" + GameControler.control.upgradeCostRateOfFire;
        } else
        {
            rateOfFireCostText.fontSize = 33;
            rateOfFireCostText.text = " out of stock";
        }
        #endregion

        #region Update the Speed Upgrade Text
        SpeedCostText.text = "" + GameControler.control.upgradeCostSpeed;
        if (GameControler.control.SpeedCount > 4)
        {
            SpeedCostText.fontSize = 33;
            SpeedCostText.text = " out of stock";
        }
        #endregion

        #region Update the Magnet Upgrade Text
        MagnetCostText.text = "" + GameControler.control.upgradeCostMagnet;
        if (GameControler.control.MagnetCount > 4)
        {
            MagnetCostText.fontSize = 33;
            MagnetCostText.text = " out of stock";
        }
        #endregion

        Missle.text = "" + GameControler.control.missles;

        if (GameControler.control.shield == 0)
        {
            ShieldCountText.text = "Inactive";
        } else if (GameControler.control.shield < 5)
        {

            ShieldCountText.text = "Repair Shield";
        } else
        {

            ShieldCountText.text = "Shield Active";
        }

    }

    public void updateUpgrades()
    {

        for (int i = 0; i <= 4; i++)
        {
            rateOfFreupgradePoints[i].SetActive(false);
            SpeedUpgradePoints[i].SetActive(false);
            MagnetUpgradePoints[i].SetActive(false);
        }

        for (int i = 0; i < GameControler.control.FireRateCount; i++)
        {
            rateOfFreupgradePoints[i].SetActive(true);
        }

        for (int i = 0; i < GameControler.control.MagnetCount; i++)
        {
            MagnetUpgradePoints[i].SetActive(true);
        }
        for (int i = 0; i < GameControler.control.SpeedCount; i++)
        {
            SpeedUpgradePoints[i].SetActive(true);
        }



    }



    #region Buttons
    /**
     * The method that increase Missiles
     **/
    public void buyMissile()
    {
        if (AudioManager.AudioAPI)
        {
            AudioManager.AudioAPI.PlaySound(1);
        } else
        {
            Debug.LogWarning("AudioManager Does not Exist");
        }
        Missle.text = "" + GameControler.control.missles;
        if (GameControler.control.coins >= GameControler.control.upgradeCostMissle && GameControler.control.missles <= 75)
        {
            updateGameManagerCoins(GameControler.control.upgradeCostMissle);
            GameControler.control.missles += mMissilesIncriment;


        } else
        {
            MissleCostText.text = " out of stock";
        }
    }

    /**
     * The method that increase the shield
     **/
    public void buyShield()
    {
        if (AudioManager.AudioAPI)
        {
            AudioManager.AudioAPI.PlaySound(1);
        } else
        {
            Debug.LogWarning("AudioManager Does not Exist");
        }
        updateUpgrades();
        if (GameControler.control.coins >= GameControler.control.upgradeCostShield && GameControler.control.shield < 5)
        {
            updateGameManagerCoins(GameControler.control.upgradeCostShield);
            GameControler.control.shield = 5;
            GameControler.control.upgradeCostShield *= 2;
            ShieldCountText.text = " is Active";
            MissleCostText.text = " out of stock";

        }
    }

    /**
   * The method that increase the Weapon Fire Rate of the Player
   **/
    public void buyFireRateUpgrade()
    {
        if (AudioManager.AudioAPI)
        {
            AudioManager.AudioAPI.PlaySound(1);
        } else
        {
            Debug.LogWarning("AudioManager Does not Exist");
        }
        updateUpgrades();
        if (GameControler.control.coins >= GameControler.control.upgradeCostRateOfFire && GameControler.control.FireRateCount <= 4)
        {
            updateGameManagerCoins(GameControler.control.upgradeCostRateOfFire);
            GameControler.control.FireRate += mFireRateIncriment;
            GameControler.control.upgradeCostRateOfFire *= 2;
            GameControler.control.FireRateCount++;
            rateOfFreupgradePoints[GameControler.control.FireRateCount - 1].SetActive(true);

        }
    }

    /**
   * The method that increase the Speed
   **/
    public void buySpeed()
    {
        if (AudioManager.AudioAPI)
        {
            AudioManager.AudioAPI.PlaySound(1);
        } else
        {
            Debug.LogWarning("AudioManager Does not Exist");
        }
        updateUpgrades();
        //If you have coins to pay for the upgrade and if speedcount is less or equal to 5
        if (GameControler.control.coins >= GameControler.control.upgradeCostSpeed && GameControler.control.SpeedCount <= 4)
        {
            updateGameManagerCoins(GameControler.control.upgradeCostSpeed);
            GameControler.control.speed += mSpeedIncriment;
            GameControler.control.upgradeCostSpeed *= 2;
            GameControler.control.SpeedCount++;
            SpeedUpgradePoints[GameControler.control.SpeedCount - 1].SetActive(true);

        }
    }

    /**
   * The method that increase the Magnet Power of the ship
   **/
    public void buyMagnet()
    {
        if (AudioManager.AudioAPI)
        {
            AudioManager.AudioAPI.PlaySound(1);
        } else
        {
            Debug.LogWarning("AudioManager Does not Exist");
        }
        updateUpgrades();
        if (GameControler.control.coins >= GameControler.control.upgradeCostMagnet && GameControler.control.MagnetCount <= 4)
        {
            updateGameManagerCoins(GameControler.control.upgradeCostMagnet);
            GameControler.control.magneticPower += mMagnetIncriment;
            GameControler.control.upgradeCostMagnet *= 2;
            GameControler.control.MagnetCount++;
            MagnetUpgradePoints[GameControler.control.MagnetCount - 1].SetActive(true);

        }
    }
    #endregion

    public void updateGameManagerCoins(int Cost)
    {

        GameControler.control.coins -= Cost;
        if (GameControler.control.coins < 0)
        {
            GameControler.control.coins = 0;
        }

    }



}
