using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class PausedButton : MonoBehaviour, IPointerClickHandler
{

    Rect screenButtonRect;

    //Pause Game UI

    [Header("Pause Game Settings")]
    public GameObject pausedMenu;
    private bool isPaused;
    //Text when in Defense mode to notify the player of the timer;

    public GameObject pauseButton;
    //bool isEnabled;

    public void OnPointerClick(PointerEventData eventData)
    {
        pauseButton.SetActive(true);

    }
    //void setIsEnabled(bool value)
    //{
    //    isEnabled = value;
    //}
}
