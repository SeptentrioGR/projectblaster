using UnityEngine;
using UnityEngine.UI;

public class ShipSelectionManager : MonoBehaviour
{
    [Range(0, 2)]
    public static int curShip = 0;
    public GameObject LevelSelectionPanel;
    public GameObject[] ships;
    public Image LockedIcon;
    public Text UnlockedText;
    public bool Menu;
    // Use this for initialization
    void Start()
    {
        curShip = GameControler.control.currentShip;
        placeShipInFront(curShip);
    }

    private void FixedUpdate()
    {
        if (Menu)
        {
            LockedIcon.gameObject.SetActive(true);

            if ((curShip == 1) && LockedIcon.gameObject.activeSelf)
            {
                UnlockedText.text = "Unlocked at Killing Objective Rank 8";
            }
            if (curShip == 2 && LockedIcon.gameObject.activeSelf)
            {
                UnlockedText.text = "Unlocked at Survival Objective Rank 12";
            }

            if (curShip == 0 || GameControler.control.RankInKilling >= 8 && curShip == 1 || GameControler.control.RankInSurvival >= 12 && curShip == 2)
            {
                LockedIcon.gameObject.SetActive(false);
            }

        }
    }

    public bool IsUnlocked()
    {
        if (LockedIcon.gameObject.activeSelf)
        {
            return false;
        } else
        {
            AudioManager.AudioAPI.PlaySound(1);
            return true;
        }
    }

    public void placeShipInFront(int value)
    {
        foreach (GameObject go in ships)
        {
            go.SetActive(false);
        }
        ships[value].SetActive(true);
        GameControler.control.currentShip = curShip;
    }
    public void nextShip()
    {
        AudioManager.AudioAPI.PlaySound(1);
        if (curShip < 2)
        {
            curShip += 1;
        }


        placeShipInFront(curShip);
    }

    public void previousShip()
    {
        AudioManager.AudioAPI.PlaySound(0);
        if (curShip > 0)
        {
            curShip -= 1;
        }
        placeShipInFront(curShip);
    }
}
