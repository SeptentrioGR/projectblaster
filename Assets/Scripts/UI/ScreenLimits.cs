using UnityEngine;
using System.Collections;
public class ScreenLimits : MonoBehaviour
{
    public BoxCollider2D box;


    void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        box.offset = transform.position;
        Gizmos.DrawWireCube(box.offset, box.size);
    }

    void OnDrawGizmosSelected()
    {
        // Display the explosion radius when selected
        Gizmos.color = Color.white;
        Gizmos.DrawWireCube(box.offset, box.size);
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag != "Player")
        {
            collision.gameObject.SetActive(false);
        }
    }

    public void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag != "Player")
        {
           
        }
    }
}
