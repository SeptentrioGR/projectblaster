using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class OnMouseClickEvent : MonoBehaviour
{
    public GameObject Window;
    public string window1;
    public string window2;
    public string window3;
    private Vector2 touchDeltaPosition;
    private Vector2 mousePosition;
    private GameObject player;//The player game objecrt
    private Player_Move pmove;//Player_Move
                              // Use this for initialization



    void Start()
    {
        pmove = GameObject.FindGameObjectWithTag("Player").GetComponent<Player_Move>();
    }

    void Update()
    {
        mousePosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        if (Input.touchCount == 1)
        {
            touchDeltaPosition = new Vector2(Input.GetTouch(0).position.x, Screen.height - Input.GetTouch(0).position.y);

        }


        if (RaycastWorldUI(mousePosition))
        {
            Game_Manager.gm.playerCanMove = true;
        } else
        {
            Game_Manager.gm.playerCanMove = false;
        }
    }



    //Checks if the touch or mouse position is over a UI and returns true or false to be used to stop the 
    //movement of the camera when we try to interact with the UI in the game.
    public bool RaycastWorldUI(Vector2 position)
    {
        PointerEventData pointerData = new PointerEventData(EventSystem.current);

        pointerData.position = position;

        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(pointerData, results);

        if (results.Count > 0)
        {
            //WorldUI is my layer name
            if (results[0].gameObject.layer == LayerMask.NameToLayer("UI"))
            {


                return false;

                //string dbg = "Root Element: {0} \n GrandChild Element: {1}";
                //Debug.Log(string.Format(dbg, results[results.Count - 1].gameObject.name, results[0].gameObject.name));


            } else
            {

                return true;
            }

        }
        results.Clear();
        return true;
    }

}
