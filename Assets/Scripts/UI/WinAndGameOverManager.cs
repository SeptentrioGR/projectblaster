using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class WinAndGameOverManager : MonoBehaviour
{

    public Text HighScore;
    public Text Score;
    public Text Credits;

    //Update is called once per frame
    void Update()
    {
            HighScore.text = GameControler.control.highscore.ToString("00000000");
            Score.text = GameControler.control.score.ToString("00000000");
            Credits.text = GameControler.control.coins.ToString("00");   
    }

    public void BackButton(int numb)
    {
        GameControler.control.saveGame();
        SceneManager.LoadScene(numb);
    }


}
