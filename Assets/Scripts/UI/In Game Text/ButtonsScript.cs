using System.Collections;
using UnityEngine;

public class ButtonsScript : MonoBehaviour
{

    [Header("Sound")]
    public AudioClip exitSfx;


    public void saveButton()
    {
        GameControler.control.saveGame();
        Camera.main.GetComponent<AudioSource>().PlayOneShot(exitSfx, GameControler.GetSoundVolume());
    }

    public void loadButton()
    {
        GameControler.control.LoadGame();
        Camera.main.GetComponent<AudioSource>().PlayOneShot(exitSfx, GameControler.GetSoundVolume());

    }

    public void backButton()
    {
        Camera.main.GetComponent<AudioSource>().PlayOneShot(exitSfx, GameControler.GetSoundVolume());
    }

    public void exitButton()
    {
        Camera.main.GetComponent<AudioSource>().PlayOneShot(exitSfx, GameControler.GetSoundVolume());
        GameControler.control.saveGame();
        StartCoroutine(LevelLoad());
    }

    IEnumerator LevelLoad()
    {
        yield return new WaitForSeconds(.5f);
        Application.Quit();
    }

}
