using UnityEngine;
using UnityEngine.UI;

public class FloatingTextScript : MonoBehaviour
{

    public Text mFloatingText;
    public string[] congrats;
    public int fontSize;


    public void setFloatingText(string text)
    {
        if (Game_Manager.gm)
        {
            if (Game_Manager.gm.killedEnemiesWithoutGettingHit >= 5)
            {
                mFloatingText.text = string.Format((congrats[Random.Range(0, congrats.Length)] + "\n{0}"), text);
                Game_Manager.gm.killedEnemiesWithoutGettingHit = 0;
            } else
            {
                mFloatingText.text = text;

            }
        }

    }
}
