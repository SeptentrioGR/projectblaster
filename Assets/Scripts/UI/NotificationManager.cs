using UnityEngine;
using System.Collections;


public class NotificationManager : MonoBehaviour {
    public GameObject[] speachbar;
    public GameObject[] NotificationLocations;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}



    public  void createNotification()
    {
        GameObject notification = Instantiate(speachbar[0]) as GameObject;
        notification.transform.position = NotificationLocations[0].transform.position;
        notification.transform.SetParent(NotificationLocations[0].transform);
    }
}
