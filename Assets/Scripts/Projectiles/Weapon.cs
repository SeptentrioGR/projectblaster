using UnityEngine;
public class Weapon : MonoBehaviour
{
    public GameObject cannonPrefab;
    public Transform[] cannonsPoints;

    public Transform target;
    public float Damage;
    //public int numberOfBullets;
    //public bool shoot;
    private GameObject weapons;
    public GameObject MissilePrefab;
    public bool hasMissile;
    public int numberOfMissile;
    public int MissileDamage;
    public Transform[] missilePoints;
    // Use this for initialization
    void Start()
    {
        if (GameObject.FindGameObjectWithTag("Player"))
        {
            target = GameObject.FindGameObjectWithTag("Player").transform;
        } else
        {
            Debug.LogWarning("Player is not found or is dead");
        }
        if (GameObject.FindGameObjectWithTag("BulletsAndWeapon"))
        {
            weapons = GameObject.FindGameObjectWithTag("BulletsAndWeapon");
        } else
        {
            Debug.LogWarning("BulletsAndWeapons Don't exist");
        }
        FireEnemyBullet();
        if (hasMissile)
        {
            FireMissile();
        }
    }

    void FireMissile()
    {

        if (numberOfMissile == 1)
        {
            GameObject instance = Instantiate(MissilePrefab, missilePoints[0].position, missilePoints[0].rotation) as GameObject;
            if (instance.GetComponent<Projectle>())
            {
                instance.GetComponent<Projectle>().setDamage(MissileDamage);
            }

            instance.transform.parent = weapons.transform;
        } else if (numberOfMissile == 2)
        {
            GameObject instance = Instantiate(MissilePrefab, missilePoints[1].position, missilePoints[1].rotation) as GameObject;
            if (instance.GetComponent<Projectle>())
            {
                instance.GetComponent<Projectle>().setDamage(MissileDamage);
            }

            instance.transform.parent = weapons.transform;
            GameObject instance2 = Instantiate(MissilePrefab, missilePoints[2].position, missilePoints[2].rotation) as GameObject;
            if (instance.GetComponent<Projectle>())
            {
                instance.GetComponent<Projectle>().setDamage(MissileDamage);
            }

            instance.transform.parent = weapons.transform;
            instance2.transform.parent = weapons.transform;
        } else if (numberOfMissile == 3)
        {
            GameObject instance = Instantiate(MissilePrefab, missilePoints[0].position, missilePoints[0].rotation) as GameObject;
            if (instance.GetComponent<Projectle>())
            {
                instance.GetComponent<Projectle>().setDamage(MissileDamage);
            }

            instance.transform.parent = weapons.transform;
            GameObject instance2 = Instantiate(MissilePrefab, missilePoints[1].position, missilePoints[1].rotation) as GameObject;
            if (instance2.GetComponent<Projectle>())
            {
                instance2.GetComponent<Projectle>().setDamage(MissileDamage);
            }

            instance.transform.parent = weapons.transform;
            GameObject instance3 = Instantiate(MissilePrefab, missilePoints[2].position, missilePoints[2].rotation) as GameObject;
            if (instance3.GetComponent<Projectle>())
            {
                instance3.GetComponent<Projectle>().setDamage(MissileDamage);
            }

            instance.transform.parent = weapons.transform;
            instance2.transform.parent = weapons.transform;
            instance3.transform.parent = weapons.transform;
        }
        Destroy(gameObject, 1f);
    }


    void FireEnemyBullet()
    {
        for (int i = 0; i < cannonsPoints.Length; i++)
        {
            GameObject instance = Instantiate(cannonPrefab, cannonsPoints[i].position, cannonsPoints[i].rotation) as GameObject;
            if (instance.GetComponent<Enemy_Bullets>())
            {
                Vector2 direction;
                if (target)
                {
                    direction = target.position - instance.transform.position;
                } else
                {
                    direction = Vector3.down;
                }
                instance.GetComponent<Enemy_Bullets>().SetDirection(direction);
                instance.GetComponent<Enemy_Bullets>().setDamageToProjectiles(Damage);
            }

            if (instance.GetComponent<Projectle>())
            {
                instance.GetComponent<Projectle>().setDamage(Damage);
            }

            instance.transform.parent = weapons.transform;
        }
        Destroy(gameObject, 1f);
    }

    public void setParentOfThisObject(GameObject o)
    {
        weapons = o;
    }


}
