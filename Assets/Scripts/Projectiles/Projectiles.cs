using UnityEngine;

public class Projectiles : MonoBehaviour
{
    [Range(1, 45)]
    public float speed;
    public float damage;

    public float xvel;
    public float yvel;

    public GameObject explosion;
    public AudioClip Hitsfx;
    public AudioClip exlpodeSfx;

    public void setDamage(float newDamage)
    {
        damage = newDamage;
    }

    public float DealDamage()
    {
        return damage;
    }



}
