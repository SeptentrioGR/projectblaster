using System.Collections;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public enum Type { simple, follow }
    public Type type;
    public Projectiles projectile;

    //Missiles
    public string searchTag;
    private GameObject closetMissiles;
    private Transform target;
    public GameObject missileExpObject;
    private Vector3 lastKnownLocation;
    //Rigidbody2D rigid;
    public float direciton;
    bool isDone;
    Vector2 min, max;

    void Start()
    {
        if (GetComponent<Projectiles>())
        {
            projectile = GetComponent<Projectiles>();
        } else
        {
            Debug.LogWarning("Projectiles Does not exist");
        }

        if (target == null)
        {
            closetMissiles = FindClosestEnemy();

        }
    }



    // Update is called once per frame
    void Update()
    {


        // rigid = GetComponent<Rigidbody2D>();
        if (type == Type.simple)
        {
            Move();
        } else if (type == Type.follow)
        {
            if (!target)
            {

                if (closetMissiles)
                {
                    target = closetMissiles.transform;
                }

            } else if (target)
            {

                LookAtTarget();

            }

            transform.Translate(Vector3.up * 10f * Time.deltaTime);
        }

    }

    void LookAtTarget()
    {
        if (target.gameObject.GetComponent<EnemyVisibilityCheck>())
        {
            if (target.gameObject.GetComponent<EnemyVisibilityCheck>().CheckIfPositionReseted())
            {
                DestoryNow();
                Destroy(gameObject);
            }
        }

        Vector3 diff = target.transform.position - transform.position;
        diff.Normalize();

        float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0f, 0f, rot_z - 90);
    }

    GameObject FindClosestEnemy()
    {
        GameObject[] gos;
        gos = GameObject.FindGameObjectsWithTag(searchTag);
        GameObject closest = null;
        float distance = Mathf.Infinity;
        foreach (GameObject go in gos)
        {
            Vector3 diff = go.transform.position - transform.position;
            float curDistance = diff.sqrMagnitude;
            if (curDistance < distance)
            {
                closest = go;
                distance = curDistance;
            }
        }
        return closest;
    }

    void Move()
    {
        //rigid.velocity = new Vector2(direciton * projectile.speed, projectile.speed);
        transform.position += transform.up * projectile.speed * Time.deltaTime;

        //if (!isDone)
        //{
        //    isDone = !isDone;
        //    StartCoroutine(stopMovement());
        //}
        Vector3 max = Camera.main.ViewportToWorldPoint(new Vector2(1, 1));
        if (transform.position.y > max.y)
        {
            Destroy(gameObject);
        }
    }

    IEnumerator stopMovement()
    {
        yield return new WaitForSeconds(.3f);
        direciton = 0;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        GameObject obj = other.gameObject;
        if (obj.GetComponent<Enemy_Ship>())
        {
            DestoryNow();
        }

        if (obj.GetComponent<Rock>())
        {
            DestoryNow();
            Destroy(gameObject);
        }
    }

    public void DestoryNow()
    {
        if (projectile.explosion)
        {
            GameObject Explosion = Instantiate(projectile.explosion, transform.position, Quaternion.identity) as GameObject;
            Explosion.transform.SetParent(GameObject.FindGameObjectWithTag("BulletsAndWeapon").transform);
            Destroy(Explosion, 1f);
        }
        if (projectile.exlpodeSfx)
        {
            Camera.main.GetComponent<AudioSource>().PlayOneShot(projectile.exlpodeSfx, GameControler.GetSoundVolume());
        }
        Destroy(gameObject);
    }

    public void setDamageToProjectiles(float damage)
    {
        projectile.setDamage(damage);
    }

    public float DealDamage()
    {
        return projectile.DealDamage();
    }
}
