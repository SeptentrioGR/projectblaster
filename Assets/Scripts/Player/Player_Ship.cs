using System.Collections;
using UnityEngine;

public class Player_Ship : MonoBehaviour
{
    [Header("General Settings")]
    [HideInInspector]
    public bool isAlive;
    [HideInInspector]
    public bool lowHealthPlayed; //Low Health Sound 
    public GameObject[] ships; //Diffrent ships
    [HideInInspector]
    public int curship; //CurrentShip

    [Header("Effects")]
    public GameObject hitEffect;//The effect when bulles hit the ship;
    public GameObject explosionPrefab;//The effect when the player dies
    public GameObject cannonPrefab;//The weapon prefab
    public GameObject misslePrefab;//The missle prefab
    public ParticleSystem[] LeftshootEffect;//Effects on Left Cannon when you shoot
    public ParticleSystem[] RightshootEffect;//Effects on Right Cannon when you shoot
    public ParticleSystem[] shieldSfx;//Shield Effect enable it when you have shield

    [Header("Sounds")]
    private AudioSource mainSource;
    public AudioClip shotSfx;//The sound when the player shoots
    public AudioClip Hitsfx;//The sound when enemy projectiles hit the ship
    public AudioClip exlpodeSfx;//The sound when the player ship explodes
    public AudioClip lowHealthWarningSfx;//Audio when the health is low to notify the player that his health is low
    public AudioClip shieldBrokeSfx; //Sound when the Shield goes off

    [Header("Upgrades")]
    [HideInInspector]
    public bool missleUpgrade;//Missiles is Available
    [HideInInspector]
    public bool ShieldUpgrade;//Shield is Available


    [Header("Stats Configuration")]
    [HideInInspector]
    public float startingHealth;
    public float shieldCapacity;
    public float currentHealth;

    [Header("Weapon Configuration")]
    private float CannonDamage;
    public float fireRate;
    [HideInInspector]
    public float nextFire = 0.0F;
    [HideInInspector]
    public float RocketFireRate = 1.0f;
    [HideInInspector]
    public float nextRocketFire = 0.0F;
    [HideInInspector]
    public bool playerGotHit;
    [HideInInspector]
    public bool isDamaged;
    [HideInInspector]
    public bool protectionFromAttacks;


    public void Start()
    {
        setPlayerModel();
        isAlive = true;//set Alive to true
        if (GameControler.control)
        {
            startingHealth = GameControler.control.Health;//starting health
        } else
        {
            startingHealth = 100;
        }
        currentHealth = startingHealth;//Health
        fireRate = 0.8f;
        UpdateShipStats();


    }
    public void UpdateShipStats()
    {
        CannonDamage = GameControler.control.Damage;//Damage
        shieldCapacity = GameControler.control.shield;//Shield

    }
    public void Update()
    {
        UpdateShipStats();
        Health();
        Shield();


    }

    void Health()
    {
        if (currentHealth < startingHealth * 0.20f && !lowHealthPlayed && isAlive)
        {
            lowHealthPlayed = true;
            GetComponent<AudioSource>().PlayOneShot(lowHealthWarningSfx, GameControler.GetSoundVolume());
            StartCoroutine(playLowHealth());
        }

        if (currentHealth <= 0 && isAlive)
        {
            Death();
        }

    }

    void Shield()
    {
        if (GameControler.control.shield > 0 && !ShieldUpgrade)
        {
            shieldSfx[curship].Play();
            ShieldUpgrade = true;

        } else if (GameControler.control.shield <= 0 && ShieldUpgrade)
        {
            shieldSfx[curship].Stop();
            GameControler.control.shield = 0;
            ShieldUpgrade = false;

        }
    }



    void Death()
    {
        isAlive = !isAlive;
        if (exlpodeSfx)
        {
            Camera.main.GetComponent<AudioSource>().PlayOneShot(exlpodeSfx, GameControler.GetSoundVolume());
        }
        if (explosionPrefab)
        {
            GameObject Explosion = Instantiate(explosionPrefab, transform.position, Quaternion.identity) as GameObject;
            Destroy(Explosion, 1f);
        }
        gameObject.SetActive(false);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        GameObject obj = other.gameObject;
        if (shieldCapacity <= 0 && !protectionFromAttacks && obj.GetComponent<Enemy_Ship>() || obj.GetComponent<Enemy_Bullets>())
        {
            if (GameControler.control.weaponUpgrade > 0 && !protectionFromAttacks)
            {
                GameControler.control.weaponUpgrade--;
            }
        }


        if (!protectionFromAttacks && other.gameObject.tag == "Enemies")
        {
            TakeDamage(startingHealth / 2);
            other.gameObject.SetActive(false);
            other.gameObject.GetComponent<Enemy_Ship>().onDeath();
        }



        if (!protectionFromAttacks && other.gameObject.tag == "Asteroids")
        {
            TakeDamage(currentHealth);
            Destroy(other.gameObject);
        }

    }

    public void healShip(int value)
    {
        if (currentHealth < GameControler.control.Health)
        {
            currentHealth += value;
            if (currentHealth > startingHealth)
            {
                currentHealth = startingHealth;
            }
        }
    }

    public void PlayDamagedAnimation()
    {
        ships[curship].GetComponent<Animator>().SetTrigger("Damaged");
    }

    public void PlayExitAnimation()
    {
        ships[curship].GetComponent<Animator>().SetTrigger("Exit");
    }

    public void TakeDamage(float amount)
    {
        if (!protectionFromAttacks)
        {
            playerGotHit = true;

            PlayDamagedAnimation();
            shieldCapacity = GameControler.control.shield;
            if (currentHealth <= 0)
            {
                currentHealth = 0;
            }
            if (shieldCapacity > 0)
            {
                shieldCapacity -= amount * 2;
            } else if (shieldCapacity <= 0)
            {
                shieldCapacity = 0;

                currentHealth -= amount;
            }

            GameControler.control.shield = shieldCapacity;
            playerGotHit = false;
        }
    }


    public void invulnerability(int value)
    {
        if (value == 0)
        {
            protectionFromAttacks = false;
            GetComponent<PlayerShoot>().canShoot = true;
        } else if (value == 1)
        {

            protectionFromAttacks = true;
            GetComponent<PlayerShoot>().canShoot = false;
        } else
        {
            Debug.LogError("Invulnerability value is out of bound");
        }
    }

    IEnumerator playLowHealth()
    {
        yield return new WaitForSeconds(16f);
        lowHealthPlayed = false;
    }

    public void setPlayerModel()
    {
        curship = ShipSelectionManager.curShip;
        foreach (GameObject go in ships)
        {
            go.SetActive(false);
        }
        ships[curship].SetActive(true);
    }

    public void setFireRate(float value)
    {
        fireRate = value;
    }


}
