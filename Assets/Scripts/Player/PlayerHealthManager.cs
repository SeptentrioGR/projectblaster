using UnityEngine;
using UnityEngine.UI;

public class PlayerHealthManager : MonoBehaviour
{
    [Header("Health Bar Settings")]
    public GameObject HealthBar;
    public Slider Slider;
    public Image SliderFill;
    public Color fullHealthColor = Color.green;
    public Color zeroHealthColor = Color.red;

    private GameObject player;
    private Player_Ship shipManager;
    // Use this for initialization
    void Start()
    {
        shipManager = GameObject.FindGameObjectWithTag("Player").GetComponent<Player_Ship>();
    }

    // Update is called once per frame
    void Update()
    {
        updateHealthBars();
    }

    public void updateHealthBars()
    {
        Slider.transform.GetChild(2).gameObject.GetComponent<Image>().enabled = shipManager.protectionFromAttacks;
        if (Slider && SliderFill)
        {
            Slider.value = shipManager.currentHealth;
            Slider.maxValue = shipManager.startingHealth;
            SliderFill.color = Color.Lerp(zeroHealthColor, fullHealthColor, shipManager.currentHealth / shipManager.startingHealth);
        }
    }
}
