using UnityEngine;

public class Player_Move : MonoBehaviour
{
    private Camera mCamera;
    //private BoxCollider[] myColliders;

    public bool canMove;
    public float moveSpeed;//Movement Speed;
    public float LeapMoveSpeed;
    private float xMove;//X Movement Velocity
    private float yMove;//Y Movement Velocity
    private float hight;//Sprite height
    private float width;//Sprite width   
    public float offset;
    //The start and finish positions for the interpolation.
    private Vector2 position;
    private Vector2 mousePosition;
    private Vector2 destiPos;
    private Vector3 lastPosition;

    private Vector2 touchDeltaPosition;
    public float distance;
    public float maxDistance;
    //public Rect screenRect;
    //public Rect rect;
    public Vector3 mouseDeltaPosition;
    public float TiltSpeed;
    public Quaternion target;



    Rect screenRect = new Rect(0, 0, Screen.width, Screen.height);


    public void setDestination(Vector3 position)
    {
        destiPos = new Vector3(0, -10, 0);
    }

    // Use this for initialization
    void Start()
    {
        mCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        mousePosition = Input.mousePosition;


    }

    void Update()
    {
        distance = Vector3.Distance(transform.position, destiPos);
        moveSpeed = GameControler.control.speed;
        Move();
    }

    public void setCanMove(bool value)
    {
        canMove = value;
    }

    public bool canThePlayerMove()
    {
        return canMove;
    }


    public void TouchControls()
    {
        foreach (Touch touch in Input.touches)
        {
            var rotationVector = transform.rotation.eulerAngles;
            if (touch.phase == TouchPhase.Moved)
            {
                if (canMove && !Game_Manager.gm.gameIsPaused)
                {
                    UpdateTouchPosition();
                    if (screenRect.Contains(Input.mousePosition))
                    {
                        destiPos = Camera.main.ScreenToWorldPoint(new Vector2(touchDeltaPosition.x, touchDeltaPosition.y + offset));
                    } else
                    {
                        destiPos = new Vector3(0, -10, 0);
                    }
                }
            }
        }

    }

    public void TouchTilt()
    {
        var rotationVector = transform.rotation.eulerAngles;

        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);


            if (touch.phase == TouchPhase.Began)
            {
                //Debug.Log("TouchPhase Begun" + rotationVector.y);
                //play animation whenever any input from mouse is obtained
                rotationVector.y = 0;
            }


            if (touch.phase == TouchPhase.Moved)
            {
                //Debug.Log("TouchPhase Moved" + rotationVector.y);

                if (Vector3.Distance(transform.position, destiPos) > maxDistance)
                {
                    float pointer_x = Input.GetAxis("Mouse X");
                    float pointer_y = Input.GetAxis("Mouse Y");
                    if (Input.touchCount > 0)
                    {
                        pointer_x = Input.touches[0].deltaPosition.x;
                        pointer_y = Input.touches[0].deltaPosition.y;
                        Debug.Log(" pointer_x " + pointer_x);
                        Debug.Log(" pointer_y " + pointer_x);
                    }

                    ///// NEW IMPLEMENTATION

                    if (pointer_x < -1f)
                    {
                        //play animation for left move

                        rotationVector.y = 45;
                    } else if (pointer_x > 1f)
                    {
                        //play animation for right move
                        rotationVector.y = -45;
                    } else if (pointer_x == 0)
                    {
                        //play animation whenever any input from mouse is obtained
                        rotationVector.y = 0;
                    }
                    target = Quaternion.Euler(0, rotationVector.y, 0);
                }
            }


            if (touch.phase == TouchPhase.Stationary)
            {
                rotationVector.y = 0;
                // Debug.Log("TouchPhase Stationary" + rotationVector.y);
                //play animation whenever any input from mouse is obtained

            }
            if (touch.phase == TouchPhase.Ended)
            {

                rotationVector.y = 0;

                // Debug.Log("TouchPhase Ended" + rotationVector.y);
                //play animation whenever any input from mouse is obtained
            }

            if (touch.phase == TouchPhase.Canceled)
            {
                rotationVector.y = 0;
                // Debug.Log("TouchPhase Canceled" + rotationVector.y);
                //play animation whenever any input from mouse is obtained

            }
            target = Quaternion.Euler(0, rotationVector.y, 0);
        }



    }



    public void MouseControls()
    {
        if (Input.GetMouseButton(0))
        {
            if (canMove)
            {
                UpdateMousePosition();
                if (screenRect.Contains(Input.mousePosition))
                {
                    destiPos = Camera.main.ScreenToWorldPoint(new Vector2(mousePosition.x, mousePosition.y + offset));
                } else
                {
                    destiPos = new Vector3(0, -10, 0);
                }
            }
        }
    }


    /// <summary>
    /// Moves the Player accordintly
    /// </summary>
    void Move()
    {
        if (GameControler.control.debugMode)
        {
            DebugUI.debugui.setText(0, "" + Camera.main.ScreenToWorldPoint(mousePosition));
            DebugUI.debugui.setText(1, "" + Camera.main.ScreenToWorldPoint(touchDeltaPosition));
            DebugUI.debugui.setText(2, "" + transform.position);
        }

        TouchControls();
        MouseControls();

        if (canMove)
        {
            transform.position = Vector2.Lerp(transform.position, destiPos, moveSpeed * Time.deltaTime);
            transform.rotation = Quaternion.Slerp(transform.rotation, target, Time.deltaTime * TiltSpeed);
        } else
        {
            destiPos = transform.position;
            target = Quaternion.Euler(0, 0, 0);

        }
        TouchTilt();
    }

    public void UpdateTouchPosition()
    {
        touchDeltaPosition = new Vector2(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y + offset);
        touchDeltaPosition = new Vector2(
                  Input.GetTouch(0).position.x,
                  Mathf.Clamp(Input.GetTouch(0).position.y + offset, 0, 1600));
    }
    public void UpdateMousePosition()
    {

        mousePosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y + offset); // Update Mouse Position3
        mousePosition = new Vector2(
                   Input.mousePosition.x,
                   Mathf.Clamp(Input.mousePosition.y, 0, 1600));



    }

    void Tilt()
    {
        ///// OLD IMPLEMENTATION
        //Vector3 rotationVector = transform.rotation.eulerAngles;
        //if (Input.GetAxis("Mouse X") > 0.8f)
        //{
        //    rotationVector.y = -30;
        //} else if (Input.GetAxis("Mouse X") < -0.8f)
        //{
        //    rotationVector.y = 30;
        //}

        //if (Vector2.Distance(transform.position, destiPos) < 0.2f)
        //{

        //    rotationVector.y = 0;
        //}
        //transform.rotation = Quaternion.Euler(rotationVector);
        float pointer_x = Input.GetAxis("Mouse X");
        float pointer_y = Input.GetAxis("Mouse Y");
        if (Input.touchCount > 0)
        {
            pointer_x = Input.touches[0].deltaPosition.x;
            pointer_y = Input.touches[0].deltaPosition.y;
        }

        ///// NEW IMPLEMENTATION
        var rotationVector = transform.rotation.eulerAngles;
        if (pointer_x < -.5f)
        {
            //play animation for left move

            rotationVector.y = 45;
        } else if (pointer_x > 0.5f)
        {
            //play animation for right move
            rotationVector.y = -45;
        } else if (pointer_x == 0)
        {
            //play animation whenever any input from mouse is obtained
            rotationVector.y = 0;
        }


        Quaternion target = Quaternion.Euler(0, rotationVector.y, 0);
        transform.rotation = Quaternion.Slerp(transform.rotation, target, Time.deltaTime * TiltSpeed);
    }

    public void TildTouch()
    {


        float z = Input.GetAxis("Horizontal") * 15.0f; // might be negative, just test it
        Vector3 euler = transform.localEulerAngles;
        euler.z = Mathf.Lerp(euler.z, z, 2.0f * Time.deltaTime);
        transform.localEulerAngles = euler;

    }








}
