using System.Collections;
using UnityEngine;

public class PlayerShoot : MonoBehaviour
{
    private Player_Ship playership;
    private AudioSource playerAudioSource;
    [Header("Prefabs")]
    public GameObject cannonPrefab;
    public GameObject misslePrefab;
    public GameObject[] Weapons;//List of All the Weapons
    public Transform[] cannons;
    public Transform[] missleCannon;
    [Header("Audio")]
    public AudioClip shotSfx;

    [Space]
    public ParticleSystem LeftshootEffect;
    public ParticleSystem MiddleShootEffect;
    public ParticleSystem RightshootEffect;
    private int weapons;
    public bool missleUpgrade;//Upgrades
    private GameObject weapon;
    private float nextMissleFire;
    private float MisslefireRate;
    private RaycastHit hit;
    private Vector2 forward;
    private Transform sightStart, sightEnd;
    private bool spotted;
    public bool canShoot;
    [HideInInspector]
    public int tapCount = 0;
    private bool coroutineStarted;
    private Player_Move pmove;//Player_Move;

    public GameObject frontPosition;
    private bool autoAttackMode;
    void Awake()
    {
        playership = GetComponent<Player_Ship>();
        playerAudioSource = GetComponent<AudioSource>();
        pmove = GameObject.FindGameObjectWithTag("Player").GetComponent<Player_Move>();
        weapon = new GameObject("WeaponHolder");
        weapon.tag = "BulletsAndWeapon";
        MisslefireRate = playership.RocketFireRate;
    }
    // Use this for initialization
    void Start()
    {
        LeftshootEffect.Stop();
        MiddleShootEffect.Stop();
        RightshootEffect.Stop();

        autoAttackMode = GameControler.GetAutoAttackMode();

    }

    // Update is called once per frame
    void Update()
    {
        //Weapon Mechanics
        if (canShoot)
        {
            if (autoAttackMode)
            {
                if (Input.GetMouseButton(0) && pmove.canThePlayerMove())
                {
                    shootMechanics();

                } else if (Input.touchCount > 0)
                {
                    shootMechanics();

                }
            } else
            {
                shootMechanics();
            }

            //Missile Upgrade
            if (Input.GetMouseButtonDown(1) && pmove.canThePlayerMove())
            {
                Missle();
            }

            checkMissiles();

            if (missleUpgrade && Input.touchCount > 0)
            {
                foreach (Touch touch in Input.touches)
                {
                    if (touch.phase == TouchPhase.Began)
                    {
                        if (pmove.canThePlayerMove() && canShoot)
                        {
                            tapCount++;
                        }
                        if (tapCount >= 2)
                        {
                            Missle();
                            tapCount = 0;
                        }
                    }
                }
                if (tapCount > 0)
                {
                    if (!coroutineStarted)
                    {
                        coroutineStarted = true;
                        StartCoroutine(resetTapCount());
                    }
                }
            }
        } else
        {
            CheckFrontOfShipForObstacles();
        }
    }

    void CheckFrontOfShipForObstacles()
    {
        if (GameObject.FindGameObjectWithTag("AsteroidManager"))
        {
            var Asteroids = GameObject.FindGameObjectWithTag("AsteroidManager").transform;

            for (int i = 0; i < Asteroids.childCount; i++)
            {
                float angle = Vector3.Angle(transform.up, Asteroids.GetChild(i).transform.position - transform.position);
                if (Mathf.Abs(angle) < 20)
                {
                    shootMechanics();
                }
            }

        } else
        {
            Debug.LogWarning("AsteroidManager not found");
        }

    }

    void checkMissiles()
    {
        if (GameControler.control)
        {
            if (GameControler.control.missles > 0)
            {
                missleUpgrade = true;
            } else
            {
                missleUpgrade = false;
            }
        }
    }

    //void RayCasting()
    //{
    //    LayerMask mask = LayerMask.NameToLayer("Player");
    //    //Debug.DrawLine(sightStart.position, sightEnd.position);
    //    if (Physics2D.Linecast(sightStart.position, sightEnd.position, 1 << LayerMask.NameToLayer("air_Vehicles")))
    //    {
    //        spotted = true;
    //    } else
    //    {
    //        spotted = false;
    //    }
    //}

    void shootMechanics()
    {
        if (Time.time > playership.nextFire)
        {
            for (int i = 0; i < cannons.Length; i++)
            {
                playership.nextFire = Time.time + playership.fireRate;

                GameObject Cannon = Instantiate(Weapons[(int)GameControler.control.weaponUpgrade], cannons[i].transform.position, Quaternion.identity) as GameObject;
                Cannon.transform.parent = weapon.transform;
                Cannon.gameObject.GetComponent<Weapon>().setParentOfThisObject(weapon);
                if (shotSfx)
                {
                    if ((int)GameControler.control.weaponUpgrade == 0)
                    {
                        MiddleShootEffect.Play();
                    } else if ((int)GameControler.control.weaponUpgrade == 1)
                    {
                        LeftshootEffect.Play();
                        RightshootEffect.Play();
                    } else if ((int)GameControler.control.weaponUpgrade > 1)
                    {
                        LeftshootEffect.Play();
                        MiddleShootEffect.Play();
                        RightshootEffect.Play();
                    }

                    playerAudioSource.PlayOneShot(shotSfx, GameControler.GetSoundVolume());
                }
            }
        }
    }

    void Missle()
    {
        if (GameControler.control.missles > 0)
        {
            if (Time.time > nextMissleFire)
            {

                for (int i = 0; i < missleCannon.Length; i++)
                {

                    nextMissleFire = Time.time + MisslefireRate;
                    GameObject Missile = Instantiate(misslePrefab, missleCannon[i].position, Quaternion.identity) as GameObject;
                    Missile.transform.parent = weapon.transform;
                    GameControler.control.missles--;
                    if (i == 0)
                    {
                        Missile.GetComponent<Missile>().direciton = 1;
                    } else
                    {
                        Missile.GetComponent<Missile>().direciton = -1;
                    }
                    Missile.transform.parent = weapon.transform;
                    if (shotSfx)
                    {
                        playerAudioSource.PlayOneShot(shotSfx, GameControler.GetSoundVolume());
                    }
                }
            }
        }
    }

    void checkWeaponUpgrade()
    {
        if (GameControler.control.weaponUpgrade > 5)
        {
            GameControler.control.weaponUpgrade = 0;
        }
    }

    IEnumerator resetTapCount()
    {
        yield return new WaitForSeconds(1f);
        tapCount = 0;
        coroutineStarted = false;
    }

}
