using UnityEngine;

public class Stars : MonoBehaviour
{
    public float yvel;
    Vector2 newPosition;
    // Use this for initialization
    void Start()
    {
        transform.position = new Vector3(Random.Range(-25, 25), Random.Range(-25, 25));
        yvel = Random.Range(-1.0f, -5.0f);
    }

    // Update is called once per frame
    void Update()
    {
        newPosition = transform.position;
        newPosition.y += yvel * Time.deltaTime;
        transform.position = newPosition;
        if (transform.position.y < -30)
        {
            transform.position = new Vector3(Random.Range(-17, 17), Random.Range(18, 40));
        }
    }
}
