using UnityEngine;
using System.Collections;

public class StarManager : MonoBehaviour
{

    public float numOfStars;
    public GameObject starPrefab;
    public GameObject[] stars;
    public float speed;

    public Vector2 PointsOnX;
    public Vector2 PointsOnY;


    // Use this for initialization
    void Start()
    {
        stars = new GameObject[(int)numOfStars];
        CreateStars();
    }

    void Update()
    {
        moveStars();
    }

    private void CreateStars()
    {
        for(int i = 0; i < numOfStars; i++)
        {
           GameObject star = Instantiate(starPrefab, (Vector3)new Vector2(Random.Range(PointsOnX.x, PointsOnX.y), Random.Range(PointsOnY.x, PointsOnY.y))   , Quaternion.identity) as GameObject;
           stars[i] = star;
            star.transform.SetParent(transform);
        }
    }

    private void moveStars()
    {
        foreach(GameObject star in stars) {
            if(star!=null)
            star.transform.position -= transform.up * speed * Time.deltaTime;
        }
        reset();
    }


    private void reset()
    {
        foreach (GameObject star in stars)
        {
            if (star != null)
            {
               if (star.transform.position.y < -10)
                {
                    star.transform.position = (Vector3)new Vector2(Random.Range(PointsOnX.x, PointsOnX.y), Random.Range(PointsOnY.x, PointsOnY.y));
                }
            }
        }
    }

}
       
  
