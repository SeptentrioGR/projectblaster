using UnityEngine;

public class Projectle : MonoBehaviour
{

    public float speed;
    public float damage;

    public GameObject explosion;

    public AudioClip exlpodeSfx;
    public bool IsMissle;
    private void Start()
    {
        if (!IsMissle)
        {
            speed = GameControler.control.FireRate;
        }
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector2.up * speed * Time.deltaTime);


        Vector3 max = Camera.main.ViewportToWorldPoint(new Vector2(1, 1));
        if (transform.position.y > max.y)
        {
            Destroy(gameObject);
        }
    }


    public void setDamage(float newDamage)
    {
        damage = newDamage;
    }

    public float getDamage()
    {
        return damage;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        GameObject obj = other.gameObject;
        if (obj.GetComponent<Enemy_Ship>())
        {

            obj.GetComponent<Enemy_Ship>().DealDamage(damage);
            DestoryNow();
        }


        if (obj.GetComponent<Rock>())
        {

            DestoryNow();
            Destroy(gameObject);
        }
    }

    public void DestoryNow()
    {
        if (explosion)
        {
            GameObject Explosion = Instantiate(explosion, transform.position, Quaternion.identity) as GameObject;
            Explosion.transform.SetParent(GameObject.FindGameObjectWithTag("BulletsAndWeapon").transform);
            Destroy(Explosion, 1f);
        }
        if (exlpodeSfx)
        {
            Camera.main.GetComponent<AudioSource>().PlayOneShot(exlpodeSfx, GameControler.GetSoundVolume());
        }
        Destroy(gameObject);
    }
}
