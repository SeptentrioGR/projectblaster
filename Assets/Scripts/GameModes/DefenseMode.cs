using UnityEngine;
using UnityEngine.UI;

public class DefenseMode : MonoBehaviour
{

    //Defence Game Mode Variables
    public GameObject cruiserLife;
    public Text cruiserLifeText;
    public DefenceMode defence;



    public float damageToCruiserPerEnemy;

    public GameObject cruiserLifeHealthBar;
    public Slider cruiserLifeSplider;
    public float cruiserCurLife;
    public float cruiserStartLife;
    public bool isAlive;

    public Image HealthSliderBackground;
    public Color fullHealthColor = Color.green;
    public Color zeroHealthColor = Color.red;


    // Use this for initialization
    void Start()
    {
        if (defence != null)
        {
            isAlive = true;
            cruiserCurLife = cruiserStartLife;
            cruiserLifeSplider.maxValue = cruiserStartLife;
            cruiserLifeSplider.value = cruiserCurLife;
            cruiserLifeSplider.maxValue = cruiserStartLife;
        }


    }

    void gameMode(GameObject missionText, GameObject TimeTextUI)
    {
        missionText.GetComponent<Text>().text = "Objective" + "\n" + " Protect the Cruiser!";
        TimeTextUI.SetActive(true);

    }
    // Update is called once per frame
    void Update()
    {
        cruiserLifeText.enabled = true;
        cruiserLife.SetActive(true);
        HealthSliderBackground.color = Color.Lerp(zeroHealthColor, fullHealthColor, cruiserCurLife / cruiserStartLife);
        checkGameStatus(cruiserCurLife, Game_Manager.gm.gameover, isAlive);

    }

    void checkGameStatus(float currentLife, bool TheGameIsOver, bool isAlive)
    {
        if (currentLife <= 0 && !TheGameIsOver)
        {
            if (isAlive)
            {
                isAlive = !isAlive;
                Game_Manager.gm.gameover = true;
            }
        }
    }
    void takeDamage(float value)
    {
        if (!Game_Manager.gm.gameover)
        {
            lowerHealth(value);
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {

        if (other.gameObject.tag == "Enemies")
        {
            takeDamage(100f);
            Destroy(other.gameObject);
        }

        if (other.gameObject.tag == "Asteroids")
        {
            takeDamage(100f);
            Destroy(other.gameObject);
        }

        if (other.gameObject.tag == "EnemyProjectles")
        {
            takeDamage(10f);
            Destroy(other.gameObject);
        }

    }

    public void lowerHealth(float value)
    {
        cruiserLifeSplider.maxValue = cruiserStartLife;
        cruiserCurLife -= value;
        cruiserLifeSplider.value = cruiserCurLife;

    }
}
