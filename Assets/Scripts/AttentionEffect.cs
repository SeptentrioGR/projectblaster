using UnityEngine;

public class AttentionEffect : MonoBehaviour
{
    Camera mCamera;
    private Transform playerPosition;
    private GameObject effects;
    private GameObject exlamatinonmark;
    private GameObject WarningArrow;
    public Vector3 position;
    public Vector3 offsetPosition;
    public GameObject target;
    public bool lookForNewTarget;
    // Use this for initialization
    void Start()
    {
        mCamera = Camera.main;
        effects = transform.GetChild(0).gameObject;
        WarningArrow = effects.transform.GetChild(1).gameObject;
        exlamatinonmark = effects.transform.GetChild(0).gameObject;
        if (GameObject.FindGameObjectWithTag("Player"))
        {
            playerPosition = GameObject.FindGameObjectWithTag("Player").transform;
        } else
        {
            Debug.LogWarning("Player not Found");
        }
        lookForNewTarget = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameObject.FindObjectOfType<HeavyEnemyMovement>() && lookForNewTarget)
        {
            lookForNewTarget = false;
            target = GameObject.FindObjectOfType<HeavyEnemyMovement>().gameObject;



        } else
        {
            Debug.LogWarning("Target is destroyed or not found");
        }

        if (target)
        {
            exlamatinonmark.transform.rotation = new Quaternion(0, 0, 0, 0);
            transform.position = playerPosition.position + offsetPosition;
            position = target.transform.position;
            Vector3 dir = position - transform.position;
            float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        }
    }

    public void setTarget(GameObject obj)
    {

        target = obj;
    }

    public bool hasTarget()
    {
        if (target)
        {
            return true;
        } else
        {
            return false;
        }
    }
}
