using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityStandardAssets.ImageEffects;
public class Game_Manager : MonoBehaviour
{
    public string[] messages = {
        "Nice!",
        "Awesome!",
        "Great!",
        "Ouch!",
        "Killtacular!",
        "Toasty!"};
    //Static reference for Game Manager
    public static Game_Manager gm;
    public Enemy_Manager spawningManager;
    private AsteroidManager asteroidManager; //The Manager which manage Asteroid Spawns
    //Referense to the Player
    private GameObject player;
    private Player_Ship PlayerAircraft;
    //Reward Module
    private RewardScript rewardModule;

    //The start and finish positions for the interpolation.
    public Vector2 startingPos;
    private Vector2 newPos;


    public bool gameIsPaused;
    public bool gameover;
    public bool beatTheGame;
    public bool waveIsCompleted;
    public int wave;
    public int score;
    public int missles;
    public int howManyEnemiesAreAlive;
    public int TotalEnemiesInAWave;//How many enemies the wave Has
    public int remainingEnemiesInWave;
    public float TimeBeforeNextWave;//Waiting time before next wave
    public float maxTimeBeforeNextWave;
    private Text GameScore;
    private Text WaveText;
    private Text noet;//Number of Enemies Text
    private Text CoinText;
    public Text MissilesText;
    public Text TimeTillNextWaveText;
    //GameOver GameObject
    public GameObject winingWindow;
    public GameObject gameOverWindow;
    public GameObject gamePausedWindow;
    public GameObject objectiveText;
    public GameObject waveWarningSign;
    public GameObject CoinsScreen;
    public GameObject ConfirmPanel;

    private AudioSource pAS;//playerAudioSource
    private BlurOptimized cBC;//Camera Blur Compoment

    private PlayerShoot pshoot;//PlayerShoot
    private Player_Move pmove;//PlayerMove
    public GameObject Uc;//Upgrade Confirmation
    public GameObject Us;//Upgrade Shop;
    public bool isUpgrading;
    private bool missionFinished;
    private bool shipEnterTheScene;
    public bool playerCanMove;
    public GameObject DeathMessage;
    public int killedEnemiesWithoutGettingHit;
    public bool ClearMap;

    void Awake()
    {
        gm = GetComponent<Game_Manager>();
        if (GameObject.FindGameObjectWithTag("waveTextUI"))
            WaveText = GameObject.FindGameObjectWithTag("waveTextUI").GetComponent<Text>();
        if (GameObject.FindGameObjectWithTag("scoreTextUI"))
            GameScore = GameObject.FindGameObjectWithTag("scoreTextUI").GetComponent<Text>();
        if (GameObject.FindGameObjectWithTag("coinsTextUI"))
            CoinText = GameObject.FindGameObjectWithTag("coinsTextUI").GetComponent<Text>();
        if (GameObject.FindGameObjectWithTag("NumberOfEnemiesUI"))
        {
            noet = GameObject.FindGameObjectWithTag("NumberOfEnemiesUI").GetComponent<Text>();
        }
        Uc.SetActive(false);
        Us.SetActive(false);


        rewardModule = GameObject.FindGameObjectWithTag("RewardSystem").GetComponent<RewardScript>();
        cBC = Camera.main.GetComponent<BlurOptimized>();

        player = GameObject.FindGameObjectWithTag("Player");
        pAS = GameObject.FindGameObjectWithTag("Player").GetComponent<AudioSource>();
        pshoot = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerShoot>();
        pmove = GameObject.FindGameObjectWithTag("Player").GetComponent<Player_Move>();
        PlayerAircraft = GameObject.FindGameObjectWithTag("Player").GetComponent<Player_Ship>();


        DeathMessage = GameObject.FindGameObjectWithTag("DeathMessage");
    }




    //Use this for initialization
    void Start()
    {
        int value = GameControler.GetOffset();
        if (value == 1)
        {
            pmove.offset = 0;
        } else if (value == 2)
        {
            pmove.offset = 25;
        } else if (value == 3)
        {
            pmove.offset = 50;
        }
        playerCanMove = true;

        shipEnterTheScene = true;
        TimeBeforeNextWave = maxTimeBeforeNextWave;
        spawningManager = Enemy_Manager.em;
        asteroidManager = GameObject.FindGameObjectWithTag
            ("AsteroidManager").GetComponent<AsteroidManager>();
        pmove.setDestination(startingPos);
    }



    /// <summary>
    /// Clamp the player in bountaries X and Y when it is not hte First Entry and is the mission is not Finished
    /// </summary>

    void Clamp()
    {
        if (objectiveText && objectiveText.activeSelf)
        {
            if (!shipEnterTheScene || !missionFinished)
            {
                player.transform.position = new Vector2(
                    Mathf.Clamp(player.transform.position.x, GameBoundaries.b.lwt.x, GameBoundaries.b.rwt.x),
                    Mathf.Clamp(player.transform.position.y, GameBoundaries.b.lwtb.y, GameBoundaries.b.rwtb.y)
                );
            }
        }

    }
    // Update is called once per frame
    void Update()
    {

        pmove.setCanMove(playerCanMove);
        updateCoinText();
        updateGameScore();
        updateWaveTest();

        if (gameover && !gameIsPaused)
        {

        }

        //Check if Game Is Paused Or Not And Pause The Game or UnPause The Game
        if (gameIsPaused && !isUpgrading)
        {
            cBC.enabled = gameIsPaused;
            Time.timeScale = 0;
            pAS.pitch = 0;
        } else if (!gameIsPaused && !isUpgrading)
        {
            cBC.enabled = gameIsPaused;
            Time.timeScale = 1;
            pAS.pitch = 1;
        }

        if (!gameover && !beatTheGame)
        {
            GameLogic();
        } else
        {
            spawningManager.stopSpawningEnemies();
        }

    }


    void GameLogic()
    {

        if (TimeTillNextWaveText.enabled)
        {
            TimeTillNextWaveText.text = "" + Mathf.Round(TimeBeforeNextWave);
        }
        //If Wave is Completed start Timer Before Next Wave, Reset Total Enemies Alive and increase wave and show message on the screen
        //When Timer is completed start Spawning Again and repeat
        if (waveIsCompleted)
        {

            if (!rewardModule.startReward && !isUpgrading)
            {
                TimeBeforeNextWave -= Time.deltaTime;
            }


            if (!waveWarningSign.activeSelf && !rewardModule.startReward && !isUpgrading)
            {
                StartCoroutine(writeTextOnScreenAfterWave());

            }
            if (TimeBeforeNextWave < 0)
            {
                if (waveWarningSign.activeSelf) waveWarningSign.SetActive(false);
                if (TimeTillNextWaveText.enabled) TimeTillNextWaveText.enabled = false;
                spawningManager.startSpawningEnemies();
                waveIsCompleted = false;
                TimeBeforeNextWave = maxTimeBeforeNextWave;
                howManyEnemiesAreAlive = TotalEnemiesInAWave;
                AfterWaveCompleted();
                if (!pshoot.canShoot)
                    pshoot.canShoot = true;
                ClearMap = false;
                PlayerAircraft.invulnerability(0);
            }

        }


        //When wave is done wait for few minutes again
        if (remainingEnemiesInWave >= TotalEnemiesInAWave)
        {
            spawningManager.stopSpawningEnemies();
        }

        if (howManyEnemiesAreAlive <= 0 && !waveIsCompleted)
        {
            if (wave < 24)
            {
                ClearMap = true;
                rewardModule.startReward = true;
                PlayerAircraft.invulnerability(1);
                playerCanMove = false;
                waveIsCompleted = true;
                howManyEnemiesAreAlive = TotalEnemiesInAWave;
                remainingEnemiesInWave = 0;
                GameControler.control.goldcoinscollected += GameControler.control.coins;
            } else
            {

                beatTheGame = true;
                winingWindow.SetActive(true);
                player.GetComponent<Player_Ship>().PlayExitAnimation();
                player.GetComponent<Player_Move>().enabled = false;
                player.GetComponent<PlayerShoot>().enabled = false;
            }
        }

        if (PlayerAircraft.playerGotHit)
        {
            killedEnemiesWithoutGettingHit = 0;
        }

        if (!PlayerAircraft.isAlive && !gameover)
        {
            //Show GameOver Window
            StartCoroutine(waitALittleBitBeforeShowGameOverWindow());
            if (MusicManager.musicmusicPlayer)
            {
                MusicManager.musicmusicPlayer.changePitch(0.5f);
            }
        }
        //CongratulatePlayer();
    }

    public void CongratulatePlayer()
    {
        if (killedEnemiesWithoutGettingHit >= 5)
        {
            int random = Random.Range(0, messages.Length);
            DeathMessage.GetComponent<Text>().text = messages[random];
            DeathMessage.GetComponent<Animator>().SetTrigger("Enter");
            killedEnemiesWithoutGettingHit = 0;
        }
    }

    public void increaseRemingEnemiesInWave()
    {
        remainingEnemiesInWave++;
    }

    public void AfterWaveCompleted()
    {
        spawningManager.decreaseSpawningTime();
        spawningManager.increaseAvailableTypeOfEnemies();
        wave++;
        //Mission
        GameControler.control.waveSurvived = wave;
        asteroidManager.decreaseAsteroidSpawnTime();


    }

    public void resetEnemies()
    {
        howManyEnemiesAreAlive = 0;
        TotalEnemiesInAWave = 12;
        spawningManager.ResetTimer();
        TimeBeforeNextWave = 5;
        killedEnemiesWithoutGettingHit = 0;
    }

    public void increaseGameScore(int value)
    {
        score += value;
    }

    public void decreaseHowManyEnemiesAreAlive()
    {
        GameControler.control.enemykilled++;
        howManyEnemiesAreAlive--;
        killedEnemiesWithoutGettingHit++;

    }

    public void ConfirmationWindow(int value)
    {
        if (value == 1)
        {
            ConfirmPanel.SetActive(true);
            gamePausedWindow.SetActive(false);
        } else if (value == 0)
        {
            gamePausedWindow.SetActive(true);
            ConfirmPanel.SetActive(false);
        } else
        {
            Debug.LogError("Confirmation Window Value out of bound");
        }
    }
    public void GoBackToTheMainMenu()
    {
        GameControler.control.weaponUpgrade = 0;
        if (MusicManager.musicmusicPlayer)
        {
            if (MusicManager.musicmusicPlayer.getPitch() == 0.5f)
            {
                MusicManager.musicmusicPlayer.changePitch(1.0f);
            }
        } else
        {
            Debug.LogWarning("MusicPlayer Not Exist");
        }
        SceneManager.LoadScene("01 Main");
    }

    public void SaveAndExit()
    {
        saveScore();
        if (MusicManager.musicmusicPlayer)
        {
            if (MusicManager.musicmusicPlayer.getPitch() == 0.5f)
            {
                MusicManager.musicmusicPlayer.changePitch(1.0f);
            }
        } else
        {
            Debug.LogWarning("MusicPlayer Not Exist");
        }
        SceneManager.LoadScene("01 Main");
    }

    public void ResetGame()
    {
        Time.timeScale = 1f;
        if (MusicManager.musicmusicPlayer)
        {
            if (MusicManager.musicmusicPlayer.getPitch() < 1.0f)
            {
                MusicManager.musicmusicPlayer.changePitch(1.0f);
            }
        } else
        {
            Debug.LogWarning("MusicPlayer Not Exist");
        }

        GameControler.control.weaponUpgrade = 0;
        SceneManager.LoadSceneAsync("02a LevelSurvival");
    }



    public void ButtonPause()
    {
        if (MusicManager.musicmusicPlayer)
        {
            if (MusicManager.musicmusicPlayer.getPitch() == 0.5f)
            {
                MusicManager.musicmusicPlayer.changePitch(1.0f);
            } else
            {
                MusicManager.musicmusicPlayer.changePitch(0.5f);
            }
        } else
        {
            Debug.LogWarning("MusicPlayer Not Exist");
        }
        gameIsPaused = !gameIsPaused;
        gamePausedWindow.SetActive(gameIsPaused);
    }

    public void pauseGame()
    {
        gamePausedWindow.SetActive(true);
    }

    public void unPauseGame()
    {
        gamePausedWindow.SetActive(false);

    }
    public void updateCoinText()
    {

        if (CoinText)
            CoinText.text = GameControler.control.coins.ToString("00");
    }

    public void updateGameScore()
    {

        GameScore.text = "" + score.ToString("0000000");
    }

    public void updateWaveTest()
    {
        if (wave == 0)
        {
            WaveText.text = "" + 1;
        } else
        {
            WaveText.text = "" + wave;
        }

        //noet.text = "" + howManyEnemiesAreAlive;
    }

    IEnumerator writeTextOnScreenAfterWave()
    {
        waveWarningSign.SetActive(true);
        TimeTillNextWaveText.gameObject.SetActive(true);
        if (wave > 0)
        {
            waveWarningSign.GetComponent<Text>().text = "Wave " + wave + " Completed!";
        } else
        {
            waveWarningSign.GetComponent<Text>().text = "Incoming";
        }
        yield return new WaitForSeconds(2);
        if (wave > 0)
        {
            waveWarningSign.GetComponent<Text>().text = "Wave " + (wave + 1);
        } else
        {
            waveWarningSign.GetComponent<Text>().text = "Warning\nEnemies approching";

        }
        yield return new WaitForSeconds(2);
        waveWarningSign.GetComponent<Text>().text = "defend yourself!";
    }

    IEnumerator waitALittleBitBeforeShowGameOverWindow()
    {
        gameover = !gameover;
        Time.timeScale = 0.3f;
        Time.timeScale = .5f;
        GameControler.control.weaponUpgrade = 0;
        yield return new WaitForSeconds(2);
        gameOverWindow.SetActive(true);
        saveScore();
    }


    public void saveScore()
    {
        GameControler.control.score = score;
        if (GameControler.control.highscore < score)
        {
            GameControler.control.highscore = score;
        }
        GameControler.control.saveGame();

    }

    public void openShop()
    {
        Us.SetActive(true);
        CloseUpgradeConfirmation();
    }

    public void closeShop()
    {
        OpenUpgradeConfirmation();
        Us.SetActive(false);
    }
    public void OpenUpgradeConfirmation()
    {
        Uc.SetActive(true);
        isUpgrading = true;
        Time.timeScale = 0;

    }

    public void CloseUpgradeConfirmation()
    {
        Uc.SetActive(false);

    }

    public void doneWithUpgradeing()
    {
        isUpgrading = false;
        playerCanMove = true;
        Time.timeScale = 1;
        PlayerAircraft.UpdateShipStats();
    }

}



