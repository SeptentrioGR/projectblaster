using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager AudioAPI;
    public AudioSource[] sounds;
    public MusicManager MusicSource;
    public AudioSource SoundSource;


    public AudioClip click;
    public AudioClip bclick;//Back Button Click


    // Use this for initialization
    void Start()
    {
        if (AudioAPI != null && SoundSource != this)
        {
            Destroy(gameObject);
            print("Duplicate music musicPlayer self-destructing!");
        } else
        {
            AudioAPI = this;
            GameObject.DontDestroyOnLoad(gameObject);

            SoundSource = GetComponent<AudioSource>();
            MusicSource = GameObject.FindObjectOfType<MusicManager>();
        }

    }

    public void adjustVolume()
    {

        foreach (AudioSource sound in sounds)
        {
            sound.volume = GameControler.GetSoundVolume();
        }

        AudioManager.AudioAPI.setMusicVolume(GameControler.GetMusicVolume());
        AudioManager.AudioAPI.setSoundVolue(GameControler.GetSoundVolume());

    }

    public float GetSoundVolume()
    {
        return SoundSource.volume;

    }

    public float GetMusicVolume()
    {
        return MusicSource.getMusicVolume();
    }

    public void setSoundVolue(float value)
    {
        SoundSource.volume = value;
    }

    public void setMusicVolume(float value)
    {
        MusicSource.changeMusicVolume(value);
    }

    public void PlaySound(int value)
    {
        if (value == 1)
        {
            SoundSource.PlayOneShot(click, GameControler.GetSoundVolume());
        } else
        {
            SoundSource.PlayOneShot(bclick, GameControler.GetSoundVolume());
        }
    }


    public void SetDefaults()
    {
        setMusicVolume(GameControler.GetMusicVolume());
        setSoundVolue(GameControler.GetSoundVolume());
    }



}
