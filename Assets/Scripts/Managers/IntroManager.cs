using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class IntroManager : MonoBehaviour
{

    public GameObject[] Enemies;
    public Text mMessage;
    public bool firstTime;
    public bool levelIsLoading;
    public bool startCounting;
    public Color normalColor = Color.white;
    public Color highLight = Color.red;
    public int fontSize;
    public int fontSizeBolt;
    public MusicManager music;
    public AudioSource source;

    public bool startCutScene;
    public float speed;//Enemies in the background speed;
    // Use this for initialization
    void Start()
    {
        music = GameObject.FindGameObjectWithTag("MusicManager").GetComponent<MusicManager>();
        source = GameObject.FindGameObjectWithTag("MusicManager").GetComponent<AudioSource>();
        fontSize = mMessage.fontSize;
        fontSizeBolt = mMessage.fontSize + 10;
        StartCoroutine(playIntroCutscene());
    }

    // Update is called once per frame
    void Update()
    {

        if (startCutScene)
        {
            foreach (GameObject e in Enemies)
            {
                e.transform.position -= transform.up * speed * Time.deltaTime;
            }
        }


        if (source.GetComponent<AudioSource>().volume <= 0 || Input.GetMouseButtonDown(0) || Input.touchCount > 0)
        {
            if (!levelIsLoading)
            {
                GameControler.control.firstRun = !GameControler.control.firstRun;
                levelIsLoading = true;
                LoadMainGame();
            }

        }
    }

    public void LoadMainGame()
    {
        GameControler.control.SaveGameData();
        SceneManager.LoadSceneAsync("Main");
    }

    IEnumerator playIntroCutscene()
    {
        mMessage.text = "In the year of 2220, Mainkind send a high technological ship into space They choose the best Pilot for the job.";
        startCutScene = true;
        yield return new WaitForSeconds(4);
        mMessage.fontSize = fontSizeBolt;
        mMessage.color = highLight;
        mMessage.text = "You!";

        yield return new WaitForSeconds(4);
        mMessage.fontSize = fontSize;
        mMessage.color = normalColor;
        mMessage.text = "This Mission supposed to be easy, But unofornutate, You found yourself against alien forces that endanger Humanity";

        yield return new WaitForSeconds(4);
        mMessage.text = "World depends on you, for its Survival";
        yield return new WaitForSeconds(4);
        mMessage.text = "Good Luck!";
        startCounting = true;



    }
}
