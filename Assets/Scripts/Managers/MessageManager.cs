using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MessageManager : MonoBehaviour
{
    public static MessageManager message;

    public GameObject playerMessages;
    public GameObject npcMessages;

    public Text playerMessagesText;
    public Text npcMessagesText;
    public string leveltext;
    // Use this for initialization
    void Start()
    {
        message = GetComponent<MessageManager>();
        playerMessages.SetActive(false);
        npcMessages.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (playerMessages.activeSelf || npcMessages.activeSelf)
        {
            StartCoroutine(hideMessage());
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene(leveltext);
        }

    }

    public void showMessage(string who, string text)
    {
        switch (who)
        {
            case "Player":
                playerMessages.SetActive(true);
                playerMessagesText.text = "You: " + text;
                break;
            case "NPC":
                npcMessages.SetActive(true);
                npcMessagesText.text = "You: " + text;
                break;
            default:
                break;
        }
    }

    IEnumerator hideMessage()
    {
        yield return new WaitForSeconds(3f);
        playerMessages.SetActive(false);
        npcMessages.SetActive(false);

    }
}
