using UnityEngine;

public class AsteroidManager : MonoBehaviour
{
    public GameObject[] asteroids;
    private GameObject curAsteroid;
    public Boundaries boundaries;

    private float startingTime;
    public float time;
    public float repeat;

    public Vector2 PointsOnX;
    public Vector2 PointsOnY;

    private bool spawnAsteroid;
    // Use this for initialization
    void Start()
    {
        startingTime = time;
        time = 16;

    }
    public bool AMineExist()
    {
        foreach (Transform go in transform)
        {
            if (go.gameObject.GetComponent<Enemy_Ship>())
            {
                return true;
            }
        }
        return false;
    }
    public void Update()
    {

        if (Game_Manager.gm.ClearMap && transform.childCount > 0)
        {
            foreach (Transform go in transform)
            {
                if (go.gameObject.GetComponent<Rock>())
                {
                    go.gameObject.GetComponent<Rock>().DestoryNow(true);
                } else if (go.gameObject.GetComponent<Enemy_Ship>())
                {
                    go.gameObject.GetComponent<Enemy_Ship>().currentHealth = 0;
                }
            }
        }


        if (!Game_Manager.gm.waveIsCompleted && Game_Manager.gm.wave > 2)
        {
            time -= 1 * Time.deltaTime;
            if (time <= 0)
            {

                time = startingTime - 1;
                time = Mathf.Clamp(time, 2, 16);
                AsteroidSpawner();
            }
        }
    }


    public void decreaseAsteroidSpawnTime()
    {
        if (startingTime > 6)
        {
            startingTime -= 1;
        } else
        {
            startingTime = 6;
        }
    }
    void AsteroidSpawner()
    {
        Vector2 pos = new Vector2(Random.Range(boundaries.minX, boundaries.maxX), Random.Range(boundaries.minY, boundaries.maxY));
        curAsteroid = asteroids[Random.Range(0, asteroids.Length)];
        GameObject instance = Instantiate(curAsteroid, pos, Quaternion.identity) as GameObject;
        instance.name = curAsteroid.name;
        instance.transform.SetParent(transform);
    }
}
