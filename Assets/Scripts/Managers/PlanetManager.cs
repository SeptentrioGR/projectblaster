using UnityEngine;

public class PlanetManager : MonoBehaviour
{
    public GameObject[] planets;
    public GameObject curPlanet;
    public float ymax;
    public float speed;

    public Vector2 PointsOnX;
    public Vector2 PointsOnY;
    public float offspec;
    void Start()
    {
        curPlanet = planets[Random.Range(0, planets.Length)];
        for (int i = 0; i < planets.Length; i++)
        {
            int size = Random.Range(4, 16);
            curPlanet.transform.localScale = new Vector2(size, size);
            curPlanet.transform.position = new Vector2(Random.Range(PointsOnX.x, PointsOnX.y), Random.Range(PointsOnY.x, PointsOnY.y));
        }
    }

    void Update()
    {

        //for (int i = 0; i < planets.Length; i++)
        //{
        Vector2 newPos = curPlanet.transform.position;
        newPos.x += speed * Time.deltaTime;
        newPos.y += speed * Time.deltaTime;
        curPlanet.transform.position -= transform.up * speed * Time.deltaTime;

        if (curPlanet.transform.position.y < -ymax)
        {
            int size = Random.Range(4, 16);
            curPlanet = planets[Random.Range(0, planets.Length)];
            curPlanet.transform.localScale = new Vector2(size, size);

            curPlanet.transform.position = new Vector2(Random.Range(PointsOnX.x, PointsOnX.y), Random.Range(PointsOnY.x, PointsOnY.y));
        }
        //} 
    }

}
