using UnityEngine;
using UnityEngine.SceneManagement;

public class MusicManager : MonoBehaviour
{
    public static MusicManager musicmusicPlayer;

    [Header("List of Music")]
    [Tooltip("Add the music you want to play while playing the game")]
    public AudioClip splashClip;
    public AudioClip startClip;
    public AudioClip intro;
    public AudioClip[] gameClip;

    private AudioSource musicPlayer;
    public float timeTheMusicPlay;
    public bool inGame;

    private void Awake()
    {
        if (musicPlayer != null && musicmusicPlayer != this)
        {
            Destroy(gameObject);
            print("Duplicate music musicPlayer self-destructing!");
        } else
        {
            musicmusicPlayer = this;
            GameObject.DontDestroyOnLoad(gameObject);
            musicPlayer = GetComponent<AudioSource>();
            musicPlayer.clip = splashClip;
            musicPlayer.Play();

        }
    }


    void Start()
    {
        musicPlayer.volume = GameControler.GetMusicVolume();
    }

    public void Update()
    {

        if (inGame)
        {
            if (!musicPlayer.isPlaying)
            {
                musicPlayer.Play();

            }
            if (Mathf.Round(musicPlayer.time) == Mathf.Round(musicPlayer.clip.length))
            {

                musicPlayer.clip = changeSong();

            }

        }
    }
    void OnEnable()
    {

        //Tell our 'OnLevelFinishedLoading' function to start listening for a scene change as soon as this script is enabled.
        SceneManager.sceneLoaded += OnLevelFinishedLoading;

    }

    void OnDisable()
    {
        //Tell our 'OnLevelFinishedLoading' function to stop listening for a scene change as soon as this script is disabled. Remember to always have an unsubscription for every delegate you subscribe to!
        SceneManager.sceneLoaded -= OnLevelFinishedLoading;
    }

    void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        Debug.Log("Level Loaded");
        Debug.Log(scene.name);
        Debug.Log(mode);

        Debug.Log("MusicmusicPlayer: Loaded level + " + scene);
        musicPlayer.Stop();
        inGame = false;
        if (scene.name.Equals("00 Splash"))
        {
            musicPlayer.clip = splashClip;
        }
        if (scene.name.Equals("01 Main"))
        {
            musicPlayer.clip = startClip;
        }
        if (scene.name.Equals("02 LoadingGame"))
        {
            musicPlayer.clip = null;
            musicPlayer.Stop();
        }
        if (scene.name.Equals("02a LevelSurvival"))
        {
            musicPlayer.clip = changeSong();
            musicPlayer.loop = false;
            inGame = true;
        }


        musicPlayer.Play();
    }


    public AudioClip changeSong()
    {
        AudioClip newClip;
        newClip = gameClip[Random.Range(0, gameClip.Length)];
        return newClip;
    }

    public void changePitch(float value)
    {
        musicPlayer.pitch = value;
    }
    public float getPitch()
    {
        return musicPlayer.pitch;
    }


    public void changeMusicVolume(float value)
    {
        musicPlayer.volume = value;
    }

    public float getMusicVolume()
    {
        return musicPlayer.volume;
    }
}
