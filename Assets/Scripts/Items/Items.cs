using UnityEngine;

public class Items : MonoBehaviour
{
    // Use this for initialization

    [Header("What Item Is It?")]
    public bool powerUp;
    public bool coins;
    public bool fix;
    public bool shield;

    private int direction = 1;
    private float vx;
    private float vy;
    private float xPos, yPos;
    private Camera mainCamera;
    private GameObject player;
    private Vector2 newPosition;
    public float speed;

    private AudioSource mAudioSource;
    public AudioClip pickSfx;

    public GameObject floatingText;
    public bool isClose;

    public CircleCollider2D colide;
    public float mDist;//MaxDistance
    void Start()
    {
        direction = Random.Range(-2, 2);
        vx = Random.Range(4, 8);
        vy = Random.Range(4, 8);
        player = GameObject.FindGameObjectWithTag("Player");
        mAudioSource = GetComponent<AudioSource>();
        speed = Random.Range(4, 8);
        colide = GetComponent<CircleCollider2D>();
        mDist = GameControler.control.magneticPower;
        if (GameObject.FindGameObjectWithTag("HoldItems"))
            transform.SetParent(GameObject.FindGameObjectWithTag("HoldItems").transform);
    }

    // Update is called once per frame
    void Update()
    {
        mDist = GameControler.control.magneticPower;
        if (!isClose)
        {
            newPosition = transform.position;
            newPosition.x += vx * direction * Time.deltaTime;
            newPosition.y += -vy * Time.deltaTime;
            transform.position = newPosition;
        } else
        {
            transform.position = Vector3.MoveTowards(transform.position, player.transform.position, speed * Time.deltaTime);

        }

        if (player)
        {
            getDistance();

            xPos = Camera.main.WorldToScreenPoint(transform.position).x;
            if (xPos > Screen.width)
            {
                direction = -1;
            } else if (xPos < 0)
            {
                direction = 1;
            }

            if (isClose)
            {
                transform.position = Vector3.MoveTowards(transform.position, player.transform.position, speed * Time.deltaTime);
            } else
            {
                Debug.LogWarning("Player is Dead");
            }
        }
    }

    void getDistance()
    {

        float dist = Vector2.Distance(transform.position, player.transform.position);
        if (dist < mDist)
        {
            isClose = true;
        } else
        {
            isClose = false;
        }

    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "Player")
        {
            if (coins)
            {
                mAudioSource.volume = GameControler.GetSoundVolume() - 0.5f;
                if (Game_Manager.gm)
                {
                    GameControler.control.coins++;
                }

            }

            if (powerUp)
            {
                if (GameObject.FindGameObjectWithTag("Player").GetComponent<Player_Ship>().fireRate > 0.3f)
                {
                    GameObject.FindGameObjectWithTag("Player").GetComponent<Player_Ship>().setFireRate(0.3f);
                } else
                {
                    if (GameControler.control.weaponUpgrade < 4)
                    {
                        GameObject.FindGameObjectWithTag("Player").GetComponent<Player_Ship>().setFireRate(0.8f);
                        GameControler.control.weaponUpgrade++;
                    }
                }
            }
            if (fix)
            {
                GameObject.FindGameObjectWithTag("Player").GetComponent<Player_Ship>().healShip(Random.Range(5, 10));
            }
            if (shield)
            {
                GameControler.control.shield = 5;
            }
            mAudioSource.PlayOneShot(pickSfx, GameControler.GetSoundVolume());
            GetComponent<SpriteRenderer>().enabled = false;
            GetComponent<BoxCollider2D>().enabled = false;
            Destroy(gameObject, 2f);
        }
    }
}
