using UnityEngine;

public class WarnEnemy : MonoBehaviour
{
    private AttentionEffect mAttentionEffect;

    // Use this for initialization
    void Start()
    {

        mAttentionEffect = GetComponent<AttentionEffect>();




    }

    // Update is called once per frame
    void Update()
    {


        if (mAttentionEffect.hasTarget())
        {
            transform.GetChild(0).gameObject.SetActive(true);
            mAttentionEffect.enabled = true;
        } else
        {
            transform.GetChild(0).gameObject.SetActive(false);
            mAttentionEffect.enabled = false;
        }
    }


}
