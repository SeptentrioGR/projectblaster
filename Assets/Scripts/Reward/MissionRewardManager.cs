using UnityEngine;
using UnityEngine.UI;

public class MissionRewardManager : MonoBehaviour
{

    public GameObject RewardEffect;
    public Text LevelUpText;
    public Text RankText;
    public Text RewardText;
    public Button claimButton;


    public void setMission(string text1, string text2, string text3)
    {
        GetComponent<AudioSource>().volume = GameControler.GetSoundVolume();

        LevelUpText.text = "" + text1;

        RankText.text = "" + text2;
        RewardText.text = "" + text3;
    }

    public void closeButton()
    {

    }
    // Update is called once per frame
    void Update()
    {
        RewardEffect.transform.position = Camera.main.transform.position;
    }
}
