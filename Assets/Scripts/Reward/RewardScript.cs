using System.Collections;
using UnityEngine;
using UnityEngine.UI;
public class RewardScript : MonoBehaviour
{

    public int index;
    public GameObject[] images = new GameObject[3];
    public GameObject[] selectableImage;
    public GameObject[] rewards;
    public GameObject curImageSelected;
    public bool startReward;
    public bool choosRewardStarted;
    public float timeSpeed;
    public float timer;
    public float startingTimer;
    public float timerTillReward = 10;
    public bool doneChoosingReward;
    public Vector2 image1Pos;
    public Vector2 image2Pos;
    public Vector3 image3Pos;
    public GameObject Window;
    public Text information;
    bool restarting;
    bool buttonPressed;
    int numberOfCoins;
    int numberOfMissiles;
    public GameObject RewardParent;
    public RandomCharacterGenerator itemManager;
    // Use this for initialization
    void Start()
    {
        initializeRandomItemGenerator(70, 10, 10, 10, 10);
        newRewards();

        image1Pos = images[0].GetComponent<RectTransform>().anchoredPosition;
        image2Pos = images[1].GetComponent<RectTransform>().anchoredPosition;
        image3Pos = images[2].GetComponent<RectTransform>().anchoredPosition;
        timerTillReward = Random.Range(5, 8);


    }

    public void initializeRandomItemGenerator(int coinchance, int Missilechance, int powerchance, int shieldchance, int RepairChance)
    {
        RandomCharacterGenerator.wipe();
        RandomCharacterGenerator.setWeight("Coin", coinchance);
        RandomCharacterGenerator.setWeight("Missile", Missilechance);
        RandomCharacterGenerator.setWeight("Power", powerchance);
        RandomCharacterGenerator.setWeight("Shield", shieldchance);
        RandomCharacterGenerator.setWeight("Repair", RepairChance);
    }

    int RollForReward()
    {
        string id = RandomCharacterGenerator.rollWeights();
        if (id == "Coin")
        {
            return 0;
        } else if (id == "Missile")
        {
            return 1;
        } else if (id == "Power")
        {
            return 2;
        } else if (id == "Shield")
        {
            return 3;
        } else if (id == "Repair")
        {
            return 4;
        }
        return 0;
    }
    void newRewards()
    {
        string id = RandomCharacterGenerator.rollWeights();

        int number0 = RollForReward();
        int number1 = RollForReward();
        int number2 = RollForReward();

        GameObject reward0 = Instantiate(rewards[number0], rewards[number0].transform) as GameObject;
        reward0.transform.SetParent(RewardParent.transform, false);
        reward0.transform.position = selectableImage[0].transform.position;
        GameObject reward1 = Instantiate(rewards[number1], rewards[number1].transform) as GameObject;
        reward1.transform.SetParent(RewardParent.transform, false);
        reward1.transform.position = selectableImage[1].transform.position;
        GameObject reward2 = Instantiate(rewards[number2], rewards[number2].transform) as GameObject;
        reward2.transform.SetParent(RewardParent.transform, false);
        reward2.transform.position = selectableImage[2].transform.position;

        images[0] = reward0;
        images[1] = reward1;
        images[2] = reward2;

        updateText(0, number0);
        updateText(1, number1);
        updateText(2, number2);
    }

    void updateText(int number, int value)
    {

        if (value == 0)
        {
            numberOfCoins = Random.Range(50, 100);
            images[number].transform.GetChild(0).GetComponent<Text>().text = " + " + numberOfCoins;
        } else if (value == 1)
        {
            images[number].transform.GetChild(0).GetComponent<Text>().text = "Weapon\nUpgrade";

        } else if (value == 2)
        {
            numberOfMissiles = Random.Range(10, 25);
            images[number].transform.GetChild(0).GetComponent<Text>().text = " + " + numberOfMissiles;
        } else if (value == 3)
        {
            images[number].transform.GetChild(0).GetComponent<Text>().text = "Shield\nUpgrade";
        } else if (value == 4)
        {
            images[number].transform.GetChild(0).GetComponent<Text>().text = "Repair Ship";

        }
    }

    // Update is called once per frame
    void Update()
    {


        if (!buttonPressed && Input.touchCount > 0 || Input.GetMouseButtonDown(0))
        {
            information.enabled = false;
            buttonPressed = true;
        }
        if (startReward)
        {
            GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerShoot>().canShoot = false;
            Window.SetActive(true);
            if (!doneChoosingReward)
            {

                if (buttonPressed)
                {
                    timerTillReward -= Time.deltaTime;
                }

                if (timerTillReward < 0.1f * 10)
                {
                    startingTimer = 0.5f;
                } else if (timerTillReward < 0.1f * 40)
                {
                    startingTimer = 0.4f;
                } else if (timerTillReward < 0.1f * 60)
                {
                    startingTimer = 0.3f;
                } else if (timerTillReward < 0.1f * 80)
                {
                    startingTimer = 0.2f;
                }
                if (timerTillReward <= 0)
                {
                    doneChoosingReward = true;

                }
                timer -= timeSpeed * Time.deltaTime;
                if (timer < 0)
                {
                    timer = startingTimer;
                    index++;
                    if (index > images.Length - 1)
                    {
                        index = 0;
                    }
                }



                foreach (GameObject selImg in selectableImage)
                {
                    selImg.SetActive(false);
                }

                curImageSelected = images[index];

                selectableImage[index].SetActive(true);


            } else if (doneChoosingReward)
            {
                for (int i = 0; i < images.Length; i++)
                {
                    images[i].SetActive(false);
                    selectableImage[i].SetActive(false);
                }
                if (!choosRewardStarted)
                {
                    choosRewardStarted = true;

                    chooseReward(images[index].name);
                }
                curImageSelected.GetComponent<Outline>().enabled = false;
                curImageSelected.SetActive(true);
                curImageSelected.transform.localScale = new Vector2(1.5f, 1.5f);
                curImageSelected.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
                if (!restarting)
                {
                    restarting = true;
                    StartCoroutine(waitBeforeRestart());
                }
            }

        }
    }

    public void reset()
    {
        for (int i = 0; i < images.Length; i++)
        {
            images[i].SetActive(true);
        }

        foreach (GameObject obj in images)
        {
            Destroy(obj);
        }
        newRewards();
        images[0].GetComponent<RectTransform>().anchoredPosition = image1Pos;
        images[1].GetComponent<RectTransform>().anchoredPosition = image2Pos;
        images[2].GetComponent<RectTransform>().anchoredPosition = image3Pos;
        curImageSelected.transform.localScale = new Vector2(1, 1);

        curImageSelected = null;
        timerTillReward = Random.Range(5, 8);
        Window.SetActive(false);
        startReward = false;
        choosRewardStarted = false;
        doneChoosingReward = false;
        startingTimer = 0.1f;
        restarting = false;
        buttonPressed = false;
        information.enabled = true;
    }

    public void chooseReward(string name)
    {
        switch (name)
        {
            case "CoinReward(Clone)":
                GameControler.control.coins += numberOfCoins;
                break;
            case "Upgrade(Clone)":
                if (GameObject.FindGameObjectWithTag("Player").GetComponent<Player_Ship>().fireRate > 0.3f)
                {
                    GameObject.FindGameObjectWithTag("Player").GetComponent<Player_Ship>().setFireRate(0.3f);
                } else
                {
                    if (GameControler.control.weaponUpgrade < 4)
                    {
                        GameObject.FindGameObjectWithTag("Player").GetComponent<Player_Ship>().setFireRate(0.8f);
                        GameControler.control.weaponUpgrade++;
                    }
                }
                break;
            case "MissilesPack(Clone)":
                GameControler.control.missles += numberOfMissiles;
                break;
            case "ShieldReward(Clone)":
                if (GameControler.control.shield < 5)
                {
                    GameControler.control.shield = 5;
                }
                break;
            case "RepairReward(Clone)":
                GameObject.FindGameObjectWithTag("Player").GetComponent<Player_Ship>().healShip(Random.Range(5, 10));
                break;
        }


    }

    IEnumerator waitBeforeRestart()
    {

        yield return new WaitForSeconds(2);
        reset();
        if (Game_Manager.gm.wave % 2 == 0)
        {
            {
                Game_Manager.gm.OpenUpgradeConfirmation();
            }
        }
    }

}
