using UnityEngine;

public class Planets : MonoBehaviour
{

    public float yvel;
    Vector2 newPosition;
    // Use this for initialization
    void Start()
    {
        transform.position = new Vector3(Random.Range(-10, 10), Random.Range(50, 100));
    }

    // Update is called once per frame
    void Update()
    {
        newPosition = transform.position;
        newPosition.y += yvel * Time.deltaTime;


        transform.position = newPosition;



        if (transform.position.y < -50)
        {
            Destroy(gameObject);
        }
    }
}
