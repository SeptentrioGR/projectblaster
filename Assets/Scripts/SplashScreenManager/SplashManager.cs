using UnityEngine;
using UnityEngine.UI;

public class SplashManager : MonoBehaviour
{
    public Image splashscreen;
    public float timeToApear = 0;
    public bool splashShowed;
    public float maxSplashScreenTime;
    private LoadingScreen mLoadingScreen;

    void Start()
    {
        mLoadingScreen = GetComponent<LoadingScreen>();
    }



    // Update is called once per frame
    void Update()
    {
        if (!mLoadingScreen.GameLevelLoader)
        {
            splashscreen.color = new Color(255, 255, 255, timeToApear);
            if (timeToApear < maxSplashScreenTime && !splashShowed)
            {
                timeToApear += .75f * Time.deltaTime;
            } else
            {
                splashShowed = true;
                timeToApear -= .75f * Time.deltaTime;
            }

            if (timeToApear < 0 && !IsInvoking("checkandloadlevel"))
            {
                Invoke("checkandloadlevel", 1);
            }

        }
    }

    public void checkandloadlevel()
    {
        if (!mLoadingScreen.GameLevelLoader)
        {
            Debug.Log("Its not loading Game Level Contining..");
            if (!mLoadingScreen.needRestart)
            {
                Debug.Log("It does not need Restart. Continuing");

                if (!mLoadingScreen.firstRun)
                {
                    Debug.Log("It is not first run, Loading Main Scene");
                    mLoadingScreen.LoadMain();
                } else if (mLoadingScreen.firstRun)
                {
                    Debug.Log("It is first Run, Loading Introduction Scene");
                    GameControler.control.firstRun = !GameControler.control.firstRun;
                    mLoadingScreen.LoadIntro();
                }
                if (!mLoadingScreen.mMapLoaded)
                {
                    Debug.Log("Loading Level....");
                    mLoadingScreen.mMapLoaded = !mLoadingScreen.mMapLoaded;
                    mLoadingScreen.LoadScene();

                }
            } else if (mLoadingScreen.needRestart)
            {
                Debug.Log("Needs Restart...");
                GameControler.control.saveGame();
                GameControler.control.deleteFiles();
                mLoadingScreen.needRestart = false;
            }
        }
    }

}
