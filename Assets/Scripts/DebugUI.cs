using UnityEngine;
using UnityEngine.UI;

public class DebugUI : MonoBehaviour
{
    public static DebugUI debugui;


    public string[] textToDisplay;
    public Text[] debugdisplay;
    void Start()
    {
        debugui = GetComponent<DebugUI>();
        for (int i = 0; i < transform.childCount; i++)
        {
            debugdisplay[i] = transform.GetChild(i).GetComponent<Text>();
            debugdisplay[i].gameObject.SetActive(GameControler.control.debugMode);
        }




    }

    // Update is called once per frame
    void Update()
    {
        if (GameControler.control.debugMode)
        {
            for (int i = 0; i < textToDisplay.Length; i++)
            {
                debugdisplay[i].text = textToDisplay[i];
                debugdisplay[i].gameObject.SetActive(GameControler.control.debugMode);
            }
        } else
        {
            for (int i = 0; i < textToDisplay.Length; i++)
            {
                debugdisplay[i].gameObject.SetActive(GameControler.control.debugMode);
            }
        }
    }

    public void setText(int number, string text)
    {
        textToDisplay[number] = text;
    }
}
