using UnityEngine;

public class CameraControl : MonoBehaviour
{

    public float xvel;
    private Transform target;
    public float offset;
    public float speed;
    public AudioSource mainCameraSource;
    public float minX, maxX;
    // Use this for initialization
    void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player").transform;
        mainCameraSource = Camera.main.GetComponent<AudioSource>();

    }
    void Update()
    {
        if (GameControler.control)
            mainCameraSource.volume = GameControler.GetMusicVolume();
    }

    void LateUpdate()
    {

        Vector3 newPosition = transform.position;
        newPosition.x += target.position.x * speed * Time.deltaTime;
        newPosition.y = 0;
        newPosition.z = -100;
        transform.position = newPosition;

        transform.position = new Vector3(
            Mathf.Clamp(transform.position.x, -minX, maxX),
            transform.position.y,
            transform.position.z);
    }
}
