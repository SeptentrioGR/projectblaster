using UnityEngine;

public class Rock : MonoBehaviour
{

    private bool isDead;
    private bool isChasing;
    private int time;

    public GameObject explosionPrefab;


    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Projectiles")
        {

            DestoryNow(true);
            Destroy(col.gameObject);
        }
    }

    void OnCollisionEnter2D(Collision2D other)
    {

        if (other.gameObject.tag == "Enemies")
        {
            DestoryNow(true);
        }

        if (other.gameObject.tag == "Projectiles")
        {
            DestoryNow(true);
        }

        if (other.gameObject.tag == "Player")
        {

            if (!isDead)
            {
                isDead = !isDead;
            }

            other.gameObject.GetComponent<Player_Ship>().TakeDamage(25f);

        }

    }



    public void DestoryNow(bool sound)
    {
        if (transform.localScale.x > 4)
        {
            for (int i = 0; i < 2; i++)
            {
                GameObject newRock = Instantiate(gameObject, transform.position, Quaternion.identity) as GameObject;
                newRock.name = gameObject.name;
                newRock.transform.SetParent(GameObject.FindGameObjectWithTag("AsteroidManager").transform);
                newRock.transform.localScale = transform.localScale / 2;

            }
        }
        if (sound)
        {
            if (explosionPrefab)
            {
                GameObject Explosion = Instantiate(explosionPrefab, transform.position, Quaternion.identity) as GameObject;
                Explosion.transform.SetParent(GameObject.FindGameObjectWithTag("AsteroidManager").transform);
                Destroy(Explosion, 1f);
            }
        }

        DestroyObject(gameObject);
    }


}
