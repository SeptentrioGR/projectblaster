using UnityEngine;

public class Rock_Move : MonoBehaviour
{
    public float minSpeed;
    public float maxSpeed;
    [SerializeField]
    private float directionxVel;
    private float yVel;
    private float xVel;
    [SerializeField]
    private int direction = 1;

    private float xPos, yPos;
    private Camera mainCamera;




    public float yMax;
    // Use this for initialization
    void Awake()
    {

        direction = Random.Range(-2, 2);
        yVel = Random.Range(minSpeed, maxSpeed);
        int randomSize = Random.Range(2, 6);
        transform.localScale = new Vector2(randomSize, randomSize);
        mainCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
    }

    void Update()
    {
        Vector2 newPosition = transform.position;
        Vector2 pos = mainCamera.WorldToScreenPoint(transform.position);
        newPosition.x += xVel * direction * Time.deltaTime;
        newPosition.y += -yVel * Time.deltaTime;
        transform.position = newPosition;

        rotation();

        if (pos.y < 0)
        {
            Destroy(gameObject, 1.0f);
        }


        if (pos.x > Screen.width)
        {
            direction = -1;
        }
        else if (pos.x < 0)
        {
            direction = 1;
        }

    }


    void rotation()
    {

        Vector3 rotationVector = transform.rotation.eulerAngles;

        rotationVector.z += -.5f * direction;

        transform.rotation = Quaternion.Euler(rotationVector);
    }
}
