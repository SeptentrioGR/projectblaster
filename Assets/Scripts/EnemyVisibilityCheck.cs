using UnityEngine;

public class EnemyVisibilityCheck : MonoBehaviour
{
    public bool isOutOfRange;

    private void FixedUpdate()
    {
        if (isOutOfRange)
        {
            Invoke("restartCheck", .5f);
        }
    }


    public bool CheckIfPositionReseted()
    {
        if (isOutOfRange)
        {
            return true;
        } else
        {
            return false;
        }
    }

    public void PositionRested()
    {
        isOutOfRange = true;
    }
    public void restartCheck()
    {
        isOutOfRange = false;
    }
}
