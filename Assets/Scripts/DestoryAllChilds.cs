using UnityEngine;

public class DestoryAllChilds : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Game_Manager.gm.ClearMap && transform.childCount > 0)
            foreach (Transform go in transform)
            {
                if (go.GetComponent<Enemy_Bullets>())
                {
                    go.GetComponent<BoxCollider2D>().enabled = false;
                }
            }
    }
}
