using System.Collections;
using UnityEngine;

public class Enemy_Bullets1 : MonoBehaviour
{

    [SerializeField]
    private float vx;
    [SerializeField]
    private float vy;

    public float speed;

    private Transform targetPos;


    public float yMax;


    public AudioClip Hitsfx;
    public AudioClip exlpodeSfx;


    public GameObject explosion;
    GameObject explostionObject;

    public bool isFollowing;
    public bool explode;
    public GameObject Bullet2n;
    Camera my_main_camera;
    //private Vector3 shootDirection;
    void Awake()
    {
        my_main_camera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        if (GameObject.FindGameObjectWithTag("Player"))
        {
            Transform pos = GameObject.FindGameObjectWithTag("Player").transform;
            targetPos = pos;
        }


    }

    void Start()
    {
        //Vector3 diff = targetPos.transform.position - transform.position;
        //shootDirection = diff;
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        Move();
    }

    void Move()
    {
        Vector3 pos = my_main_camera.WorldToScreenPoint(transform.position);
        if (isFollowing)
        {
            float step = speed * Time.deltaTime;
            if (targetPos)
                GetComponent<Rigidbody2D>().velocity = new Vector2(targetPos.transform.position.x - transform.position.x, step);
            if (explode)
            {
                StartCoroutine(explodeBeforeCreateBullets());
            }
        }
        else {
            transform.position += (Vector3)new Vector2(vx, vy) * Time.deltaTime;

        }

        if (pos.y < 0 || pos.y > Screen.height || pos.x < 0 || pos.x > Screen.width + 32)
        {
            Destroy(gameObject);
        }


    }


    void OnTriggerEnter2D(Collider2D col)
    {

        if (col.gameObject.tag == "Player")
        {
            col.gameObject.GetComponent<Player_Ship>().TakeDamage(25);
            if (explosion)
            {
                explostionObject = Instantiate(explosion, col.transform.position, col.transform.rotation) as GameObject;

            }
            Destroy(gameObject);
        }
        if (explostionObject)
        {
            explostionObject.transform.parent = GameObject.FindGameObjectWithTag("Player").transform;
        }
    }

    IEnumerator explodeBeforeCreateBullets()
    {
        yield return new WaitForSeconds(2f);
        Instantiate(Bullet2n, new Vector2(transform.position.x, transform.position.y - 4), Quaternion.identity);
        Destroy(gameObject);
    }

}
