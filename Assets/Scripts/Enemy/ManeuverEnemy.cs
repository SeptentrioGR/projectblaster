using System.Collections;
using UnityEngine;

public class ManeuverEnemy : MonoBehaviour
{
    Rigidbody2D rigid;
    public Vector2 startWait;
    public Vector2 maneuverTime;
    public Vector2 maneuverWait;
    public float currentSpeed;
    private float targetManeuver;
    public float dodge;
    public float smoothing;
    public float tilt;
    public Vector2 startPos;
    // Use this for initialization
    void Start()
    {
        rigid = GetComponent<Rigidbody2D>();
        StartCoroutine(Evade());
        startPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        float newManeuver = Mathf.MoveTowards(rigid.velocity.x, targetManeuver, Time.deltaTime * smoothing);
        rigid.velocity = new Vector3(newManeuver, currentSpeed, 0.0f);
        rigid.rotation = rigid.velocity.x * -tilt;
        Vector2 min = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));


        if (transform.position.y < min.y)
        {

            transform.position = startPos;

        }
    }

    IEnumerator Evade()
    {
        yield return new WaitForSeconds(Random.Range(startWait.x, startWait.y));
        while (true)
        {
            targetManeuver = Random.Range(50, dodge) * -Mathf.Sign(transform.position.x);
            yield return new WaitForSeconds(Random.Range(maneuverTime.x, maneuverTime.y));
            targetManeuver = 0;
            yield return new WaitForSeconds(Random.Range(maneuverWait.x, maneuverWait.y));
        }
    }




}
