using UnityEngine;

public class Enemy_Bullets : MonoBehaviour
{
    public enum Movements
    {
        normal, follow
    }
    public Movements m;
    Rigidbody2D rigid;
    public bool isReady;
    public Vector2 _direction;

    [Header("Projectile Configuration")]
    public float speed;
    public float damage;

    public float xvel;
    public float yvel;

    public GameObject explosion;
    public AudioClip exlpodeSfx;


    void Awake()
    {
        rigid = GetComponent<Rigidbody2D>();
    }


    // Update is called once per frame
    void Update()
    {
        switch (m)
        {
            case Movements.normal:
                Move();
                break;
            case Movements.follow:
                Follow();
                break;
            default:
                break;
        }

    }
    void Follow()
    {
        if (isReady)
        {
            //Get the Bullet's current position
            Vector2 position = transform.position;
            //calculate the bullet's snew position
            position += _direction * speed * Time.deltaTime;
            transform.position = position;

            Vector2 min = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));
            Vector2 max = Camera.main.ViewportToWorldPoint(new Vector2(1, 1));

            if (transform.position.y < min.y || transform.position.y > max.y || transform.position.x < -25 || transform.position.x > 25)
            {
                Destroy(gameObject);
            }
        }

    }
    void Move()
    {
        rigid.velocity = -transform.up * speed;


        Vector2 min = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));
        Vector2 max = Camera.main.ViewportToWorldPoint(new Vector2(1, 1));

        if (transform.position.y < min.y || transform.position.y > max.y || transform.position.x < -25 || transform.position.x > 25)
        {
            Destroy(gameObject);
        }

    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Projectiles")
        {

            if (GameControler.control.bulletReflect)
            {
                Destroy(col.gameObject);
                if (explosion)
                {
                    GameObject Explosion = Instantiate(explosion, transform.position, Quaternion.identity) as GameObject;
                    Destroy(Explosion, 1f);
                    Destroy(gameObject);
                }
            }
        }
        if (col.gameObject.tag == "Player")
        {
            col.gameObject.GetComponent<Player_Ship>().TakeDamage(damage);
            Debug.Log("Enemy does " + damage + " Damage to Player");
            if (explosion)
            {
                GameObject Explosion = Instantiate(explosion, transform.position, Quaternion.identity) as GameObject;
                Destroy(Explosion, 1f);
            }
            Destroy(gameObject);
        }
    }


    public void SetDirection(Vector2 direction)
    {
        _direction = direction.normalized;
        isReady = true;
    }


    public void setDamageToProjectiles(float newDamage)
    {
        damage = newDamage;
    }
}
