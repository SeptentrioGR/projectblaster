using UnityEngine;
using UnityEngine.UI;

public class HeavyEnemyShip : MonoBehaviour
{
    private HeavyEnemyMovement hem;
    private HeavyEnemyShooter hes;

    [Header("General Settings")]
    public string mName;
    public bool isAlive;
    private Animator anim; //Animations

    [Header("Effects")]
    public GameObject hitEffect;//The effect when bulles hit the ship;
    public GameObject explosionPrefab;//The effect when the player dies
    public ParticleSystem shieldSfx;//Shield Effect enable it when you have shield

    [Header("Sounds")]
    private AudioSource mainSource;
    public AudioClip shotSfx;//The sound when the player shoots
    public AudioClip Hitsfx;//The sound when enemy projectiles hit the ship
    public AudioClip exlpodeSfx;//The sound when the player ship explodes
    private AudioClip shieldBrokeSfx; //Sound when the Shield goes off

    [Header("Health Bar Settings")]
    public GameObject HealthBar;
    public Slider Slider;
    public Image HealthSliderBackground;
    public Color fullHealthColor = Color.green;
    public Color zeroHealthColor = Color.red;

    [Header("Upgrades")]
    public bool missleUpgrade;//Missiles is Available
    public bool ShieldUpgrade;//Shield is Available

    [Header("Stats Configuration")]
    public float startingHealth;
    public float shieldCapacity;
    [HideInInspector]
    public float currentHealth;

    public SpriteRenderer mRenderer;
    public Color normalColor = new Color(0, 0, 0);
    public Color hitColor = Color.red;
    public bool startShooting = false;

    //***************************************************
    [Tooltip("How much score is Rewarded")]
    public int ScoreValue;// How much score is Rewarded

    [Header("Drop Item on Death")]
    //The item that will instasiate when Enemy dies
    public bool dropItem;
    public GameObject[] item;
    public RandomCharacterGenerator itemManager;
    public GameObject mFloatingText;
    public bool MultipleCannons;
    public float offset;
    public bool isOutOfBound;
    private GameObject parentObject;
    private AudioSource enemySourceAudio;
    private Enemy_Move em;//Enemy_Move;s
    private Vector3 pos;//Objects WorldToScreenPoint

    void Start()
    {
        hem = GetComponent<HeavyEnemyMovement>();
        hes = GetComponent<HeavyEnemyShooter>();
        currentHealth = startingHealth;
        Slider.maxValue = startingHealth;
        Slider.value = startingHealth;
        refreshHealthBarUI();
        isAlive = true;
    }

    public void Update()
    {
        refreshHealthBarUI();

        if (currentHealth <= 0f && isAlive)
        {
            isAlive = false;
            onDeath();
        }

        if (HealthBar.activeSelf)
        {
            StartCoroutine("hideHealthBar");
        }
        if (mRenderer.color == Color.red)
        {
            StartCoroutine("resetColor");
        }
    }

    public void onDeath()
    {
        if (dropItem)
        {
            string id = RandomCharacterGenerator.rollWeights();
            if (id == "Coin")
            {
                for (int i = 0; i < Random.Range(1, 4); i++)
                {
                    Instantiate(item[0], transform.position, Quaternion.identity);
                }
            } else if (id == "Fix")
            {
                Instantiate(item[1], transform.position, Quaternion.identity);
            } else if (id == "Power")
            {
                Instantiate(item[2], transform.position, Quaternion.identity);
            }
        }
        if (mFloatingText)
        {
            GameObject instance = Instantiate(mFloatingText, transform.position, Quaternion.identity) as GameObject;
            instance.GetComponent<FloatingTextScript>().setFloatingText("" + ScoreValue);
            Destroy(instance, 1.5f);
        }

        if (Game_Manager.gm)
        {
            Game_Manager.gm.increaseGameScore(ScoreValue);
            Game_Manager.gm.decreaseHowManyEnemiesAreAlive();
        }

        if (explosionPrefab)
        {
            GameObject Explosion = Instantiate(explosionPrefab, transform.position, Quaternion.identity) as GameObject;
            Destroy(Explosion, 1f);
        }

        Destroy(gameObject);

    }

    public void refreshHealthBarUI()
    {
        Slider.value = currentHealth;
        Slider.maxValue = startingHealth;
        HealthSliderBackground.color = Color.Lerp(zeroHealthColor, fullHealthColor, currentHealth / startingHealth);
    }



}
