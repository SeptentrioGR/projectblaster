using UnityEngine;

public class Enemy_Tank : MonoBehaviour
{

    Vector2 shootDirection;
    public Transform tank;
    public Transform target;

    public GameObject rocket;


    public new Camera camera;

    //private bool isShoots = false;
    public float fireRate = 1.0F;
    private float nextFire = 0.0F;

    // Use this for initialization
    void Start()
    {
        camera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 targetPos = camera.WorldToScreenPoint(transform.position);
        if (targetPos.y < 690)
        {
            if (Time.time > nextFire)
            {
                nextFire = Time.time + fireRate;
                Instantiate(rocket, transform.position, Quaternion.identity);

            }
        }
    }
}
