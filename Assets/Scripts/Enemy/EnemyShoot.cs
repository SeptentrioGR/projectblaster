using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyShoot : MonoBehaviour {

	public GameObject weaponPrefab;
	public float waitTime;
	public float FireRate;
	// Use this for initialization
	void Start ()
	{
		InvokeRepeating("shoot", waitTime, FireRate);
	
	}

	void shoot()
	{
		Instantiate(weaponPrefab, transform.position, Quaternion.identity);
		
	}

}
