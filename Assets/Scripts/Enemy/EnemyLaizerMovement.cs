using UnityEngine;

public class EnemyLaizerMovement : MonoBehaviour
{
    public enum direction
    {
        left, right
    }

    public direction dir = direction.left;
    public float xvel;
    public float yvel;

    public Vector3 leftStartingPoint;
    public Vector3 RightStartingPoint;
    public Vector3 newPosition;
    public Vector2 startPos;
    // Use this for initialization
    void Start()
    {
       setPosition();
    }

    void setPosition()
    {
        int index = Random.Range(0, 2);
        if (index == 0)
        {
            dir = direction.left;
        } else if (index == 1)
        {
            dir = direction.right;
        }

        if (dir == direction.left)
        {
            transform.position = leftStartingPoint;
            startPos = transform.position;
            RotateObject(90f);
        } else if (dir == direction.right)
        {
            transform.position = RightStartingPoint;
            startPos = transform.position;
            RotateObject(-90f);
        }
    }

    // Update is called once per frame
    void Update()
    {
        move();

        Vector2 min = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));
        if (transform.position.y < min.y)
        {
            setPosition();
            transform.position = startPos;

        }
    }


    void move()
    {
        newPosition = transform.position;
        newPosition.x += xvel * Time.deltaTime;
        newPosition.y += yvel * Time.deltaTime;

        transform.position = newPosition;
    }


    void RotateObject(float degree)
    {
        Vector3 rotationVector = transform.rotation.eulerAngles;

        rotationVector.z = degree;

        transform.rotation = Quaternion.Euler(rotationVector);
    }
}
