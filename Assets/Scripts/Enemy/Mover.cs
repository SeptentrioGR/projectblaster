using UnityEngine;

public class Mover : MonoBehaviour
{

    public float speed;
    private Rigidbody2D rigid;
    void Start()
    {
        rigid = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        rigid.velocity = transform.forward * speed;
    }
}
