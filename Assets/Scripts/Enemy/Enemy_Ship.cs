using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Enemy_Ship : MonoBehaviour
{
    public enum Shooting
    {
        weaponless, normal, rapidfire, HeavyEnemyShip, AllAround, Bomb
    }
    public Shooting shootMechanic;

    [Header("General Settings")]
    public bool isAlive;
    private Animator anim; //Animations
    public bool CountsInGameEnemyCount;
    [Header("Effects")]
    public GameObject hitEffect;//The effect when bulles hit the ship;
    public GameObject explosionPrefab;//The effect when the player dies
    public GameObject cannonPrefab;//The weapon prefab
    public GameObject misslePrefab;//The missle prefab
    public ParticleSystem LeftshootEffect;//Effects on Left Cannon when you shoot
    public ParticleSystem RightshootEffect;//Effects on Right Cannon when you shoot
    public ParticleSystem DownshootEffect;
    public ParticleSystem shieldSfx;//Shield Effect enable it when you have shield

    [Header("Sounds")]
    private AudioSource mainSource;
    public AudioClip shotSfx;//The sound when the player shoots
    public AudioClip Hitsfx;//The sound when enemy projectiles hit the ship
    public AudioClip exlpodeSfx;//The sound when the player ship explodes
    private AudioClip shieldBrokeSfx; //Sound when the Shield goes off

    [Header("Health Bar Settings")]
    public GameObject HealthBar;
    public Slider Slider;
    public Image HealthSliderBackground;
    public Color fullHealthColor = Color.green;
    public Color zeroHealthColor = Color.red;


    [Header("Stats Configuration")]
    public float startingHealth;
    public float shieldCapacity;


    public float currentHealth;

    [Header("Weapon Configuration")]
    //public float CannonDamage;

    [Range(0.3f, 4)]
    public float fireRate = 0.3f;
    public float nextFire = 0.0F;

    public float RocketFireRate = 1.0f;
    public float nextRocketFire = 0.0F;

    public SpriteRenderer mRenderer;
    public Color normalColor = new Color(0, 0, 0);
    public Color hitColor = Color.red;


    public bool startShooting = false;
    //***************************************************
    [Tooltip("How much score is Rewarded")]
    public int ScoreValue;// How much score is Rewarded

    [Header("Drop Item on Death")]
    //The item that will instasiate when Enemy dies
    public bool dropItem;
    public GameObject[] item;
    public RandomCharacterGenerator itemManager;
    public GameObject mFloatingText;
    public bool MultipleCannons;
    public float offset;
    public bool isOutOfBound;
    private GameObject parentObject;
    private AudioSource enemySourceAudio;
    private Enemy_Move em;//Enemy_Move
    private Vector3 pos;//Objects WorldToScreenPoint

    public int timeToFire;
    public bool isReady;
    public bool HasShield;

    public float numberOfAttacks;

    public float DelayToStartFire;
    public float DelayTillNextFire;
    public float DelayBetweenFire;

    [Header("Item Chance")]
    public string[] drop;
    public int[] dropchance;

    void Start()
    {
        //startPos = transform.position;
        enemySourceAudio = GetComponent<AudioSource>();
        //Get the Score Text UI
        parentObject = GameObject.FindGameObjectWithTag("BulletsAndWeapon");
        pos = Camera.main.WorldToScreenPoint(transform.position);

        isAlive = true;
        updateEnemyLevel();
        //mainSource = GetComponent<AudioSource>() ;
        initializeRandomItemGenerator();
        mRenderer = GetComponentInChildren<SpriteRenderer>();
        if (HasShield && GameObject.FindObjectOfType<Game_Manager>() && Game_Manager.gm.wave > 4)
        {
            shieldCapacity = 5;
        }


        shieldSfx.Play();

    }

    public void initializeRandomItemGenerator()
    {
        RandomCharacterGenerator.wipe();
        for (int i = 0; i < drop.Length; i++)
        {
            RandomCharacterGenerator.setWeight(drop[i].ToString(), dropchance[i]);
        }
    }

    void Update()
    {
        checkGameObjectOnScreen();
        Shoot();
        if (!gameObject.GetComponent<PolygonCollider2D>().enabled)
            gameObject.GetComponent<PolygonCollider2D>().enabled = !gameObject.GetComponent<PolygonCollider2D>().enabled;

        if (currentHealth <= 0f && isAlive)
        {
            isAlive = false;
            onDeath();
        }

        if (HealthBar.activeSelf)
        {
            StartCoroutine("hideHealthBar");
        }
        if (mRenderer.color == Color.red)
        {
            StartCoroutine("resetColor");
        }
        if (shieldCapacity <= 0)
        {
            shieldSfx.Stop();
        }

    }

    public void checkGameObjectOnScreen()
    {
        if (pos.y < Screen.height && pos.y > 0)
        {
            GetComponent<PolygonCollider2D>().enabled = true;
            isOutOfBound = false;
        } else
        {
            GetComponent<PolygonCollider2D>().enabled = false;
            isOutOfBound = true;
        }
        if (!gameObject.GetComponent<PolygonCollider2D>().enabled)
            gameObject.GetComponent<PolygonCollider2D>().enabled = !gameObject.GetComponent<PolygonCollider2D>().enabled;
    }

    public void destroy()
    {
        currentHealth = 0;
        isAlive = false;
        Game_Manager.gm.decreaseHowManyEnemiesAreAlive();

    }

    public void onDeath()
    {
        if (dropItem)
        {
            string id = RandomCharacterGenerator.rollWeights();
            if (id == "Coin")
            {
                for (int i = 0; i < Random.Range(1, 4); i++)
                {
                    Instantiate(item[0], transform.position, Quaternion.identity);
                }
            } else if (id == "Fix")
            {
                Instantiate(item[1], transform.position, Quaternion.identity);
            } else if (id == "Power")
            {
                Instantiate(item[2], transform.position, Quaternion.identity);
            } else if (id == "Shield")
            {
                Instantiate(item[2], transform.position, Quaternion.identity);
            }
        }
        if (mFloatingText)
        {
            GameObject instance = Instantiate(mFloatingText, transform.position, Quaternion.identity) as GameObject;
            instance.GetComponent<FloatingTextScript>().setFloatingText("" + ScoreValue);
            Destroy(instance, 1.5f);
        }
        if (CountsInGameEnemyCount)
        {
            if (Game_Manager.gm)
            {
                Game_Manager.gm.increaseGameScore(ScoreValue);
                Game_Manager.gm.decreaseHowManyEnemiesAreAlive();
            } else if (MissionMode.missionAPI)
            {
                MissionMode.missionAPI.totalEnemies--;
            }
        }

        if (explosionPrefab)
        {
            GameObject Explosion = Instantiate(explosionPrefab, transform.position, Quaternion.identity) as GameObject;
            Destroy(Explosion, 1f);
        }
        HealthBar.SetActive(false);
        Destroy(gameObject);

    }

    void Shoot()
    {
        if (isAlive && cannonPrefab)
        {

            switch (shootMechanic)
            {
                case Shooting.weaponless:
                    return;
                case Shooting.normal:
                    if (Time.time > nextFire)
                    {
                        if (timeToFire > 0)
                        {
                            fireRate = 0.3f;
                        }
                        nextFire = Time.time + fireRate;
                        GameObject instance = Instantiate(cannonPrefab, transform.position, transform.rotation) as GameObject;
                        instance.transform.parent = parentObject.transform;
                        if (shotSfx)
                        {
                            enemySourceAudio.PlayOneShot(shotSfx, GameControler.GetSoundVolume());
                        }
                    }
                    break;
                case Shooting.rapidfire:
                    if (!isReady)
                    {
                        isReady = !isReady;
                        StartCoroutine(delayAttack());
                    }
                    break;
                case Shooting.HeavyEnemyShip:
                    if (GetComponent<HeavyEnemyShip>().startShooting && IsInvoking("Shot"))
                    {
                        InvokeRepeating("HeavyShipShootingMechanic", timeToFire, fireRate);
                    }
                    break;
                case Shooting.AllAround:
                    if (Time.time > nextFire)
                    {
                        if (timeToFire > 0)
                        {
                            fireRate = 0.3f;
                        }
                        nextFire = Time.time + fireRate;
                        GameObject instance = Instantiate(cannonPrefab, transform.position, transform.rotation) as GameObject;
                        instance.transform.parent = parentObject.transform;
                        if (shotSfx)
                        {
                            enemySourceAudio.PlayOneShot(shotSfx, GameControler.GetSoundVolume());
                        }
                    }
                    break;
                case Shooting.Bomb:
                    if (GameObject.FindGameObjectWithTag("Player"))
                    {
                        GameObject obj = GameObject.FindGameObjectWithTag("Player");
                        float Distance = Vector3.Distance(transform.position, obj.transform.position);
                        if (Distance < 3)
                        {

                            obj.GetComponent<Player_Ship>().TakeDamage(2.5f);
                            DealDamage(currentHealth);

                        }

                    }
                    break;
                default:
                    break;
            }

        }
    }


    public void updateEnemyLevel()
    {
        currentHealth = startingHealth;
        Slider.maxValue = startingHealth;
        Slider.value = startingHealth;
        updateHealthBars();
    }

    public void DealDamage(float damage)
    {
        if (shieldCapacity > 0)
        {
            shieldCapacity -= damage;
        } else
        {
            currentHealth -= damage;
        }
        if (mRenderer.color != hitColor)
        {

            updateHealthBars();
            mRenderer.color = hitColor;
            HealthBar.SetActive(true);
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        //GameObject obj = col.gameObject;
        //if (obj.GetComponent<Player_Ship>())
        //{
        //    float damageValue = obj.GetComponent<Projectle>().getDamage();
        //    if (mRenderer.color != hitColor)
        //    {
        //        currentHealth -= damageValue;
        //        updateHealthBars();
        //        mRenderer.color = hitColor;
        //        HealthBar.SetActive(true);
        //        Destroy(col.gameObject);
        //    }
        //}
    }
    void HeavyShipShootingMechanic()
    {
        GameObject instance = Instantiate(cannonPrefab, transform.position, transform.rotation) as GameObject;

        instance.transform.parent = parentObject.transform;
    }
    IEnumerator delayAttack()
    {

        yield return new WaitForSeconds(DelayToStartFire);
        for (int i = 0; i < numberOfAttacks; i++)
        {
            GameObject instance = Instantiate(cannonPrefab, transform.position, transform.rotation) as GameObject;
            instance.transform.parent = parentObject.transform;
            if (shotSfx)
            {
                enemySourceAudio.PlayOneShot(shotSfx, GameControler.GetSoundVolume());
            }
            yield return new WaitForSeconds(DelayBetweenFire);
        }
        yield return new WaitForSeconds(DelayTillNextFire);

        isReady = !isReady;
    }

    IEnumerator hideHealthBar()
    {
        yield return new WaitForSeconds(1f);
        HealthBar.SetActive(false);
    }

    IEnumerator resetColor()
    {
        yield return new WaitForSeconds(.3f);
        mRenderer.color = Color.white;
    }

    public void updateHealthBars()
    {
        Slider.value = currentHealth;
        Slider.maxValue = startingHealth;
        HealthSliderBackground.color = Color.Lerp(zeroHealthColor, fullHealthColor, currentHealth / startingHealth);
    }

}
