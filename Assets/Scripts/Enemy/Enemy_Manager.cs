using System.Collections;
using UnityEngine;

public class Enemy_Manager : MonoBehaviour
{

    public Boundaries boundaries;
    public static Enemy_Manager em;


    public float yMax;

    [Header("Wave Settings")]
    public Vector2 SpawnGroups;
    public float SpecialSpawnWait;
    public float SpecialSpawnStartWait;
    public float spawnWait;
    public float startWait;

    [Header("Enemy Settings")]
    public GameObject[] enemyTypes;

    public int availableTypeOfEnemies;//How many trypes are going to be available

    public int numberOfEnemies;//Group Sizes


    public float time;  //Timer
    public float maxTimer; //The Timer starting number




    public bool spawnEnemies;
    public bool firstRun;


    GameObject RandomEnemyType;


    public Vector3 lastSpawnEnemyPosition;
    public int[] numbers;

    public int[] HowManyEnemiesCanBeInMap;
    public int[] HowManyEnemiesAreInMap;
    public bool debug;
    void Start()
    {
        em = GetComponent<Enemy_Manager>();
        time = maxTimer;

    }


    // Update is called once per frame
    void Update()
    {
        //Wait few minutes before spawning enemies
        //WHen timer is 0 spawn next enemy and timer is equal to previous time
        //Start Spawning Enemies again
        if (spawnEnemies)
        {
            time -= Time.deltaTime; //Start spawning enemies 

            if (time <= 0)
            {
                time = maxTimer;
                if (debug)
                {
                    spawnHazardDebug();
                } else
                {
                    spawnHazard();
                }
            }
        }

        if (!spawnEnemies)
        {
            time = maxTimer;
        }
    }

    public void stopSpawningEnemies()
    {
        if (spawnEnemies)
            spawnEnemies = false;
    }

    public void startSpawningEnemies()
    {
        if (!spawnEnemies)
            spawnEnemies = true;
    }

    public void decreaseSpawningTime()
    {

        if (spawnWait > 2)
        {
            spawnWait -= .5f;
        }
    }

    public void increaseAvailableTypeOfEnemies()
    {
        if (availableTypeOfEnemies <= 22)
            availableTypeOfEnemies++;
    }
    void spawnHazardDebug()
    {
        //how many enemies will spawn each group
        int MaxGroupNumber = (int)Random.Range(SpawnGroups.x, SpawnGroups.y);

        for (int i = 0; i < MaxGroupNumber; i++)
        {
            Vector2 spawnPosition = new Vector2(numbers[Random.Range(0, numbers.Length)], Random.Range(GameBoundaries.b.spawnpointA1.y, GameBoundaries.b.spawnpointA2.y));


            lastSpawnEnemyPosition = spawnPosition;

            //if (Vector3.Distance(lastSpawnEnemyPosition, spawnPosition) < 1)
            //{
            //    int value = Random.Range(0, 2);
            //    if (value == 0)
            //    {
            //        spawnPosition.x += 2;
            //    } else if (value == 1)
            //    {
            //        spawnPosition.x -= 2;
            //    }
            //}
            Quaternion spawnRotation = Quaternion.identity;
            int randomNumber = Random.Range(0, availableTypeOfEnemies);
            GameObject RandomEnemyType = enemyTypes[randomNumber];

            if (returnNumberOfEnemyInLevel(RandomEnemyType, randomNumber) < HowManyEnemiesCanBeInMap[randomNumber])
            {
                GameObject Enemy = Instantiate(RandomEnemyType, spawnPosition, spawnRotation) as GameObject;
                Enemy.name = RandomEnemyType.name;
                Enemy.transform.SetParent(transform);
                HowManyEnemiesAreInMap[randomNumber] += 1;
            } else
            {
                Debug.Log("Out of bound");

            }

        }
    }

    public int returnNumberOfEnemyInLevel(GameObject enemy, int number)
    {
        int value = 0;
        foreach (Transform obj in transform)
        {

            if (obj.name.Equals(enemy.name))
            {
                value++;

            }
        }


        return value;
    }

    void spawnHazard()
    {
        //how many enemies will spawn each group
        int MaxGroupNumber = (int)Random.Range(SpawnGroups.x, SpawnGroups.y);

        for (int i = 0; i < MaxGroupNumber; i++)
        {
            if (Game_Manager.gm.remainingEnemiesInWave < 16)
            {


                Vector2 spawnPosition = new Vector2(numbers[Random.Range(0, numbers.Length)], Random.Range(GameBoundaries.b.spawnpointA1.y, GameBoundaries.b.spawnpointA2.y));

                if (spawnPosition.x == lastSpawnEnemyPosition.x)
                {

                    if (spawnPosition.x >= 7)
                    {
                        spawnPosition.x -= 2;
                    }
                    if (spawnPosition.x <= 5)
                    {
                        spawnPosition.x -= 2;
                    }

                    if (spawnPosition.x < -7)
                    {
                        spawnPosition.x = -7;
                    }

                    if (spawnPosition.x > 7)
                    {
                        spawnPosition.x = 7;
                    }
                    if (spawnPosition.x == -7)
                    {
                        spawnPosition.x += 2;
                    }
                    if (spawnPosition.x >= -5)
                    {
                        spawnPosition.x += 2;
                    }
                }

                lastSpawnEnemyPosition = spawnPosition;

                //if (Vector3.Distance(lastSpawnEnemyPosition, spawnPosition) < 1)
                //{
                //    int value = Random.Range(0, 2);
                //    if (value == 0)
                //    {
                //        spawnPosition.x += 2;
                //    } else if (value == 1)
                //    {
                //        spawnPosition.x -= 2;
                //    }
                //}
                Quaternion spawnRotation = Quaternion.identity;
                int randomNumber = Random.Range(0, availableTypeOfEnemies);
                GameObject RandomEnemyType = enemyTypes[randomNumber];

                if (HowManyEnemiesCanBeInMap[randomNumber] > returnNumberOfEnemyInLevel(RandomEnemyType, randomNumber))
                {
                    HowManyEnemiesAreInMap[randomNumber] += 1;
                    GameObject Enemy = Instantiate(RandomEnemyType, spawnPosition, spawnRotation) as GameObject;
                    Enemy.name = RandomEnemyType.name;
                    Enemy.transform.SetParent(transform);

                    Game_Manager.gm.increaseRemingEnemiesInWave();
                }
            }
        }
    }



    public void ResetTimer()
    {
        time = maxTimer;
    }






    IEnumerator waitBeforeSpawnEnemies()
    {
        yield return new WaitForSeconds(5);

        if (!spawnEnemies)
        {
            spawnEnemies = !spawnEnemies;
        }
    }
}
