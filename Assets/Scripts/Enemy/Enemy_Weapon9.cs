using UnityEngine;

public class Enemy_Weapon9 : MonoBehaviour
{

    private Projectiles projectile;
    public Vector2 newPosition;
    public float xVel, yVel;
    public float damage;
    Transform ship;
    public float offset;

    // Use this for initialization
    void Start()
    {
        transform.localScale = new Vector2(0, 1);
        projectile = GetComponent<Projectiles>();
    }

    // Update is called once per frame
    void Update()
    {
        Move();
    }


    public void Move()
    {

        Invoke("ChargeWeapon", 1);
        newPosition = transform.position;
        newPosition.x += xVel * Time.deltaTime;
        newPosition.y += yVel * Time.deltaTime;

        transform.position = newPosition;


        Vector3 min = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));

        if (transform.localScale.x >= 2)
        {
            Invoke("resetLaizer", 2);
        }


        if (transform.position.y < min.y)
        {
            Destroy(gameObject);
        }
    }

    public void ChargeWeapon()
    {

        transform.localScale += new Vector3(projectile.speed * Time.deltaTime, 1, 1);
        transform.localScale = new Vector3(Mathf.Clamp(transform.localScale.x, 0, 2), 1, 1);
    }

    void OnTriggerStay2D(Collider2D other)
    {

        GameObject obj = other.gameObject;
        Debug.Log(name + " Hit " + obj);
        if (obj.GetComponent<Player_Ship>())
        {

            other.gameObject.GetComponent<Player_Ship>().TakeDamage(damage);
        }
    }


    void resetLaizer()
    {
        transform.localScale = new Vector2(0, 1);
    }
}
