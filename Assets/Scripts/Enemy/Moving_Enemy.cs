 using UnityEngine;

public class Moving_Enemy : MonoBehaviour
{
    public float moveSpeed;  // enemy move speed when moving
    public float speed;  // enemy move speed when moving forwrd

    public GameObject[] myWaypoints; // to define the movement waypoints

    // store references to components on the gameObject
    Transform _transform;
    Rigidbody2D _rigidbody;


    //[SerializeField]
    //bool _moving = true;

    public float minX, maxX;

    public float vx;
    public float vy;



    public bool isAlive;




    void Awake()
    {
        // get a reference to the components we are going to be changing and store a reference for efficiency purposes
        _transform = GetComponent<Transform>();
        _rigidbody = GetComponent<Rigidbody2D>();
        if (_rigidbody == null) // if Rigidbody is missing
            Debug.LogError("Rigidbody2D component missing from this gameobject");
       //   _moving = true;


    }
    void Start()
    {
        isAlive = true;

    }

  
    void Update()
    {

        EnemyMovement();
    }

    // Move the enemy through its rigidbody based on its waypoints
    void EnemyMovement()
    {
        vx = moveSpeed * Time.fixedDeltaTime;
        vy = speed * Time.fixedDeltaTime;

        if (_transform.position.x < -minX && moveSpeed < 0)
        {
            moveSpeed *= -1;

        }
        else if (_transform.position.x > maxX && moveSpeed > 0)
        {
            moveSpeed *= -1;
        }


        _transform.position += (Vector3)new Vector2(vx, vy);
    }


}
