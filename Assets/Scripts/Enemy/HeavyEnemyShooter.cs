using UnityEngine;

public class HeavyEnemyShooter : MonoBehaviour
{

    [Header("Effects")]
    public GameObject[] Weapons;
    public GameObject cannonPrefab;//The weapon prefab
    public GameObject misslePrefab;//The missle prefab
    public ParticleSystem LeftshootEffect;//Effects on Left Cannon when you shoot
    public ParticleSystem RightshootEffect;//Effects on Right Cannon when you shoot
    public ParticleSystem DownshootEffect;

    [Header("Sounds")]

    public AudioClip shotSfx;//The sound when the player shoots

    [Header("Weapon Configuration")]
    [Range(5, 25)]
    public float CannonDamage;
    [Range(0.8f, 4)]
    public float fireRate;
    private float nextFire;
    [Range(0.8f, 4)]
    public float RocketFireRate = 1.0f;
    private float nextRocketFire = 0.0F;



    //***************************************************
    public float timeRate;
    private GameObject parentObject;
    // Use this for initialization

    void Start()
    {
        parentObject = GameObject.FindGameObjectWithTag("BulletsAndWeapon");
        cannonPrefab = Weapons[Random.Range(0, Weapons.Length)];

    }

    void Update()
    {
        if (GetComponent<HeavyEnemyShip>().startShooting && IsInvoking("Shot"))
        {
            InvokeRepeating("Shot", timeRate, fireRate);
        }
    }

    void Shot()
    {
        GameObject instance = Instantiate(cannonPrefab, transform.position, transform.rotation) as GameObject;

        instance.transform.parent = parentObject.transform;
    }


}
