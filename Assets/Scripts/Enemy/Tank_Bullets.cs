using UnityEngine;

public class Tank_Bullets : MonoBehaviour
{

    public Projectiles projectiles;
    Vector3 shootDirection;
    public GameObject tank;
    public GameObject target;
    public float speed;
    float rot_z;
    // Use this for initialization

    void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player");
        if (target)
        {
            Vector3 diff = target.transform.position - transform.position;
            shootDirection = diff;

            diff.Normalize();
            rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.Euler(0f, 0f, rot_z + 90);
        }
        else
        {

            Destroy(gameObject);
        }

    }

    // Update is called once per frame
    void Update()
    {
        transform.position += new Vector3(shootDirection.x, shootDirection.y, rot_z) * Time.deltaTime;
    }

    void OnTriggerEnter2D(Collider2D col)
    {

        if (col.gameObject.tag == "Player")
        {
            col.gameObject.GetComponent<Player_Ship>().TakeDamage(projectiles.damage);

            Destroy(gameObject);
        }

    }
}
