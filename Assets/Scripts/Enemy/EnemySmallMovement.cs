using UnityEngine;

public class EnemySmallMovement : MonoBehaviour
{
    public float xvel;
    public float yvel;

    public Vector3 startingPosition;
    public Vector3 destination;
    public Vector3 newPosition;

    // Use this for initialization
    void Start()
    {
        Invoke("changeDirection", 1);
        startingPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        move();


    }



    void move()
    {
        newPosition = transform.position;
        newPosition.x += xvel * Time.deltaTime;
        newPosition.y += yvel * Time.deltaTime;

        transform.position = newPosition;


        Vector2 min = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));
        if (transform.position.y < min.y)
        {
            transform.position = startingPosition;
        }
    }

    void changeDirection()
    {
        xvel = Random.Range(-5, 5);
    }


}
