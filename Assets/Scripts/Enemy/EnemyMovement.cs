using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    public float speed;
    float xvel;
    float yvel;

    public Vector3 startingPosition;
    public Vector3 destination;
    public Vector3 newPosition;

    public float maxY;


    public bool direction;
    // Use this for initialization
    void Start()
    {

        yvel = speed;

    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.y > maxY)
        {
            if (direction)
            {
                xvel = speed;
            }
            else
            {
                xvel = -speed;
            }
        }
        Move();

    }


    void Move()
    {
        newPosition = transform.position;
        newPosition.x += xvel * Time.deltaTime;
        newPosition.y += yvel * Time.deltaTime;

        transform.position = newPosition;
    }
}
