using UnityEngine;

public class HeavyEnemyMovement : MonoBehaviour
{
    public enum Movement
    {
        movement1, movement2, movement3, movement4
    }

    public Movement movement = Movement.movement1;

    public float speed;
    private float xvel;
    private float yvel;

    public Vector3 startingPosition;
    public Vector3 destination;
    public Vector3 newPosition;

    public float smoothness;

    public bool left, right;
    public int index;

    public float timeToStopWarningEffect;
    public GameObject AttentionEffect;
    public bool WarnPlayer;


    // Use this for initialization
    void Start()
    {
        if (GameObject.FindGameObjectWithTag("AttentionEffect"))
        {
            AttentionEffect = GameObject.FindGameObjectWithTag("AttentionEffect");
        }

        if (AttentionEffect)
        {
            AttentionEffect.GetComponent<AttentionEffect>().setTarget(gameObject
                );
        } else
        {
            Debug.LogWarning("AttentionEffect not found");
        }
        switch (movement)
        {
            case Movement.movement1:
                index = 0;
                break;
            case Movement.movement2:
                index = 1;
                break;
            case Movement.movement3:
                index = 0;
                break;
            case Movement.movement4:
                index = 1;
                break;
            default:
                Debug.LogWarning("Movement out of bound");
                break;

        }


        if (index == 1)
        {
            left = false;
            right = true;
            startingPosition = new Vector3(-15, -15, 0);
            xvel = speed;
            yvel = speed;
        } else if (index == 0)
        {
            left = true;
            right = false;
            startingPosition = new Vector3(15, -15, 0);
            xvel = -speed;
            yvel = speed;
        }
        transform.position = startingPosition;


    }

    // Update is called once per frame
    void Update()
    {
        move();
        if (WarnPlayer && AttentionEffect)
        {
            AttentionEffect.SetActive(true);
        } else if (!WarnPlayer && AttentionEffect)
        {
            AttentionEffect.SetActive(false);
        }

        switch (movement)
        {
            case Movement.movement1:
                checkPositionAndHideWarningEffect(0, 5);
                if (!left && right && newPosition.x > 0)
                {
                    xvel = 0;
                }
                break;

            case Movement.movement2:
                checkPositionAndHideWarningEffect(1, -5);
                if (left && !right && newPosition.x < 0)
                {
                    xvel = 0;
                }
                break;
            case Movement.movement3:
                checkPositionAndHideWarningEffect(0, 5);
                if (left && !right && newPosition.x < -4)
                {
                    xvel = 0;
                    yvel = 0;
                }
                break;
            case Movement.movement4:
                checkPositionAndHideWarningEffect(1, -5);
                if (!left && right && newPosition.x > 4)
                {
                    xvel = 0;
                    yvel = 0;
                }
                break;
        }



    }

    void checkPositionAndHideWarningEffect(int value, float time)
    {
        if (value == 0)
        {

            timeToStopWarningEffect = time;
            if (transform.position.x <= timeToStopWarningEffect)
            {
                AttentionEffect.GetComponent<AttentionEffect>().setTarget(null);
                //  ("transform.position.x <= timeToStopWarningEffect is true");

            }
        } else if (value == 1)
        {
            timeToStopWarningEffect = time;

            if (transform.position.x >= timeToStopWarningEffect)
            {
                AttentionEffect.GetComponent<AttentionEffect>().setTarget(null);
            }
        }

    }
    void move()
    {

        Vector2 max = Camera.main.ViewportToWorldPoint(new Vector2(1, 1));

        newPosition = transform.position;
        newPosition.x += xvel * Time.deltaTime;
        newPosition.y += yvel * Time.deltaTime;

        transform.position = newPosition;



        if (transform.position.y >= 5)
        {
            xvel = 0;
            yvel = 0;
            GetComponent<Enemy_Ship>().startShooting = true;

        }
    }

    void changeYVel()
    {

        yvel *= -1;

    }
    void changeXVel()
    {

        xvel *= -1;

    }
}
