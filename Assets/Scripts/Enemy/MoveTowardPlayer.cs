using System.Collections;
using UnityEngine;

public class MoveTowardPlayer : MonoBehaviour
{

    Transform target; //the enemy's target
    [SerializeField]
    float moveSpeed = .5f; //move speed
    [SerializeField]
    float rotationSpeed = 3; //speed of turning
    Vector3 vectorToTarget;

    bool isChasing;

    int time;
    void Awake()
    {
        time = 60;
    }


    void Start()
    {
        target = GameObject.FindWithTag("Player").transform; //target the player
        vectorToTarget = target.position - transform.position;

    }


    void Update()
    {
        LookAt();
        if (isChasing)
        {
            Chase();
        }
        else
        {
            StartCoroutine("waitBeforeChasing");
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            other.GetComponent<Player_Ship>().TakeDamage(25);
            Destroy(gameObject);
        }

    }


    void LookAt()
    {
        float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
        Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
        transform.rotation = Quaternion.Slerp(transform.rotation, q, Time.deltaTime * rotationSpeed);
    }
    void Chase()
    {
        time--;
        if (time < 0)
        {
            Vector2 movement = new Vector2(vectorToTarget.x, vectorToTarget.y);
            transform.position += (Vector3)movement * moveSpeed * Time.fixedDeltaTime;
        }
        else {
            transform.position = Vector2.Lerp(transform.position, vectorToTarget, moveSpeed * Time.fixedDeltaTime);
        }
    }

    IEnumerator waitBeforeChasing()
    {
        yield return new WaitForSeconds(1.0f);
        if (!isChasing)
        {
            isChasing = !isChasing;
        }
    }
}
