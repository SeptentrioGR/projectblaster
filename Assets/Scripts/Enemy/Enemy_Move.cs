using System.Collections;
using UnityEngine;

public class Enemy_Move : MonoBehaviour
{
    public enum MovementType
    {
        normal, sideways, movement3, movement4, movement5, Bomber
    }
    public MovementType mType;

    private Rigidbody2D rigid;
    private Enemy_Ship eship;
    private Camera mainCamera;

    public float minSpeed, maxSpeed;
    public float directionxVel;
    public float yVel;
    public float xVel;
    private int direction = 1;
    private int directionY = -1;
    public float xPos, yPos;

    //private Sprite left, right;
    private Vector3 pos;
    private Vector2 newPosition;
    private Vector2 startPos;
    //public float ScreenWidth;
    //public float ScreenHeight;

    private bool change;
    private bool attacked;
    [Space]
    [Header("Sideways Properties")]
    public bool moved;
    [Space]
    [Header("Bomber Properties")]
    public bool isReady;
    public Vector2 _direction;
    [Space]
    [Header("Movement 5 Properties")]
    public Transform target;
    float distance = Mathf.Infinity;
    float turn = 20f;

    float angle = 0;
    float speed = (2 * Mathf.PI) / 5;//2*PI in degress is 360, so you get 5 seconds to complete a circle
    float radius = 5;

    public float enterSpeed;
    public float moveSpeed;
    void Awake()
    {

        mainCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        if (GameObject.FindGameObjectWithTag("Player"))
            target = GameObject.FindGameObjectWithTag("Player").transform;
        rigid = GetComponent<Rigidbody2D>();
        eship = GetComponent<Enemy_Ship>();
        direction = Random.Range(-2, 2);
        switch (mType)
        {
            case MovementType.normal:
                moveSpeed = Random.Range(minSpeed, maxSpeed);
                break;
            case MovementType.sideways:

                moveSpeed = Random.Range(minSpeed, maxSpeed);
                break;
            case MovementType.movement3:
                moveSpeed = Random.Range(minSpeed, maxSpeed);
                break;
            case MovementType.movement4:
                xVel = Random.Range(minSpeed, maxSpeed);
                moveSpeed = Random.Range(minSpeed, maxSpeed);

                break;
            case MovementType.movement5:
                moveSpeed = Random.Range(minSpeed, maxSpeed);

                break;
            case MovementType.Bomber:

                InvokeRepeating("Bomber", 1, 5);
                break;
            default:
                break;
        }
        //ScreenWidth = -Screen.width;
        //ScreenHeight = -Screen.height;
        startPos = transform.position;

    }
    void Start()
    {

    }
    void Update()
    {
        Vector2 min = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));
        Vector2 max = Camera.main.ViewportToWorldPoint(new Vector2(1, 1));
        newPosition = transform.position;
        switch (mType)
        {
            case MovementType.normal:
                if (transform.position.y > 10)
                {
                    yVel = enterSpeed;
                } else
                {
                    if (yVel > moveSpeed)
                    {
                        yVel--;
                    } else
                    {
                        yVel = moveSpeed;
                    }

                }
                newPosition.y += yVel * directionY * Time.deltaTime;
                break;
            case MovementType.sideways:
                if (transform.position.y > 10)
                {
                    yVel = enterSpeed;
                } else
                {
                    if (yVel > moveSpeed)
                    {
                        yVel--;
                    } else
                    {
                        yVel = moveSpeed;
                    }

                }
                newPosition.x += xVel * direction * Time.deltaTime;
                newPosition.y += yVel * directionY * Time.deltaTime;
                if (!moved)
                {
                    moved = !moved;
                    StartCoroutine(Move());
                }
                break;
            case MovementType.movement3:

                if (transform.position.y > 10)
                {
                    yVel = enterSpeed;
                } else
                {
                    if (yVel > moveSpeed)
                    {
                        yVel--;
                    } else
                    {
                        yVel = moveSpeed;
                    }

                }
                newPosition.x += xVel * direction * Time.deltaTime;
                newPosition.y += yVel * directionY * Time.deltaTime;
                if (!moved)
                {
                    moved = !moved;
                    StartCoroutine(Move2());
                }
                break;
            case MovementType.movement4:


                angle += speed * Time.deltaTime; //if you want to switch direction, use -= instead of +=
                xVel = Mathf.Cos(angle) * radius + 1;
                yVel = Mathf.Sin(angle) * radius + 1;

                if (transform.position.y > 10)
                {
                    yVel = enterSpeed;
                } else
                {
                    if (yVel > moveSpeed)
                    {
                        yVel--;
                    } else
                    {
                        yVel = moveSpeed;
                    }

                }
                newPosition.x += xVel * direction * Time.deltaTime;
                newPosition.y += yVel * directionY * Time.deltaTime;

                break;
            case MovementType.movement5:
                if (GameObject.FindGameObjectWithTag("Player"))
                {
                    target = GameObject.FindGameObjectWithTag("Player").transform;
                    maxSpeed += .1f * Time.deltaTime;
                    float dif = (transform.position - target.position).sqrMagnitude;
                }
                if (target == null)
                {
                    return;
                }
                rigid.velocity = transform.forward * maxSpeed;
                Quaternion targetRotation = Quaternion.LookRotation(target.position - transform.position);
                Vector3 dir = target.position - transform.position;
                angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
                transform.rotation = Quaternion.AngleAxis(angle + 90, Vector3.forward);
                newPosition = Vector2.MoveTowards(transform.position, target.position, maxSpeed * Time.deltaTime);
                break;
            case MovementType.Bomber:
                if (isReady)
                {
                    //calculate the bullet's snew position
                    newPosition += _direction * speed * Time.deltaTime;


                }
                break;
            default:
                break;
        }

        transform.position = newPosition;
        if (mType != MovementType.movement4)
        {
            if (newPosition.x > GameBoundaries.b.rwt.x)
            {
                if (!change)
                {
                    changeDir();
                }
            } else if (newPosition.x < GameBoundaries.b.lwt.x)
            {
                if (!change)
                {
                    changeDir();
                }
            }
        }
        if (mType != MovementType.Bomber)
        {

            if (transform.position.y < min.y)
            {
                GetComponent<EnemyVisibilityCheck>().PositionRested();
                Vector2 spawnPosition = Vector2.zero;
                eship.currentHealth = eship.startingHealth;
                spawnPosition = new Vector2(Random.Range(GameBoundaries.b.spawnpointA1.x, GameBoundaries.b.spawnpointB1.x), Random.Range(GameBoundaries.b.spawnpointA1.y, GameBoundaries.b.spawnpointA2.y));
                transform.position = spawnPosition;

            }

        }


    }


    void changeDir()
    {
        direction *= -1;
    }

    void lookAt2D(Vector3 diff)
    {
        diff.Normalize();

        float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0f, 0f, rot_z + 90);
    }

    IEnumerator Move()
    {

        yield return new WaitForSeconds(2f);
        xVel = Random.Range(-3, 3);
        yield return new WaitForSeconds(1f);
        moved = !moved;


    }

    IEnumerator Move2()
    {

        yield return new WaitForSeconds(3f);
        xVel = RandomNumber(-6, 6, -3, 3);

        yield return new WaitForSeconds(Random.Range(2, 4));
        moved = !moved;
        xVel = 0;


    }

    void Bomber()
    {

        Vector3 direction = target.transform.position - transform.position;
        SetDirection(direction);
        lookAt2D(direction);
    }

    int RandomNumber(int min, int max, int rmin, int rmax)
    {
        int temp = 0;
        if (temp == 0 || temp < rmin && temp > rmin)
            temp = Random.Range(min, max);
        return temp;
    }


    public void SetDirection(Vector2 direction)
    {

        _direction = direction.normalized;

        isReady = true;
        speed = 10;
    }
}
