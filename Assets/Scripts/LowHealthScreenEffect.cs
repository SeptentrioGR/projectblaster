using UnityEngine;

public class LowHealthScreenEffect : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        if (GameObject.FindGameObjectWithTag("Player"))
        {
            if (GameObject.FindObjectOfType<Player_Ship>().lowHealthPlayed && GameObject.FindObjectOfType<Player_Ship>().isAlive)
            {
                GetComponent<Animator>().SetBool("PlayerLowHealth", true);
            } else if (!GameObject.FindObjectOfType<Player_Ship>().lowHealthPlayed && GameObject.FindObjectOfType<Player_Ship>().isAlive)
            {
                GetComponent<Animator>().SetBool("PlayerLowHealth", false);
            } else if (!GameObject.FindObjectOfType<Player_Ship>().isAlive)
            {
                GetComponent<Animator>().SetBool("PlayerLowHealth", false);
            }
        }
    }
}
