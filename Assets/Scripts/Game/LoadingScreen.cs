using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadingScreen : MonoBehaviour
{
    public string LevelToLoad;
    public string MainScene;
    public string IntroScene;
    public GameObject needRestartText;
    [HideInInspector]
    public bool GameLevelLoader;
    [HideInInspector]
    public bool firstRun;
    [HideInInspector]
    public bool needRestart;
    private int loadProgress = 0;
    public int OldGameVersion;

    //Check if Map is Loading
    private bool mapLoaded;
    public bool mMapLoaded
    {
        get { return mapLoaded; }
        set { mapLoaded = value; }
    }


    public void OnApplicationFocus(bool focus)
    {
        if (focus == true)
        {
            Debug.Log("Application lost Focused");
            Time.timeScale = 1;
        } else
        {
            Debug.Log("Application regain Focused");
            Time.timeScale = 0;
        }
    }

    public void OnApplicationPause(bool pause)
    {
        if (pause == true)
        {
            Debug.Log("Application resumed");
            Time.timeScale = 1;
        } else
        {
            Debug.Log("Application Paused");
            Time.timeScale = 0;
        }
    }

    public void OnApplicationQuit()
    {
        Debug.Log("Save before Quiting....");
        GameControler.control.saveGame();
        Debug.Log("Quiting");
    }

    void Start()
    {
        if (!GameLevelLoader)
        {
            Debug.Log("checking if game has diffrent verstion...");
            GameControler.control.LoadGame();
            OldGameVersion = GameControler.control.GAMESVERSION;
            Debug.Log("OldGameVersion " + OldGameVersion + " : " + GameControler.control.gameVersion);
            if (OldGameVersion > GameControler.control.gameVersion)
            {
                Debug.Log("old version need reset or it the first time running");
                GameControler.control.firstRun = true;
                GameControler.control.gameVersion = OldGameVersion;
                needRestart = true;

            }
        } else
        {
            Debug.Log("Loading Game Level...");
            StartCoroutine(DisplayLoadingScreen(LevelToLoad));
        }
        Debug.Log("Saving Game");
        GameControler.control.saveGame();
    }

    public void LoadMain()
    {
        LevelToLoad = MainScene;
    }

    public void LoadIntro()
    {
        LevelToLoad = IntroScene;
    }

    public void LoadScene()
    {
        StartCoroutine(DisplayLoadingScreen(LevelToLoad));
    }

    IEnumerator DisplayLoadingScreen(string level)
    {
        Debug.Log("Loading level : " + level);
        AsyncOperation async = SceneManager.LoadSceneAsync(level);
        while (!async.isDone)
        {
            Debug.Log("Level Load Done");
            yield return null;
        }
    }


    public void SaveAndQuit()
    {
        Debug.Log("Saving....");
        GameControler.control.saveGame();
        GameControler.control.deleteFiles();
        Debug.Log("Save Complete, Quiting");
        Application.Quit();

    }
}
