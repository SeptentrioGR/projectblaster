using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class GameControler : MonoBehaviour
{
    public static GameControler control;
    const string MUSIC_VOLUME_KEY = "music_volume";
    const string SOUND_VOLUME_KEY = "sound_volume";
    const string ATTACK_MODE_KEY = "attack_mode";
    const string OFFSET_KEY = "offset";
    string filename;
    [Header("Player Data")]
    //Ship Stats
    public float Health;
    public float Damage;
    [Header("Player Upgrades")]
    public float missles;
    public float shield;
    public int weapon;
    public float magneticPower;
    public float FireRate;
    public float weaponUpgrade;
    public float speed;
    public bool bulletReflect; //Destory Bullets on Inpact
    [Header("Game Data")]
    //Game Stats
    public int GAMESVERSION;
    public int gameVersion;
    public int coins;
    public float score;
    public float highscore;
    public int currentShip;
    public bool showStatistics;
    public bool Mouse;
    public bool firstRun = true;



    [Header("Game Upgrade Save")]
    //Game Upgrade Save
    public int MissileCount;
    public int upgradeCostMissle;//50
    public int ShieldCount;
    public int upgradeCostShield;//30
    public int FireRateCount;
    public int upgradeCostRateOfFire;//100
    public int SpeedCount;
    public int upgradeCostSpeed;//50
    public int MagnetCount;
    public int upgradeCostMagnet;//75
    public int shipUnlocked;
    private Animator anim;

    [Header("Game Mission")]
    public int RankInKilling;
    public int enemykilled;
    public int enemyToKill;
    public int RankInSurvival;
    public int waveSurvived;
    public int waveToSurvive;
    public int RankInGreedy;
    public int goldcoinscollected;
    public int goldCointToUnlocked;

    public bool debugMode;



    [Header("Game Options")]
    public bool autoattack;

    void Awake()
    {
        if (control == null)
        {
            DontDestroyOnLoad(gameObject);
            control = this;
        } else if (control != this)
        {
            Destroy(gameObject);
        }
        if (anim)
            anim = GameObject.FindGameObjectWithTag("LevelUpAnimation").GetComponent<Animator>();

    }

    public void SavePlayerUpgrades()
    {
        string filename = "/PlayerUpgrades.savefile";
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + filename);

        PlayerUpgradeData data = new PlayerUpgradeData();

        data.MissileCount = MissileCount;
        data.upgradeCostMissle = upgradeCostMissle;
        data.ShieldCount = ShieldCount;
        data.upgradeCostShield = upgradeCostShield;
        data.FireRateCount = FireRateCount;
        data.upgradeCostRateOfFire = upgradeCostRateOfFire;
        data.SpeedCount = SpeedCount;
        data.upgradeCostSpeed = upgradeCostSpeed;
        data.MagnetCount = MagnetCount;
        data.upgradeCostMagnet = upgradeCostMagnet;
        data.shipUnlocked = shipUnlocked;
        bf.Serialize(file, data);
        file.Close();


    }

    public void LoadPlayerUpgrades()
    {
        string filename = "/PlayerUpgrades.savefile";
        if (File.Exists(Application.persistentDataPath + filename))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + filename, FileMode.Open);

            PlayerUpgradeData data = (PlayerUpgradeData)bf.Deserialize(file);
            file.Close();
            MissileCount = data.MissileCount;
            upgradeCostMissle = data.upgradeCostMissle;
            ShieldCount = data.ShieldCount;
            upgradeCostShield = data.upgradeCostShield;
            FireRateCount = data.FireRateCount;
            upgradeCostRateOfFire = data.upgradeCostRateOfFire;
            SpeedCount = data.SpeedCount;
            upgradeCostSpeed = data.upgradeCostSpeed;
            MagnetCount = data.MagnetCount;
            upgradeCostMagnet = data.upgradeCostMagnet;
            shipUnlocked = data.shipUnlocked;
        }

    }



    public void SavePlayerData()
    {
        string filename = "/PlayerData.savefile";
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + filename);

        PlayerData data = new PlayerData();
        data.health = Health;
        data.coins = coins;
        data.Damage = Damage;
        data.missles = missles;
        data.shield = shield;
        data.weapon = weapon;
        data.FireRate = FireRate;
        data.magneticPower = magneticPower;
        data.speed = speed;
        data.bulletReflect = bulletReflect;

        bf.Serialize(file, data);
        file.Close();


    }

    public void SaveGameData()
    {
        string filename = "/GameData.savefile";
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + filename);

        GameData data = new GameData();
        data.gameVersion = gameVersion;
        data.Score = score;
        data.HightScore = highscore;
        data.currentShip = currentShip;
        data.Mouse = Mouse;
        data.firstRun = firstRun;
        bf.Serialize(file, data);
        file.Close();


    }

    public void SaveChallengesData()
    {
        string filename = "/ChallengesData.savefile";
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + filename);

        ChallengesData data = new ChallengesData();
        data.RankInKilling = RankInKilling;
        data.enemykilled = enemykilled;
        data.enemyToKill = enemyToKill;
        data.RankInSurvival = RankInSurvival;
        data.waveSurvived = waveSurvived;
        data.waveToSurvive = waveToSurvive;
        data.RankInGreedy = RankInGreedy;
        data.goldcoinscollected = goldcoinscollected;
        data.goldCointToUnlocked = goldCointToUnlocked;
        bf.Serialize(file, data);
        file.Close();


    }



    public void deleteFiles()
    {


        DeletePlayerData();
        DeleteGameData();
        DeletePlayerUpgrade();
        DeleteChallengeData();
        ResetToDefault();


    }
    #region Delete File Methods
    public void DeletePlayerData()
    {
        string playerData = "/PlayerData.savefile";
        if (File.Exists(Application.persistentDataPath + playerData))
        {
            File.Delete(Application.persistentDataPath + playerData);
        }
    }

    public void DeleteGameData()
    {
        string gamedData = "/GameData.savefile";
        if (File.Exists(Application.persistentDataPath + gamedData))
        {
            File.Delete(Application.persistentDataPath + gamedData);
        }
    }

    public void DeleteChallengeData()
    {
        string gamedData = "/ChallengesData.savefile";
        if (File.Exists(Application.persistentDataPath + gamedData))
        {
            File.Delete(Application.persistentDataPath + gamedData);
        }
    }

    public void DeletePlayerUpgrade()
    {

        string playerUpgradeData = "/PlayerUpgrades.savefile";

        if (File.Exists(Application.persistentDataPath + playerUpgradeData))
        {
            File.Delete(Application.persistentDataPath + playerUpgradeData);
        }
    }
    #endregion

    public void LoadPlayerData()
    {
        string filename = "/PlayerData.savefile";
        if (File.Exists(Application.persistentDataPath + filename))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + filename, FileMode.Open);

            PlayerData data = (PlayerData)bf.Deserialize(file);
            file.Close();

            Health = data.health;
            coins = data.coins;
            Damage = data.Damage;
            missles = data.missles;
            shield = data.shield;
            weapon = data.weapon;
            FireRate = data.FireRate;
            magneticPower = data.magneticPower;
            speed = data.speed;
            bulletReflect = data.bulletReflect;

        }

    }

    public void LoadGameData()
    {
        string filename = "/GameData.savefile";
        if (File.Exists(Application.persistentDataPath + filename))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + filename, FileMode.Open);

            GameData data = (GameData)bf.Deserialize(file);
            file.Close();
            gameVersion = data.gameVersion;
            score = data.Score;
            highscore = data.HightScore;
            currentShip = data.currentShip;
            Mouse = data.Mouse;
            firstRun = data.firstRun;
        }

    }

    public void LoadChallenges()
    {
        string filename = "/ChallengesData.savefile";
        if (File.Exists(Application.persistentDataPath + filename))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + filename, FileMode.Open);

            ChallengesData data = (ChallengesData)bf.Deserialize(file);
            RankInKilling = data.RankInKilling;
            enemykilled = data.enemykilled;
            enemyToKill = data.enemyToKill;
            RankInSurvival = data.RankInSurvival;
            waveSurvived = data.waveSurvived;
            waveToSurvive = data.waveToSurvive;
            RankInGreedy = data.RankInGreedy;
            goldcoinscollected = data.goldcoinscollected;
            goldCointToUnlocked = data.goldCointToUnlocked;
            file.Close();
        }

    }

    public void ResetToDefault()
    {
        missles = 0;
        shield = 0;
        weapon = 0;
        magneticPower = 0;
        FireRate = 10f;
        speed = 2;
        coins = 0;
        score = 0;
        highscore = 0;
        SetSoundVolume(0.75f);
        SetMusicVolume(0.75f);
        MissileCount = 0;
        upgradeCostMagnet = 50;
        ShieldCount = 0;
        upgradeCostShield = 30;
        FireRateCount = 0;
        upgradeCostRateOfFire = 100;
        SpeedCount = 0;
        upgradeCostSpeed = 50;
        MagnetCount = 0;
        upgradeCostMagnet = 75;

        RankInKilling = 0;
        enemykilled = 0;
        enemyToKill = 5;
        RankInSurvival = 0;
        waveSurvived = 0;
        waveToSurvive = 1;
        RankInGreedy = 0;
        goldcoinscollected = 0;
        goldCointToUnlocked = 25;
        shipUnlocked = 0;

    }

    #region Getters and Setters
    public float getScore()
    {
        return score;
    }


    public void setScore(float value)
    {
        score = value;
    }

    public float getHightScore()
    {
        return highscore;
    }
    public void setHighScore(float value)
    {
        highscore = value;
    }

    public void getFreeCoins(int coinsToGet)
    {
        coins += coinsToGet;
    }
    #endregion

    public void saveGame()
    {

        SavePlayerData();
        SaveGameData();
        SavePlayerUpgrades();
        SaveChallengesData();
    }

    public void LoadGame()
    {

        LoadPlayerData();
        LoadGameData();
        LoadPlayerUpgrades();
        LoadChallenges();
    }

    #region Class for Data
    [Serializable]
    class PlayerData
    {
        public float health;
        public int coins;
        public float Damage;
        public float missles;
        public float shield;
        public int weapon;
        public float FireRate;
        public float weaponUpgrade;
        public float magneticPower;
        public float speed;
        public bool bulletReflect;

    }

    [Serializable]
    class GameData
    {
        public int gameVersion;
        public float Score;
        public float HightScore;
        public int currentShip;
        public bool Mouse;
        public bool firstRun;
    }

    [Serializable]
    class ChallengesData
    {
        public int RankInKilling;
        public int enemykilled;
        public int enemyToKill;
        public int RankInSurvival;
        public int waveSurvived;
        public int waveToSurvive;
        public int RankInGreedy;
        public int goldcoinscollected;
        public int goldCointToUnlocked;
    }


    [Serializable]
    class PlayerUpgradeData
    {
        public int MissileCount;
        public int upgradeCostMissle;

        public int ShieldCount;
        public int upgradeCostShield;

        public int FireRateCount;
        public int upgradeCostRateOfFire;

        public int SpeedCount;
        public int upgradeCostSpeed;

        public int MagnetCount;
        public int upgradeCostMagnet;

        public int shipUnlocked;
    }
    #endregion


    public static float GetMusicVolume()
    {
        return PlayerPrefs.GetFloat(MUSIC_VOLUME_KEY);
    }

    public static float GetSoundVolume()
    {
        return PlayerPrefs.GetFloat(SOUND_VOLUME_KEY);
    }

    public static void SetMusicVolume(float volume)
    {
        if (volume >= 0f && volume <= 1f)
        {
            PlayerPrefs.SetFloat(MUSIC_VOLUME_KEY, volume);
        } else
        {
            Debug.LogError("Music Volume out of range" + volume);
        }
    }

    public static void SetSoundVolume(float volume)
    {
        if (volume >= 0f && volume <= 1f)
        {
            PlayerPrefs.SetFloat(SOUND_VOLUME_KEY, volume);
        } else
        {
            Debug.LogError("Sound Volume out of range" + volume);
        }
    }
    public static void setAutoAttackMode(int value)
    {
        if (value == 0 || value == 1)
        {
            PlayerPrefs.SetInt(ATTACK_MODE_KEY, value);
        } else
        {
            Debug.LogError("Cannot set Attack mode because its out of bound");
        }
    }
    public static bool GetAutoAttackMode()
    {
        int value = PlayerPrefs.GetInt(ATTACK_MODE_KEY);
        if (value == 0)
        {
            return false;
        } else if (value == 1)
        {
            return true;
        } else
        {
            Debug.LogError("Cannot get Attack mode because its out of bound");
        }
        return false;
    }


    public static void setOffset(int value)
    {
        if (value >= 1 && value <= 3)
        {
            PlayerPrefs.SetInt(OFFSET_KEY, value);
        } else
        {
            Debug.LogError("Offset out of range" + value);
        }
    }

    public static int GetOffset()
    {
        int value = PlayerPrefs.GetInt(OFFSET_KEY);
        return value;
    }

}
