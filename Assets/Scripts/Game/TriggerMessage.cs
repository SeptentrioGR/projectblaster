using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TriggerMessage : MonoBehaviour
{

    public string who;
    public string message;
    public bool loadNextLevel;
    public string level;
    public float waittime;
    public bool messageIsShown;
    // Use this for initialization

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (!messageIsShown)
            {
                messageIsShown = !messageIsShown;
                MessageManager.message.showMessage(who, message);
            }



        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (loadNextLevel)
        {
            StartCoroutine(explodeBeforeCreateBullets());
        }
        else
        {
            Destroy(gameObject);
        }
    }


    IEnumerator explodeBeforeCreateBullets()
    {
        yield return new WaitForSeconds(waittime);
        SceneManager.LoadScene(level);
        Destroy(gameObject);
    }

}
