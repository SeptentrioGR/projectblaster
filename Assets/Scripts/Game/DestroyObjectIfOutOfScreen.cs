using System.Collections;
using UnityEngine;
public class DestroyObjectIfOutOfScreen : MonoBehaviour
{

    Camera mainCamera;
    float xPos;
    float yPos;
    // Use this for initialization
    void Start()
    {
        mainCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        xPos = mainCamera.WorldToScreenPoint(transform.position).x;
        yPos = mainCamera.WorldToScreenPoint(transform.position).y;
        if (xPos > Screen.width || xPos < 0 || yPos > Screen.height || yPos < 0)
        {
            StartCoroutine(DestroyObject());
        }
    }


    IEnumerator DestroyObject()
    {
        yield return new WaitForSeconds(1f);
        Destroy(gameObject);
    }
}
