using UnityEngine;

public class Level : MonoBehaviour
{
    public Texture[] levelTextures;
    public float Xoffset = 0.5F;
    public float Yoffset = 0.5F;
    public Renderer rend;

    // Use this for initialization
    void Start()
    {
        rend = GetComponent<Renderer>();
        rend.material.SetTexture("_MainTex", levelTextures[Random.Range(0, levelTextures.Length)]);
    }

    void Update()
    {
        float Xspeed = Time.time * Xoffset;
        float Yspeed = Time.time * Yoffset;
        rend.material.SetTextureOffset("_MainTex", new Vector2(Xspeed, Yspeed));
    }


}
