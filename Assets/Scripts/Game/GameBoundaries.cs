using UnityEngine;
using System.Collections;

[System.Serializable]
public class GameBoundaries:MonoBehaviour {
    public static GameBoundaries b;//boundaries
    public Vector2 rwtb;//Right Wall bottom
    public Vector2 rwt;//Right Wall 
    public Vector2 lwtb;//left wall bottom
    public Vector2 lwt;//left wall transform
                       // Use this for initialization



    public Vector2 spawnpointA1;
    public Vector2 spawnpointB1;

    public Vector2 spawnpointA2;
    public Vector2 spawnpointB2;


    void Awake() {
        b = GetComponent<GameBoundaries>();

    }

    void OnDrawGizmos()
    {
        Gizmos.color = new Color(1f, 1f, 0f, 1f);
        Gizmos.DrawLine(spawnpointA1, spawnpointB1);
        Gizmos.DrawLine(spawnpointA2, spawnpointB2);

        Gizmos.color = new Color(1f, 0f, 0f, 1f);
        Gizmos.DrawLine(lwtb, lwt);
        Gizmos.DrawLine(rwtb, rwt);

        
    }

}
